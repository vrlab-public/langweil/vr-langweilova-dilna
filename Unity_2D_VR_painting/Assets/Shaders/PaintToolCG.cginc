#ifndef _PaintToolCG
#define _PaintToolCG

float distanceSqrd(float2 uv1, float2 uv2) {
    float2 dist = (uv2 - uv1);
    dist = dist * dist;
    return dist.x + dist.y;
}

float4 blendColorsClassic(float4 backColor, float4 frontColor) {
    if (frontColor.a <= 0.001f) {
        return backColor;
    }

    float resultAlpha = frontColor.a + backColor.a * (1.0f - frontColor.a);
    float3 resultColor = (frontColor.a * frontColor.rgb + (1.0f - frontColor.a) * backColor.a * backColor.rgb) / resultAlpha;

    return float4(resultColor, resultAlpha);
}

bool overestimateInsideBoundingCircle(float radius, float radiusSqrd, float distSqrd) {
    //tex:
    //$$
    //\text{Using an overestimate function, return true if pointPixels might output a non-zero alpha.}
    //\\ \text{As pixel size is 1x1, enlarge radius of bounding circle by } \sqrt{2} \text{. Use squared values for performance.}
    //\\ r_o \dots \text{radius of overestimate bounding circle} \qquad r \dots \text{radius}
    //\\ r_o^2 = (r+\sqrt{2})^2 = r^2 + 2\sqrt{2}r + 2 \simeq r^2 + 2.83r + 2
    //\\ \text{return } \left \| centerPixels-pointPixels \right \|^2 \leq r_o^2
    //$$
    return distSqrd <= radiusSqrd + 2.83f * radius + 2.0f;
}

#endif
