Shader "Unlit/ImplicitShape"
{
    Properties
    {
        [HideInInspector]
        _MainTex("Texture", 2DArray) = "white" {}
    }

    SubShader
    {
        Tags { "RenderType"="Opaque" }

        Cull Off

        Pass
        {
            CGPROGRAM

            #include "UnityCG.cginc"
            #include "PaintToolCG.cginc"

            #pragma vertex vert
            #pragma fragment frag

            #define ARRAY_BUFFER_SIZE 200

            struct appdata
            {
                float4 vertex : POSITION;
                float4 uv : TEXCOORD;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float4 uv : TEXCOORD;
            };

            UNITY_DECLARE_TEX2DARRAY(_MainTex);
            uniform float4 _MainTex_TexelSize;

            uniform float4 _ToolInfoArray[ARRAY_BUFFER_SIZE]; // x = tool U, y = tool V, z = tool size, w = tool angle 
            uniform float4 _ToolColorArray[ARRAY_BUFFER_SIZE];
            uniform int _ToolInfoLength;
            uniform int _IsEraser;

            uniform int _AALevel;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            float fragmentAlpha(in float4 info, in float2 fragUV) {
                // Init UVs of the tool center and current frag scaled to texture size
                float2 toolUV = info.xy * _MainTex_TexelSize.zw;
                float toolSize = info.z;
                float toolSizeSqrd = toolSize * toolSize;

                if (_AALevel > 0.0f) {
                    // Test on an overestimate, if whole pixel is outside skip calculations - important, big performance upgrade!!!!
                    if (overestimateInsideBoundingCircle(toolSize, toolSizeSqrd, distanceSqrd(toolUV, fragUV))) {
                        // Init values
                        float superPixelSize = 3.0f + (_AALevel - 1.0f) * 2.0f;
                        float superPixelValue = 0.0f;
                        float toolSizeScaledSqrd = toolSizeSqrd * superPixelSize * superPixelSize;

                        // Calculate tool UV's super pixel center
                        float centerOffset = float(int(superPixelSize / 2.0f));
                        float2 toolSuperPixelCenter = toolUV * superPixelSize.xx + centerOffset.xx;
                    
                        // Calculate fragment UV's super pixel borders
                        float2 fragSuperPixelStart = fragUV * superPixelSize.xx;
                        float2 fragSuperPixelEnd = float2(fragUV.x + 1.0f, fragUV.y + 1.0f) * superPixelSize.xx;

                        // Go over all sub pixels in the super pixel
                        for (float v = fragSuperPixelStart.y; v < fragSuperPixelEnd.y; v += 1.0f) {
                            for (float u = fragSuperPixelStart.x; u < fragSuperPixelEnd.x; u += 1.0f) {
                                float2 fragSubPixelUV = float2(u, v);
                                // If sub pixel is in range of the tool, increase super pixel value
                                if (distanceSqrd(toolSuperPixelCenter, fragSubPixelUV) < toolSizeScaledSqrd) {
                                    superPixelValue += 1.0f;
                                }
                            }
                        }

                        // Project value to range [0,1]
                        return superPixelValue / (superPixelSize * superPixelSize);;
                    }
                    else {
                        return 0.0f;
                    }
                }
                else {
                    return distanceSqrd(toolUV, fragUV) < toolSizeSqrd ? 1.0f : 0.0f;
                }
            }

            float4 frag(v2f i) : SV_Target
            {
                float4 fragColor = UNITY_SAMPLE_TEX2DARRAY(_MainTex, i.uv.xyz);
                
                float2 fragUV = i.uv;
                fragUV.x *= _MainTex_TexelSize.z;
                fragUV.y *= _MainTex_TexelSize.w;

                for (int tool = 0; tool < _ToolInfoLength; tool++) {
                    float4 color = _ToolColorArray[tool];
                    float4 info = _ToolInfoArray[tool];

                    color.a = fragmentAlpha(info, fragUV);
                    
                    if (_IsEraser) {
                        fragColor.a *= 1.0f - color.a;
                    }
                    else {
                        fragColor = blendColorsClassic(fragColor, color);
                    }
                }

                return fragColor;
            }

            ENDCG
        }
            //fixed4 frag(v2f i) : SV_Target
            //{
            //    float4 fragColor = float4(0.0, 0.0, 0.0, 0.0);

            //    float2 fragUV = i.uv;
            //    fragUV.x *= _PaperWidth;
            //    fragUV.y *= _PaperHeight;

            //    for (int brush = 0; brush < _BrushInfoLength; brush++) {
            //        float4 color = _BrushColorArray[brush];
            //        float4 info = _BrushInfoArray[brush];

            //        calculateColor(color, info, int(clamp(_AALevelArray[brush], 0.0, 5.0)), fragUV, _PaperWidth, _PaperHeight);

            //        if (brush == 0) {
            //            // init color
            //            fragColor.a = 1.0 - color.a;
            //        }
            //        else {
            //            // blend
            //            float alphaSrc = color.a;
            //            float alphaDst = fragColor.a;
            //            fragColor.a = min(1.0 - alphaSrc, alphaDst);
            //        }
            //    }

            //    // apply fog
            //    //UNITY_APPLY_FOG(i.fogCoord, color);
            //    return fragColor;
            //}
    }
}
