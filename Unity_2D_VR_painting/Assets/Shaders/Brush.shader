Shader "Unlit/Brush"
{

    Properties
    {
        [HideInInspector]
        _MainTex("Texture", 2DArray) = "white" {}
    }

    SubShader
    {
        Tags { "RenderType"="Opaque" }
        
        Cull Off

        Pass
        {
            CGPROGRAM

            #include "UnityCG.cginc"
            #include "PaintToolCG.cginc"

            #pragma vertex vert
            #pragma fragment frag

            #define ARRAY_BUFFER_SIZE 200

            struct appdata
            {
                float4 vertex : POSITION;
                float4 uv : TEXCOORD;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float4 uv : TEXCOORD;
            };

            UNITY_DECLARE_TEX2DARRAY(_MainTex);
            uniform float4 _MainTex_TexelSize;

            uniform float4 _ToolInfoArray[ARRAY_BUFFER_SIZE]; // x = tool U, y = tool V, z = tool size, w = tool angle
            uniform float4 _ToolColorArray[ARRAY_BUFFER_SIZE];
            uniform int _ToolInfoLength;
            uniform int _IsEraser;

            uniform sampler2D _BrushTexture;

            v2f vert(appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            float fragmentAlpha(in float4 info, in float2 fragUV) {
                float2 toolUV = info.xy * _MainTex_TexelSize.zw;
                float toolSize = info.z;

                // Pixels outside the bounding circle are guaranteed to produce a zero alpha, so skip calculations for them
                // The brush is a rotated square with side length toolSize (s), so radius of the bounding circle is
                //tex:
                //$$ r = \frac{\sqrt{2s^2}}{2} = \frac{\sqrt{2}}{2}s = \frac{1}{\sqrt{2}}s \simeq 0.707s $$
                float boundingRadius = 0.707f * toolSize;
                if (overestimateInsideBoundingCircle(boundingRadius, boundingRadius * boundingRadius, distanceSqrd(toolUV, fragUV))) {
                    float sinAlpha = sin(info.w);
                    float cosAlpha = cos(info.w);

                    // Create brush texture transformation matrix
                    //tex:
                    //$$
                    //\mathbf{c} \dots \text{toolUV, brush center} \qquad s \dots \text{toolSize} \qquad \alpha \dots \text{tool rotation}
                    //\\ S(\frac{1}{s})\cdot T(\frac{s}{2})\cdot R(\alpha )\cdot T(-\mathbf{c})=
                    //\begin{bmatrix}
                    //\frac{1}{s} & 0 & 0 \\ 0 & \frac{1}{s} & 0 \\ 0 & 0 & 1
                    //\end{bmatrix}
                    //\cdot
                    //\begin{bmatrix}
                    //1 & 0 & \frac{s}{2} \\ 0 & 1 & \frac{s}{2} \\ 0 & 0 & 1
                    //\end{bmatrix}
                    //\cdot
                    //\begin{bmatrix}
                    //\cos{\alpha} & -\sin{\alpha} & 0 \\ \sin{\alpha} & \cos{\alpha} & 0 \\ 0 & 0 & 1
                    //\end{bmatrix}
                    //\cdot
                    //\begin{bmatrix}
                    //1 & 0 & -c_x \\ 0 & 1 & -c_y \\ 0 & 0 & 1
                    //\end{bmatrix}
                    //$$
                    float3x3 paperToBrushTransform =
                    {
                        cosAlpha / toolSize,   -(sinAlpha / toolSize),    (-toolUV.x * cosAlpha + toolUV.y * sinAlpha) / toolSize + 0.5f,
                        sinAlpha / toolSize,   cosAlpha / toolSize,       (-toolUV.x * sinAlpha - toolUV.y * cosAlpha) / toolSize + 0.5f,
                        0.0f,                    0.0f,                        1.0f
                    };

                    float3 transformedUV = mul(paperToBrushTransform, float3(fragUV, 1.0f));

                    // Transformed UV is out of bounds
                    if (transformedUV.x <= 0.0f || transformedUV.y <= 0.0f || transformedUV.x >= 1.0f || transformedUV.y >= 1.0f) {
                        return 0.0f;
                    }
                    // Sample the texture using transformed UV
                    else {
                        return tex2D(_BrushTexture, transformedUV).a;
                    }
                }
                else {
                    return 0.0f;
                }

            }

            float4 frag(v2f i) : SV_Target
            {
                float4 fragColor = UNITY_SAMPLE_TEX2DARRAY(_MainTex, i.uv.xyz);

                float2 fragUV = i.uv;
                fragUV.x *= _MainTex_TexelSize.z;
                fragUV.y *= _MainTex_TexelSize.w;

                for (int tool = 0; tool < _ToolInfoLength; tool++) {
                    float4 info = _ToolInfoArray[tool];
                    float4 color = _ToolColorArray[tool];

                    color.a = fragmentAlpha(info, fragUV);

                    if (_IsEraser) {
                        fragColor.a *= 1.0f - color.a;
                    }
                    else {
                        fragColor = blendColorsClassic(fragColor, color);
                    }
                }

                return fragColor;
            }

            ENDCG
        }
            //fixed4 frag(v2f i) : SV_Target
            //{
            //    float4 fragColor = float4(1.0, 0.0, 0.0, 1.0);

            //    float w = _PaperWidth;
            //    float h = _PaperHeight;

            //    float2 fragUV = i.uv;
            //    fragUV.x *= w;
            //    fragUV.y *= h;

            //    for (int brush = 0; brush < _BrushInfoLength; brush++) {
            //        float brushAngle = 0;
            //        float brushSize = _BrushInfoArray[brush].z;
            //        float4 color = _BrushColorArray[brush];

            //        float2 brushUV = _BrushInfoArray[brush].xy;
            //        brushUV.x *= w;
            //        brushUV.y *= h;

            //        float sinAlpha = sin(brushAngle);
            //        float cosAlpha = cos(brushAngle);

            //        float3x3 paperToBrushTransform =
            //        {
            //            cosAlpha / brushSize,   -(sinAlpha / brushSize),    (-brushUV.x * cosAlpha + brushUV.y * sinAlpha) / brushSize + 0.5,
            //            sinAlpha / brushSize,   cosAlpha / brushSize,       (-brushUV.x * sinAlpha - brushUV.y * cosAlpha) / brushSize + 0.5,
            //            0.0,                    0.0,                        1.0
            //        };

            //        float3 transformedUV = mul(paperToBrushTransform, float3(fragUV, 1.0));

            //        if (transformedUV.x <= 0.0 || transformedUV.y <= 0.0 || transformedUV.x >= 1.0 || transformedUV.y >= 1.0) {
            //            color.a = 0.0;
            //        }
            //        else {
            //            // sample the texture
            //            fixed4 sampledTex = tex2D(_MainTex, transformedUV);
            //            float alpha = 1.0 - sampledTex.r;
            //            color.a = alpha;
            //        }

            //        if (brush == 0) {
            //            // init color
            //            fragColor.a = 1.0 - color.a;
            //        }
            //        else {
            //            // blend
            //            float alphaSrc = color.a;
            //            float alphaDst = fragColor.a;
            //            fragColor.a = min(1.0 - alphaSrc, alphaDst);
            //        }
            //    }

            //    // apply fog
            //    //UNITY_APPLY_FOG(i.fogCoord, col);
            //    return fragColor;
            //}
    }
}
