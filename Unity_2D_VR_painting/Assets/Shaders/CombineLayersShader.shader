Shader "Unlit/CombineLayersShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog
            #pragma require 2darray

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            CBUFFER_START(UnityPerMaterial)
                uniform sampler2D _MainTex;
                uniform float4 _MainTex_ST;
            CBUFFER_END

            uniform float _RenderTextureLength;
            UNITY_DECLARE_TEX2DARRAY(_RenderTextureArray);

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // Sample the main paper texture
                float4 fragColor = tex2D(_MainTex, i.uv);

                float3 uvArray = i.uv.xyy;
                int layerN = int(_RenderTextureLength);

                for (int layer = 0; layer < layerN; layer++) {
                    // Sample the current layer
                    uvArray.z = layer;
                    float4 color = UNITY_SAMPLE_TEX2DARRAY(_RenderTextureArray, uvArray);

                    // Blend the layer's color with background color
                    float alphaSrc = color.a;
                    float alphaDest = fragColor.a;
                    fragColor = alphaSrc * color + (1.0 - alphaSrc) * fragColor;
                    fragColor.a = alphaSrc + (1.0 - alphaSrc) * alphaDest;
                }

                //UNITY_APPLY_FOG(i.fogCoord, col);

                return fragColor;
            }
            ENDCG
        }
    }
}
