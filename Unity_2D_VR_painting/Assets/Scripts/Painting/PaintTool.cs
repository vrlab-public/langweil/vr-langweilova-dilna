using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;

namespace Langweil.Painting
{
    public abstract class PaintTool : MonoBehaviour, IInteractableTool
    {
        public static Registry<PaintTool> paintToolRegistry = new HashSetRegistry<PaintTool>();

        [SerializeField]
        [Tooltip("Shader defining the drawn shape")]
        [WarnOnNull]
        private Shader drawingShader;

        private Material drawingMaterial;
        public Material DrawingMaterial => drawingMaterial;

        [SerializeField]
        [Range(1, 200)]
        protected int toolSize = 5;
        public int ToolSize => toolSize;

        [SerializeField]
        protected Color toolColor = Color.red;
        public virtual Color ToolColor {
            get => toolColor;
            set => toolColor = value;
        }

        [SerializeField]
        [Tooltip("Distance when the tool will draw")]
        [Range(0, 5)]
        private float drawDistance = 0.01f;
        public float InteractionDistance => drawDistance;

        [SerializeField]
        [Tooltip("If true, the tool shape will be rotated based on the rotation of the tool")]
        private bool rotateBrush = true;
        public bool RotateBrush => rotateBrush;

        [SerializeField]
        [Tooltip("Direction in object's local space where the painted shape will be upright, pointing right")]
        private Vector3 rotationDirectionRight = new Vector3(1.0f, 0.0f, 0.0f);
        public Vector3 RotationDirectionRight => rotationDirectionRight;

        [SerializeField]
        [Tooltip("Direction in object's local space where the painted shape will be upright, pointing right")]
        private Vector3 rotationDirectionUp = new Vector3(0.0f, 1.0f, 0.0f);
        public Vector3 RotationDirectionUp => rotationDirectionUp;

        [SerializeField]
        [Tooltip("Tip of the tool that paints")]
        [WarnOnNull]
        private Transform tip;
        public Transform Tip => tip;

        [SerializeField]
        [Tooltip("True if the tool should erase instead of drawing")]
        private bool isEraser = false;
        public bool IsEraser => isEraser;

        public bool InteractionActive { get; set; } = false;

        private Rigidbody toolRigidBody;
        public Rigidbody ToolRigidBody => toolRigidBody;


        // Start is called before the first frame update
        protected virtual void Start() {
            WarnOnNullAttribute.Check(typeof(PaintTool), this, name);
            paintToolRegistry.Register(this);

            drawingMaterial = new Material(drawingShader);

            var s = rotationDirectionRight.NormalizeExceptZero(Vector3.right);
            if (s != null) {
                Debug.Log($"{name}: {s}");
            }
            s = rotationDirectionUp.NormalizeExceptZero(Vector3.up);
            if (s != null) {
                Debug.Log($"{name}: {s}");
            }

            toolRigidBody = GetComponent<Rigidbody>();
            ToolColor = toolColor;
        }

        /// <summary>
        /// Method for adding custom materials properties to painting tools. Called when info at <paramref name="index"/> is being set.
        /// </summary>
        /// <param name="index">Index of the currently being set info.</param>
        public abstract void SetMaterialPropertyArrayElement(int index);

        /// <summary>
        /// Method for pushing custom material properties to the material. Called before rendering the tool.
        /// </summary>
        /// <param name="mat"></param>
        public abstract void SetMaterialProperties(Material mat);

        private void OnDestroy() {
            paintToolRegistry.Unregister(this);
            Destroy(drawingMaterial);
        }
    }
}
