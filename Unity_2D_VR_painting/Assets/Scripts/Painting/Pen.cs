using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Langweil.Painting
{
    public class Pen : PaintTool
    {
        [SerializeField]
        [Tooltip("Level 0 means no anti-aliasing")]
        [Range(0, 5)]
        private int antiAliasingLevel = 1;

        public override void SetMaterialPropertyArrayElement(int index) {
            // Empty
        }

        public override void SetMaterialProperties(Material mat) {
            mat.SetInt("_AALevel", antiAliasingLevel);
        }
    }
}
