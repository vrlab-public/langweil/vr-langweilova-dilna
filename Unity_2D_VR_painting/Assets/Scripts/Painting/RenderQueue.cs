using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Langweil.Painting
{
    /// <summary>
    /// Modified queue for storing and manipulating info about a tool used for drawing/rendering.
    /// </summary>
    public class RenderQueue
    {
        // Shader property arrays
        public const int maxCapacity = 200;
        private static Vector4[] toolInfoArray = new Vector4[maxCapacity];
        private static Color[] toolColorArray = new Color[maxCapacity];

        private List<TextureLayer> layers;
        private readonly Action swapAction;
        private readonly PaintTool paintTool;

        private Vector4[] queue = new Vector4[maxCapacity];
        private int elementN = 0;

        public RenderQueue(List<TextureLayer> layers, PaintTool paintTool, Action swapAction) {
            this.layers = layers;
            this.paintTool = paintTool;
            this.swapAction = swapAction;
        }

        /// <summary>
        /// Push a new render info into the queue, processes the queue if full.
        /// </summary>
        /// <param name="renderInfo">Info about the tool used for drawing/rendering.</param>
        public void Push(Vector4 renderInfo) {
            // Put info into storage
            queue[elementN++] = renderInfo;
            // Process the render queue if number of enqueued elements exceed max capacity
            if (elementN == maxCapacity) {
                Process();
            }
        }

        /// <summary>
        /// Processes the queue, emptying it while drawing/rendering what is being emptied.
        /// </summary>
        public void Process() {
            if (elementN == 0) {
                return;
            }
            
            // Go over layers
            foreach (var layer in layers) {
                // If the layer accepts current material
                if (layer.AcceptTools.Contains(paintTool)) {
                    // Prepare material properties and blit/render
                    SetMaterialProperties();

                    layer.DrawLayer(paintTool);
                }
            }

            swapAction();
            elementN = 0;
        }

        private void SetMaterialProperties() {
            // Put the items from the render queue into the shader arrays
            for (int i = 0; i < elementN; i++) {
                toolInfoArray[i] = queue[i];
                toolColorArray[i] = paintTool.ToolColor;
                paintTool.SetMaterialPropertyArrayElement(i);
            }

            var mat = paintTool.DrawingMaterial;
            //Debug.Log($"Brush info length is {brushInfoLength}");
            mat.SetVectorArray("_ToolInfoArray", toolInfoArray);
            mat.SetColorArray("_ToolColorArray", toolColorArray);
            mat.SetInt("_ToolInfoLength", elementN);
            mat.SetInt("_IsEraser", paintTool.IsEraser ? 1 : 0);

            paintTool.SetMaterialProperties(mat);
        }
    }
}
