using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Langweil.Painting
{
    /// <summary>
    /// Class for storing and manipulating a layer.
    /// </summary>
    [Serializable]
    public class TextureLayer
    {
        [SerializeField]
        [Tooltip("List of tools the layer accepts")]
        private List<PaintTool> acceptTools;
        public List<PaintTool> AcceptTools => acceptTools;

        private Drawable owner;
        private int layerIndex;

        public int Width => owner.CurrentRenderTarget.width;
        public int Height => owner.CurrentRenderTarget.height;

        /// <summary>
        /// Initializes the layer and sets which <paramref name="layerIndex"/> the layer is in in <paramref name="rtArray"/>.
        /// </summary>
        /// <param name="rtArray">The array of render textures this layer is part of.</param>
        /// <param name="layerIndex">Index of this layer in <paramref name="rtArray"/>.</param>
        public void InitLayer(Drawable owner, int layerIndex) {
            this.owner = owner;
            this.layerIndex = layerIndex;

            var temp = RenderTexture.active;
            Graphics.SetRenderTarget(owner.CurrentRenderSource, 0, CubemapFace.Unknown, layerIndex);
            GL.Clear(false, true, Color.clear);
            Graphics.SetRenderTarget(owner.CurrentRenderTarget, 0, CubemapFace.Unknown, layerIndex);
            GL.Clear(false, true, Color.clear);
            RenderTexture.active = temp;
        }

        public void DrawLayer(PaintTool paintTool) {
            // Draw using set material properties
            var rtTemp = RenderTexture.active;
            GL.PushMatrix();

            Graphics.SetRenderTarget(owner.CurrentRenderTarget, 0, CubemapFace.Unknown, layerIndex);
            paintTool.DrawingMaterial.mainTexture = owner.CurrentRenderSource;
            if (paintTool.DrawingMaterial.SetPass(0)) {
                GL.LoadOrtho();

                GL.Begin(GL.QUADS);

                GL.TexCoord3(0, 1, layerIndex);
                GL.Vertex3(0, 1, 0);

                GL.TexCoord3(1, 1, layerIndex);
                GL.Vertex3(1, 1, 0);

                GL.TexCoord3(1, 0, layerIndex);
                GL.Vertex3(1, 0, 0);

                GL.TexCoord3(0, 0, layerIndex);
                GL.Vertex3(0, 0, 0);

                GL.End();
            }

            GL.PopMatrix();
            RenderTexture.active = rtTemp;
            //if (!paintTool.IsEraser) {
            //    Graphics.Blit(owner.CurrentRenderSource, owner.CurrentRenderTarget, paintTool.DrawingMaterial, 0, layerIndex);
            //}
            //else {
            //    Graphics.Blit(owner.CurrentRenderSource, owner.CurrentRenderTarget, paintTool.DrawingMaterial, 1, layerIndex);
            //}
        }

        /// <summary>
        /// Copies the texture from array <paramref name="copyFrom"/> to array <see cref="rtArray"/> at <see cref="layerIndex"/>.
        /// </summary>
        /// <param name="copyFrom"></param>
        public void CopyLayer(RenderTexture copyFrom) {
            // Copy the texture
            Graphics.Blit(copyFrom, owner.CurrentRenderTarget, layerIndex, layerIndex);
            Graphics.Blit(copyFrom, owner.CurrentRenderSource, layerIndex, layerIndex);
            RenderTexture.active = null;
        }
    }
}
