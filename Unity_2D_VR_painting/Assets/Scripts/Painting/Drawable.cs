using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;

namespace Langweil.Painting
{
    public class Drawable : MonoBehaviour
    {
        public static Registry<Drawable> drawableRegistry = new HashSetRegistry<Drawable>();

        // Layers
        [SerializeField]
        private List<TextureLayer> layers;

        [SerializeField]
        [WarnOnNull]
        private Shader layerCombiningShader;

        [SerializeField]
        private bool enableInterpolation = true;

        [SerializeField]
        [WarnOnNull]
        private MeshRenderer meshRenderer;
        public MeshRenderer MeshRenderer => meshRenderer;

        [SerializeField]
        [WarnOnNull]
        private MeshCollider drawingCollider;

        [SerializeField]
        private Vector3 interactionNormal = new Vector3(0.0f, 0.0f, 1.0f);
        [SerializeField]
        private Vector3 tangent = new Vector3(1.0f, 0.0f, 0.0f);
        [SerializeField]
        private Vector3 bitangent = new Vector3(0.0f, 1.0f, 0.0f);

        private Material paperMat;
        private Texture paperTex;
        public Texture PaperTexture {
            set => paperTex = value;
        }

        private RenderTexture rtMain;
        private RenderTexture rtArrayFront;
        private RenderTexture rtArrayBack;
        private Material layerCombiningMaterial;

        public RenderTexture CurrentRenderTarget => rtArrayBack;
        public RenderTexture CurrentRenderSource => rtArrayFront;

        // Queues
        private Queue<(PaintTool tool, Vector2 uv)> drawQueue = new Queue<(PaintTool, Vector2)>();
        private Dictionary<PaintTool, Vector2> lastEnqueued = new Dictionary<PaintTool, Vector2>();
        private Dictionary<PaintTool, Vector2> lastDequeued = new Dictionary<PaintTool, Vector2>();
        private Dictionary<PaintTool, RenderQueue> renderQueues = new Dictionary<PaintTool, RenderQueue>();

        private const int maxDDA = 20;

        private void Awake() {
            this.CheckNull();

            layerCombiningMaterial = new Material(layerCombiningShader);

            // Save original paper texture
            paperMat = meshRenderer.material;
            paperTex = paperMat.mainTexture;
            // Create the main render texture and replace the main paper texture with it
            rtMain = new RenderTexture(paperTex.width, paperTex.height, 0, RenderTextureFormat.ARGB32, paperTex.mipmapCount);
            InitRenderTexture(rtMain);
            paperMat.mainTexture = rtMain;

            // Create layers
            rtArrayFront = new RenderTexture(paperTex.width, paperTex.height, 0, RenderTextureFormat.ARGB32);
            InitRenderTextureArray(rtArrayFront);
            rtArrayBack = new RenderTexture(paperTex.width, paperTex.height, 0, RenderTextureFormat.ARGB32);
            InitRenderTextureArray(rtArrayBack);

            for (int i = 0; i < layers.Count; ++i) {
                layers[i].InitLayer(this, i);
            }
        }

        private void Start() {
            // Register this drawable
            drawableRegistry.Register(this);

            // Normalize
            var s = tangent.NormalizeExceptZero(Vector3.right);
            if (s != null) {
                Debug.Log($"{name}, {nameof(tangent)}: {s}");
            }
            s = bitangent.NormalizeExceptZero(Vector3.up);
            if (s != null) {
                Debug.Log($"{name}, {nameof(bitangent)}: {s}");
            }

            // Start the coroutine that processes the draw queue
            StartCoroutine(ProcessDrawQueue());
        }

        private void Update() {
            if (Input.GetKeyUp(KeyCode.C)) {
                for (int i = 0; i < layers.Count; ++i) {
                    layers[i].InitLayer(this, i);
                }
            }
        }

        private void FixedUpdate() {
            // Go over registered paint tools
            foreach (var paintTool in PaintTool.paintToolRegistry.Items) {
                bool drew = false;

                if (paintTool.InteractionActive) {
                    var normal = transform.TransformDirection(interactionNormal);

                    if (drawingCollider.Raycast(new Ray(paintTool.Tip.position, -normal), out RaycastHit hitInfo, paintTool.InteractionDistance)) {
                        // Push the hit into the queue
                        drawQueue.Enqueue((paintTool, hitInfo.textureCoord));
                        lastEnqueued[paintTool] = hitInfo.textureCoord;
                        drew = true;
                    }
                }

                if (!drew && lastEnqueued.TryGetValue(paintTool, out var lastEn) && !lastEn.IsPoisoned()) {
                    var pp = new Vector2();
                    pp.Poison();
                    drawQueue.Enqueue((paintTool, pp));
                    lastEnqueued[paintTool] = pp;
                }
            }
        }

        private void InitRenderTexture(RenderTexture rt) {
            if (rt.isPowerOfTwo) {
                rt.useMipMap = true;
                rt.autoGenerateMips = false;
            }
            else {
                Debug.LogWarning($"{name}'s main texture dimensions are not power of two, mip maps cannot be used");
            }

            rt.Create();
        }

        private void InitRenderTextureArray(RenderTexture rtArray) {
            // Set up layers and render texture array
            rtArray.dimension = UnityEngine.Rendering.TextureDimension.Tex2DArray;
            rtArray.volumeDepth = layers.Count;
            rtArray.useMipMap = false;
            rtArray.Create();
        }

        private IEnumerator ProcessDrawQueue() {
            while (true) {
                // Process the whole queue
                while (drawQueue.Count > 0) {
                    // Get and process hit
                    var drawInfo = drawQueue.Dequeue();

                    // Don't want to process poison pills
                    if (!drawInfo.uv.IsPoisoned()) {
                        var paintTool = drawInfo.tool;

                        // Get corresponding last hit
                        if (!lastDequeued.TryGetValue(paintTool, out Vector2 lastHit)) {
                            lastHit.Poison();
                            lastDequeued.Add(paintTool, lastHit);
                        }

                        // Get corresponding render queue
                        if (!renderQueues.TryGetValue(paintTool, out RenderQueue renderQueue)) {
                            renderQueue = new RenderQueue(layers, paintTool, SwapTextureArrays);
                            renderQueues.Add(paintTool, renderQueue);
                        }

                        float rotationAngle = 0.0f;

                        if (paintTool.RotateBrush) {
                            var rotDirLocal = transform.InverseTransformDirection(paintTool.transform.TransformDirection(paintTool.RotationDirectionRight));
                            var rotDirProjected = Vector3.ProjectOnPlane(rotDirLocal, interactionNormal);
                            rotDirProjected.NormalizeExceptZero(tangent);

                            rotationAngle = -Mathf.Sign(Vector3.Dot(rotDirProjected, bitangent)) * Mathf.Acos(Vector3.Dot(rotDirProjected, tangent));
                        }

                        // No last hit so no intepolation needed
                        if (!enableInterpolation || lastHit.IsPoisoned()) {
                            renderQueue.Push(new Vector4(drawInfo.uv.x, drawInfo.uv.y, paintTool.ToolSize, rotationAngle));
                        }
                        // Last hit exists, so interpolate between last and current hit using DDA to fill the gap
                        else {
                            foreach (var point in DDA(lastHit, drawInfo.uv, maxDDA)) {
                                renderQueue.Push(new Vector4(point.x, point.y, paintTool.ToolSize, rotationAngle));
                            }
                        }
                        
                    }

                    // Save current hit as last hit
                    lastDequeued[drawInfo.tool] = drawInfo.uv;
                }

                // Empty the render queues
                ProcessRenderQueues();

                yield return new WaitForSeconds(0.033f);
            }
        }

        private void ProcessRenderQueues() {
            // Draw all render queues
            foreach (var rq in renderQueues.Values) {
                rq.Process();
            }

            // Draw the layers into the main render texture
            layerCombiningMaterial.SetTexture("_RenderTextureArray", rtArrayFront);
            layerCombiningMaterial.SetInt("_RenderTextureLength", rtArrayFront.volumeDepth);
            Graphics.Blit(paperTex, rtMain, layerCombiningMaterial);
            rtMain.GenerateMips();
        }

        private IEnumerable<Vector2> DDA(Vector2 start, Vector2 end, int maxSteps) {
            // Convert to (integer) pixels
            var wh = new Vector2(rtArrayBack.width, rtArrayBack.height);
            Vector2 startPixel = start * wh;
            Vector2 endPixel = end * wh;

            // Direction of the line segment
            var diff = endPixel - startPixel;

            float signX = Mathf.Sign(diff.x);
            float signY = Mathf.Sign(diff.y);

            // Step vector
            Vector2 step = new Vector2();
            int steps;

            // Calculate number of steps (calculating in pixel space)
            if (diff.x == 0) {
                // Line goes straight up/down
                step.x = 0.0f;
                step.y = 1.0f * signY;
                steps = Mathf.FloorToInt(Mathf.Abs(diff.y));
            }
            else {
                // Slope of the line segment
                float m = diff.y / diff.x;
                steps = Mathf.FloorToInt(Mathf.Abs(Mathf.Abs(m) < 1 ? diff.x : diff.y));

                // Decide which coordinate will unit sample (one coordinate will move by 1 pixel, other by some form of m)
                if (Mathf.Abs(m) < 1) {
                    step.x = 1.0f * signX;
                    step.y = m * signX;
                }
                else {
                    step.x = (1.0f / m) * signY;
                    step.y = 1.0f * signY;
                }
            }

            // Scale step from pixel coordinates to UV coordinates
            step /= wh;

            // Scale step so that a maximum of maxSteps steps will be executed
            //tex:
            //$$
            //\begin{array}{ll}
            //  c \dots \text{steps} & m \dots \text{maxSteps} \\
            //  \vec{k} \dots \text{step} & \vec{l} \dots \text{newStep}
            //\end{array}
            //\\ c \cdot \vec{k} = m \cdot \vec{l} \quad\mid\: \text{both step vectors should be same length at last step}
            //$$
            if (steps > maxSteps) {
                step *= (float)steps / maxSteps;
                steps = maxSteps;
            }

            // Zero or undefined number of steps
            if (steps <= 0) {
                if (steps < 0) print($"DDA steps: {steps}");
                yield return start;
            }
            // Iterate over steps
            else {
                Vector2 current = new Vector2(start.x, start.y);
                //print(steps);
                for (int i = 0; i < steps; i++) {
                    current += step;
                    yield return current;
                }
            }
        }

        public void CopyTextures(Drawable copyFrom) {
            if (layers.Count != copyFrom.layers.Count
                || rtArrayFront.height != copyFrom.rtArrayFront.height || rtArrayFront.width != copyFrom.rtArrayFront.width
                || rtArrayFront.volumeDepth != copyFrom.rtArrayFront.volumeDepth) {
                throw new System.ArgumentException("Drawables do not match, cannot copy");
            }

            paperTex = copyFrom.paperTex;

            for (int i = 0; i < layers.Count; ++i) {
                layers[i].CopyLayer(copyFrom.rtArrayFront);
            }

            ProcessRenderQueues();
        }

        private void SwapTextureArrays() {
            Util.Swap(ref rtArrayBack, ref rtArrayFront);
        }

        public void SaveToDiskPNG(string path, string fileNameNoExt) {
            // Create texture to copy RT into
            var tex = new Texture2D(rtMain.width, rtMain.height, TextureFormat.ARGB32, false);

            // Save render target and switch it to rtMain
            var temp = RenderTexture.active;
            RenderTexture.active = rtMain;

            // Read whole RT into tex
            tex.ReadPixels(new Rect(0.0f, 0.0f, rtMain.width, rtMain.height), 0, 0);

            // Encode texture to PNG and save to disk
            var png = tex.EncodeToPNG();
            System.IO.File.WriteAllBytes(System.IO.Path.Combine(path, fileNameNoExt + ".png"), png);

            // Clean up
            RenderTexture.active = temp;
            Destroy(tex);
        }

        private void OnDestroy() {
            Destroy(paperMat);
            rtMain.Release();
            rtArrayBack.Release();
            rtArrayFront.Release();
            Destroy(layerCombiningMaterial);
            drawableRegistry.Unregister(this);
        }
    }
}
