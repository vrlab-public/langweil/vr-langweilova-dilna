using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;

namespace Langweil.Painting
{
    public class Brush : PaintTool
    {
        [SerializeField]
        [WarnOnNull]
        private Texture2D brushTexture;

        [SerializeField]
        [WarnOnNull]
        private MeshRenderer paintDisplay;

        public override Color ToolColor {
            get => toolColor;
            set {
                toolColor = value;
                paintDisplay.material.color = value;
            }
        }

        protected override void Start() {
            WarnOnNullAttribute.Check(typeof(Brush), this, name);
            base.Start();
        }

        public override void SetMaterialProperties(Material mat) {
            mat.SetTexture("_BrushTexture", brushTexture);
        }

        public override void SetMaterialPropertyArrayElement(int index) {
            // Empty
        }
    }
}
