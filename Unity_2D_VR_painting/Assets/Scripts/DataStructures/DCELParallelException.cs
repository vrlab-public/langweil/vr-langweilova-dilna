using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DataStructures
{
    public class DCELParallelException : DCELException
    {
        public DCELParallelException() {
        }

        public DCELParallelException(string message)
            : base(message) {
        }

        public DCELParallelException(string message, Exception inner)
            : base(message, inner) {
        }
    }
}
