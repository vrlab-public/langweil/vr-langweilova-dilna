using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DataStructures
{
    public class DCELException : Exception
    {
        public DCELException() {
        }

        public DCELException(string message)
            : base(message) {
        }

        public DCELException(string message, Exception inner)
            : base(message, inner) {
        }
    }
}
