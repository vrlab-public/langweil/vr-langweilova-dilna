using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DataStructures
{
    /// <summary>
    /// Exception thrown when an operation on the DCEL fails (e.g. adding an edge) before the operation finishes, meaning the DCEL is safe to use.
    /// </summary>
    public class DCELOpFailedSafeException : DCELException
    {
        public DCELOpFailedSafeException() {
        }

        public DCELOpFailedSafeException(string message)
            : base(message) {
        }

        public DCELOpFailedSafeException(string message, Exception inner)
            : base(message, inner) {
        }
    }
}
