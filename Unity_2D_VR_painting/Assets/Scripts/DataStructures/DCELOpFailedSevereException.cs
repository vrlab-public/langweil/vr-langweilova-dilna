using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DataStructures
{
    /// <summary>
    /// Exception thrown when an operation on the DCEL fails (e.g. adding an edge) after changes have already been made, meaning the DCEL might be unsafe to use.
    /// </summary>
    public class DCELOpFailedSevereException : DCELException
    {
        public DCELOpFailedSevereException() {
        }

        public DCELOpFailedSevereException(string message)
            : base(message) {
        }

        public DCELOpFailedSevereException(string message, Exception inner)
            : base(message, inner) {
        }
    }
}
