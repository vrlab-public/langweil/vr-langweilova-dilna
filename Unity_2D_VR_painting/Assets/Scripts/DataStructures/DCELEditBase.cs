using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DataStructures
{
    public abstract class DCELEditBase
    {
        public readonly int time;

        public readonly DCEL instance;

        protected DCELEditBase(DCEL instance) {
            time = DCEL.history.Time;
            this.instance = instance;
        }

        public abstract void Apply();
        public abstract void Revert();
    }
}
