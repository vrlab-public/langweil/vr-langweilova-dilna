using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;

namespace DataStructures
{
    using VertexNode = LinkedListNode<Vertex>;
    using HalfEdgeNode = LinkedListNode<HalfEdge>;

    /// <summary>
    /// Struct for storing intersection data and calculating line intersections.
    /// </summary>
    public readonly struct Intersection : System.IComparable<Intersection>, System.IEquatable<Intersection>, System.IFormattable
    {
        /// <summary>
        /// Edge the <see cref="intersectionPoint"/> belongs to.
        /// </summary>
        public readonly HalfEdge edge;

        public readonly float intersectionAlpha;

        /// <summary>
        /// Coordinates of the intersection.
        /// </summary>
        public readonly Vector2 IntersectionPoint {
            get {
                if (intersectionAlpha.EqualsAny(0.0f, 1.0f)) {
                    if (intersectionAlpha == 0.0f) {
                        return edge.Origin.Position;
                    }
                    else {
                        return edge.Neighbour.Position;
                    }
                }
                else {
                    return A.Position + intersectionAlpha * edge.LineSegment.Direction;
                }
            } 
        }

        /// <summary>
        /// Vertex A of <see cref="edge"/>.
        /// </summary>
        public readonly Vertex A => edge.Origin;

        /// <summary>
        /// Vertex B of <see cref="edge"/>.
        /// </summary>
        public readonly Vertex B => edge.Neighbour;

        /// <summary>
        /// Determines which coordinate will intersections be sorted with.
        /// </summary>
        public static bool sortByX = default;

        private const float mergeDistSqrd = Util.cm * Util.cm;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="edge">See <see cref="edge"/>.</param>
        /// <param name="intersectionPoint">See <see cref="intersectionPoint"/>.</param>
        public Intersection(HalfEdge edge, float intersectionAlpha) {
            this.edge = edge
                ?? throw new System.ArgumentNullException(nameof(edge), "Edge cannot be null");
            this.intersectionAlpha = (0.0f <= intersectionAlpha && intersectionAlpha <= 1.0f) ? intersectionAlpha
                : throw new System.ArgumentOutOfRangeException(nameof(intersectionAlpha), $"Intersection alpha {intersectionAlpha} must be between 0 and 1");
        }

        public static void Merge(List<Intersection> intersections, ref Vector2 start, ref Vector2 end) {
            // Merging not needed
            if (intersections.Count == 0) {
                return;
            }

            // Swap the edge so that the intersection point always lies on A
            int i;
            for (i = 0; i < intersections.Count; i++) {
                if (intersections[i].intersectionAlpha == 1.0f) {
                    intersections[i] = new Intersection(intersections[i].edge.Twin, 0.0f);
                }
            }

            // Merge endpoints with neighbouring intersections if they are close enough by poisoning them
            if (start.DistanceSqrd(intersections[0].IntersectionPoint) < mergeDistSqrd) {
                start.Poison();
            }

            if (end.DistanceSqrd(intersections[intersections.Count - 1].IntersectionPoint) < mergeDistSqrd) {
                end.Poison();
            }

            // Merge points where both lie on the same edge's vertex
            i = 0;
            while (i < intersections.Count) {
                int j = i + 1;
                // Merge intersections when intersection points have the same coordinates
                if (j < intersections.Count && intersections[i].IntersectionPoint.EqualsSimple(intersections[j].IntersectionPoint)) {
                    // Intersection points lie on the same vertex, so remove one of them
                    if (intersections[i].A == intersections[j].A) {
                        intersections.RemoveAt(j);
                    }
                    // Intersection points have the same coordinates but lie on different vertices, which should not happen
                    else {
                        throw new DCELOpFailedSafeException($"Intersection points have the same coordinates but lie on different vertices: {intersections[i]}, {intersections[j]}");
                    }
                }
                else {
                    ++i;
                }
            }
        }

        #region System methods

        public int CompareTo(Intersection other) {
            return sortByX ? IntersectionPoint.x.CompareTo(other.IntersectionPoint.x) : IntersectionPoint.y.CompareTo(other.IntersectionPoint.y);
        }

        public bool Equals(Intersection other) {
            return this == other;
        }

        public override bool Equals(object obj) {
            return obj is Intersection && this == (Intersection)obj;
        }

        public static bool operator ==(Intersection x, Intersection y) {
            return x.edge == y.edge && x.intersectionAlpha == y.intersectionAlpha;
        }

        public static bool operator !=(Intersection x, Intersection y) {
            return !(x == y);
        }

        public override int GetHashCode() {
            return (edge, intersectionAlpha).GetHashCode();
        }

        public override string ToString() {
            return ToString("G", System.Globalization.CultureInfo.CurrentCulture);
        }

        public string ToString(string format, System.IFormatProvider formatProvider) {
            if (string.IsNullOrEmpty(format)) format = "G";
            if (formatProvider == null) formatProvider = System.Globalization.CultureInfo.CurrentCulture;

            var sb = new System.Text.StringBuilder("{");
            if (edge == null) {
                sb.Append("[null]");
            }
            else {
                sb.Append("[");

                sb.Append(A.ToString(format, formatProvider));
                sb.Append(", ");
                sb.Append(B.ToString(format, formatProvider));

                sb.Append("]");
            }

            sb.Append(", ");

            sb.Append(IntersectionPoint.ToString(format, formatProvider));

            sb.Append("}");

            return sb.ToString();
        }

        #endregion
    }
}
