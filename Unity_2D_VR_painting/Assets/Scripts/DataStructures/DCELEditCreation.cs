using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DataStructures
{
    public class DCELEditCreation : DCELEditBase
    {
        protected DCEL parent;

        public DCELEditCreation(DCEL instance, DCEL parent) : base(instance) {
            this.parent = parent;
        }

        public override void Apply() {
        }

        public override void Revert() {
            if (!DCEL.history.ResurrectionQueue.TryGetValue(parent, out var children)) {
                children = new List<DCEL>();
                DCEL.history.ResurrectionQueue[parent] = children;
            }

            children.Add(instance);
        }
    }
}
