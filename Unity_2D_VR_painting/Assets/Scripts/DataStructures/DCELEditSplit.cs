using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DataStructures
{
    public class DCELEditSplit : DCELEditBase
    {
        protected readonly LineSegment edgeSegment;
        protected readonly float splitAlpha;

        public DCELEditSplit(DCEL instance, LineSegment edgeSegment, float splitAlpha) : base(instance) {
            this.edgeSegment = edgeSegment;
            this.splitAlpha = splitAlpha;
        }

        public override void Apply() {
            instance.SplitEdge(edgeSegment, splitAlpha, false);
        }

        public override void Revert() {
            instance.JoinEdges(edgeSegment, splitAlpha);
        }
    }
}
