using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DataStructures
{
    /// <summary>
    /// Exception thrown when the DCEL has gotten invalidated and it is unsafe to work with in the current state.
    /// </summary>
    public class DCELInvalidException : DCELException
    {
        public DCELInvalidException() {
        }

        public DCELInvalidException(string message)
            : base(message) {
        }

        public DCELInvalidException(string message, Exception inner)
            : base(message, inner) {
        }
    }
}
