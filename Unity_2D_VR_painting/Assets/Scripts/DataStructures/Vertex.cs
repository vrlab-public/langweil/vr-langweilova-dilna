using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DataStructures
{
    /// <summary>
    /// Class for representing DCEL vertices.
    /// </summary>
    public class Vertex : System.IFormattable
    {
        private readonly Vector2 position;
        /// <summary>
        /// Vertex coordinates.
        /// </summary>
        public Vector2 Position => position;
        
        private HalfEdge edge;
        /// <summary>
        /// A half-edge originating from this vertex.
        /// </summary>
        public HalfEdge Edge {
            get => edge;
            set => edge = value;
        }

        /// <summary>
        /// Returns neighbour vertices of this vertex.
        /// </summary>
        public List<Vertex> Neighbours {
            get {
                List<Vertex> neighbours = new List<Vertex>();

                foreach (var e in Edges) {
                    neighbours.Add(e.Neighbour);
                }

                return neighbours;
            }
        }

        /// <summary>
        /// Returns all half-edges originating from this vertex.
        /// </summary>
        public List<HalfEdge> Edges {
            get {
                var edges = new List<HalfEdge>();

                var current = edge;
                do {
                    edges.Add(current);
                    current = current.EdgeNeighbourCW;
                } while (current != edge);

                return edges;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="position">See <see cref="Position"/>.</param>
        public Vertex(Vector2 position) {
            this.position = position;
        }

        #region System methods

        public override string ToString() {
            return ToString("G", System.Globalization.CultureInfo.CurrentCulture);

        }

        public string ToString(string format, System.IFormatProvider formatProvider) {
            if (string.IsNullOrEmpty(format)) format = "G";
            if (formatProvider == null) formatProvider = System.Globalization.CultureInfo.CurrentCulture;

            return position.ToString(format, formatProvider);
        }

        #endregion
    }
}
