using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Utilities;

namespace DataStructures
{
    using VertexNode = LinkedListNode<Vertex>;
    using FaceNode = LinkedListNode<Face>;
    using HalfEdgeNode = LinkedListNode<HalfEdge>;

    /// <summary>
    /// Class for representing graphs/meshes using the doubly-connected edge list, or half-edge list.
    /// </summary>
    public class DCEL
    {
        public static readonly DCELHistory history = new DCELHistory();

        private LinkedList<HalfEdge> halfEdges = new LinkedList<HalfEdge>();
        private LinkedList<Vertex> vertices = new LinkedList<Vertex>();
        private LinkedList<Face> faces = new LinkedList<Face>();

        private Polygon boundary = null;
        private List<Polygon> holes = new List<Polygon>();

        public float ScaleInverse { get; private set; } = 1.0f;
        public float Scale {
            get => 1.0f / ScaleInverse;
            set => ScaleInverse = 1.0f / value;
        }

        private Dictionary<Face, Color32> faceToColor = new Dictionary<Face, Color32>();

        /// <summary>
        /// Number of faces.
        /// </summary>
        public int FaceCount => faces.Count;

        /// <summary>
        /// List of bridges, i.e. edges that have the same incident face and are not part of any cycle.
        /// </summary>
        public List<LineSegment> Bridges {
            get {
                var visited = new HashSet<HalfEdge>();
                var bridges = new List<LineSegment>();

                foreach (var edge in halfEdges) {
                    if (!visited.Contains(edge) && edge.Face == edge.Twin.Face) {
                        bridges.Add((edge.Origin.Position, edge.Neighbour.Position));
                        visited.Add(edge);
                        visited.Add(edge.Twin);
                    }
                }

                return bridges;
            }
        }

        private const float angleDelta = 0.1f;
        private IEnumerator<Color32> colorEnum = Util.GetColorSeq().GetEnumerator();

        private DCEL() { }

        #region DCEL creation

        /// <summary>
        /// Create a new <see cref="DCEL"/>. Used in place of constructor.
        /// </summary>
        /// <param name="boundaryPolygon">Polygon with which to initialize the created DCEL.</param>
        /// <returns></returns>
        public static DCEL Create(Polygon boundaryPolygon) {
            DCEL instance = new DCEL();
            instance.Init(boundaryPolygon);

            return instance;
        }

        private void Init(Polygon boundaryPolygon) {
            if (boundaryPolygon.VerticesRO.Count < 3) {
                throw new DCELInvalidException($"Initiating polygon ({boundaryPolygon}) must have at least 3 vertices");
            }
            if (halfEdges.Count > 0 || vertices.Count > 0 || faces.Count > 0) {
                throw new DCELOpFailedSafeException($"Trying to initialise already initialised DCEL with a polygon {boundaryPolygon}");
            }

            // Create the face
            var face = AddFace().Value;

            // Add all vertices to graph and connect with edges
            Vertex previous = null;
            HalfEdge previousEdge = null;
            foreach (var vPos in boundaryPolygon.VerticesRO) {
                HalfEdge forward = null;
                var current = AddVertex(vPos).Value;

                if (previous != null) {
                    forward = AddEdge(previous, current, face, null);
                    if (previousEdge != null) {
                        ConnectEdges(previousEdge, forward, forward.Twin, previousEdge.Twin);
                    }
                }

                previous = current;
                previousEdge = forward;
            }

            {
                // Connect last and first vertices
                var current = vertices.First.Value;
                var forward = AddEdge(previous, current, face, null);
                ConnectEdges(previousEdge, forward, forward.Twin, previousEdge.Twin);
                ConnectEdges(forward, current.Edge, current.Edge.Twin, forward.Twin);
            }

            face.OuterComponent = halfEdges.First.Value;
            boundary = boundaryPolygon;
        }

        /// <summary>
        /// Creates new <see cref="DCEL"/>s from this instance's faces, one per each face.
        /// </summary>
        /// <returns>New <see cref="DCEL"/>s created from this instance's faces, one per each face.</returns>
        public IEnumerable<DCEL> CreateFromFaces() {
            foreach (var face in faces) {
                DCEL instance = new DCEL();
                instance.ExtractFace(face);
                history.AddEdit(new DCELEditCreation(instance, this));
                yield return instance;
            }
        }

        private void ExtractFace(Face face) {
            var vertexCopies = new Dictionary<Vertex, Vertex>();
            var edgeCopies = new Dictionary<HalfEdge, HalfEdge>();
            var faceCopy = AddFace().Value;

            // Copy edges and vertices and connect them
            foreach (var component in face.AllComponents) {
                foreach (var edge in component.EdgeLoop) {
                    var fwdCopy = GetHalfEdgeCopy(edge, edgeCopies);
                    var bckwCopy = GetHalfEdgeCopy(edge.Twin, edgeCopies);
                    var v1Copy = GetVertexCopy(edge.Origin, vertexCopies);
                    var v2Copy = GetVertexCopy(edge.Neighbour, vertexCopies);

                    // Twin
                    fwdCopy.Twin = bckwCopy;
                    bckwCopy.Twin = fwdCopy;
                    // Origin
                    fwdCopy.Origin = v1Copy;
                    bckwCopy.Origin = v2Copy;
                    // Face
                    fwdCopy.Face = faceCopy;
                    if (edge.Twin.Face != face) bckwCopy.Face = null;
                    // Connect forward
                    fwdCopy.PreviousHEdge = GetHalfEdgeCopy(edge.PreviousHEdge, edgeCopies);
                    fwdCopy.NextHEdge = GetHalfEdgeCopy(edge.NextHEdge, edgeCopies);
                    // Connect backward
                    if (edge.Twin.Face != face) {
                        var bckwPrevTwin = edge.Twin;
                        while (bckwPrevTwin.Face != face) {
                            bckwPrevTwin = bckwPrevTwin.EdgeNeighbourCCW;
                        }
                        bckwCopy.PreviousHEdge = GetHalfEdgeCopy(bckwPrevTwin.Twin, edgeCopies);

                        var bckwNext = edge;
                        while (bckwNext.Twin.Face != face) {
                            bckwNext = bckwNext.EdgeNeighbourCW;
                        }
                        bckwCopy.NextHEdge = GetHalfEdgeCopy(bckwNext, edgeCopies);
                    }

                    // Vertex
                    v1Copy.Edge = fwdCopy;
                }
            }


            // Connect face to components
            faceCopy.OuterComponent = GetHalfEdgeCopy(face.OuterComponent, edgeCopies);
            foreach (var innerComponent in face.InnerComponents) {
                faceCopy.InnerComponents.AddLast(GetHalfEdgeCopy(innerComponent, edgeCopies));
            }

            // Create polygons
            var processedPolygonEdges = new HashSet<HalfEdge>();
            foreach (var component in faceCopy.AllComponents) {
                foreach (var edge in component.EdgeLoop) {
                    // If the twin edge is incident to an empty face, convert the outer edge loop into a polygon
                    if (edge.Twin.Face == null && !processedPolygonEdges.Contains(edge.Twin)) {
                        var outerEdgeLoop = edge.Twin.EdgeLoop;
                        var poly = Polygon.FromEdgeLoop(outerEdgeLoop);
                        if (poly.CCW) {
                            holes.Add(poly);
                        }
                        else {
                            poly.ReverseOrientation();
                            if (boundary != null) {
                                Debug.LogWarning($"Boundary polygon {poly:F4} found when mesh already has boundary polygon {boundary:F4}");
                            }
                            else {
                                boundary = poly;
                            }
                        }
                        outerEdgeLoop.ForEach(x => processedPolygonEdges.Add(x));
                    }
                }
            }
        }

        #endregion

        #region Tesselation

        /// <summary>
        /// Triangulates the <see cref="DCEL"/> represented by this instance.
        /// </summary>
        /// <param name="PositionToData">Function that takes a 2D position and returns corresponding data.</param>
        /// <param name="combineCallback">Function that takes vertices from which a new vertex is being created and returns corresponding data.</param>
        /// <param name="meshProperties">Tuple containing triangulated mesh - vertices, indeces and vertex data.</param>
        public void Triangulate(System.Func<Vector2, object> PositionToData, LibTessDotNet.CombineCallback combineCallback,
            out (Vector3[] vertices, int[] triangleIndeces, object[] otherData) meshProperties) {

            int polygonSize = 3;
            var tess = new LibTessDotNet.Tess();

            // Add boundary
            tess.AddContour(boundary.ToContour(PositionToData));

            // Add holes
            foreach (var hole in holes) {
                tess.AddContour(hole.ToContour(PositionToData));
            }

            // Triangulate
            tess.Tessellate(LibTessDotNet.WindingRule.EvenOdd, LibTessDotNet.ElementType.Polygons, polygonSize, combineCallback);
            
            // Copy triangulated data to unity compatible mesh data
            Vector3[] unityVertices = new Vector3[tess.VertexCount];
            object[] unityData = new object[tess.VertexCount];
            for (int i = 0; i < tess.VertexCount; ++i) {
                var tessVertices = tess.Vertices;
                unityVertices[i] = new Vector3(tessVertices[i].Position.X, tessVertices[i].Position.Y, tessVertices[i].Position.Z);
                unityData[i] = tessVertices[i].Data;
            }

            // Copy and reverse indeces (Unity uses CW rotation as front-face for rendereing)
            int elementIndexCount = tess.ElementCount * polygonSize;
            int[] unityIndeces = new int[elementIndexCount];
            for (int i = 0; i < elementIndexCount; ++i) {
                unityIndeces[elementIndexCount - 1 - i] = tess.Elements[i];
                //unityIndeces[i] = tess.Elements[i];
            }

            meshProperties = (unityVertices, unityIndeces, unityData);
        }

        /// <summary>
        /// Creates a line mesh from edges and point mesh from vertices. Each edge is coloured based on its incident face.
        /// </summary>
        /// <param name="lineMeshProperties">Tuple containing the line mesh - vertices, indeces and vertex colors.</param>
        /// <param name="pointMeshProperties">Tuple containing the point mesh - vertices, indeces and vertex colors.</param>
        public void Visualize(out (Vector3[] vertices, int[] indeces, Color32[] otherData) lineMeshProperties, out (Vector3[] vertices, int[] indeces, Color32[] otherData) pointMeshProperties) {
            // Four vertices per half edge
            lineMeshProperties = (new Vector3[halfEdges.Count * 4], Enumerable.Range(0, halfEdges.Count * 4).ToArray(), new Color32[halfEdges.Count * 4]);
            pointMeshProperties = (new Vector3[vertices.Count], Enumerable.Range(0, vertices.Count).ToArray(), new Color32[vertices.Count]);

            float pointDist = 0.005f;

            int i = 0;
            // Go through half edges
            foreach (var edge in halfEdges) {
                var A = edge.Origin.Position;
                var B = edge.Neighbour.Position;
                var prev = edge.PreviousHEdge.Origin.Position;
                var next = edge.NextHEdge.Neighbour.Position;
                var vEdge = B - A;
                var vPrev = A - prev;
                var vNext = next - B;
                var nEdge = Vector2.Perpendicular(vEdge).normalized;
                var nPrev = Vector2.Perpendicular(vPrev).normalized;
                var nNext = Vector2.Perpendicular(vNext).normalized;
                var tA = (nPrev + nEdge).normalized;
                var tB = (nNext + nEdge).normalized;

                var point = A + pointDist * nEdge;
                float alphaA = GetIntersectionAlpha(A, tA, point, vEdge, pointDist);
                float alphaB = GetIntersectionAlpha(B, tB, point, vEdge, pointDist);

                var FA = alphaA == 0.0f ? A + pointDist * nEdge : A + alphaA * tA;
                var FB = alphaB == 0.0f ? B + pointDist * nEdge : B + alphaB * tB;
                var FC = Vector2.Lerp(FA, FB, 0.8f) + pointDist * 1.5f * nEdge;

                var color = (Color32)Color.white;
                if (edge.Face != null) {
                    if (!faceToColor.TryGetValue(edge.Face, out color)) {
                        if (!colorEnum.MoveNext()) {
                            colorEnum.Reset();
                            colorEnum.MoveNext();
                        }

                        color = colorEnum.Current;
                        faceToColor.Add(edge.Face, color);
                    }
                }
                lineMeshProperties.vertices[4 * i] = new Vector3(FA.x, FA.y);
                lineMeshProperties.otherData[4 * i] = color;
                lineMeshProperties.vertices[4 * i + 1] = new Vector3(FB.x, FB.y);
                lineMeshProperties.otherData[4 * i + 1] = color;
                lineMeshProperties.vertices[4 * i + 2] = new Vector3(FB.x, FB.y);
                lineMeshProperties.otherData[4 * i + 2] = color;
                lineMeshProperties.vertices[4 * i + 3] = new Vector3(FC.x, FC.y);
                lineMeshProperties.otherData[4 * i + 3] = color;

                ++i;
            }

            i = 0;
            // Go through vertices
            foreach (var vertex in vertices) {
                pointMeshProperties.vertices[i] = new Vector3(vertex.Position.x, vertex.Position.y, 0.0f);
                pointMeshProperties.otherData[i] = Color.white;
                ++i;
            }
        }

        /// <summary>
        /// Creates a line mesh from edges.
        /// </summary>
        /// <param name="lineMeshProperties">Tuple containing the line mesh - vertices, indeces and vertex colors.</param>
        public void Visualize(out (Vector3[] vertices, int[] indeces, Color32[] otherData) lineMeshProperties) {
            if (halfEdges.Count % 2 != 0) {
                throw new DCELInvalidException($"Half edge count ({halfEdges.Count}) has to be divisible by 2");
            }
            lineMeshProperties = (new Vector3[halfEdges.Count], Enumerable.Range(0, halfEdges.Count).ToArray(), new Color32[halfEdges.Count]);

            var closed = new HashSet<HalfEdge>();
            int i = 0;
            // Go through half edges
            foreach (var edge in halfEdges) {
                if (!closed.Contains(edge)) {
                    closed.Add(edge);
                    closed.Add(edge.Twin);

                    var A = edge.Origin.Position;
                    var B = edge.Neighbour.Position;
                
                    var color = (Color32)Color.white;

                    lineMeshProperties.vertices[2 * i] = new Vector3(A.x, A.y);
                    lineMeshProperties.otherData[2 * i] = color;
                    lineMeshProperties.vertices[2 * i + 1] = new Vector3(B.x, B.y);
                    lineMeshProperties.otherData[2 * i + 1] = color;
                    ++i;
                }
            }
        }

        public string EdgesToString() {
            var sb = new System.Text.StringBuilder();
            var visited = new HashSet<HalfEdge>();
            int i = 0;
            foreach (var hedge in halfEdges) {
                // If edge hasn't been visited
                if (!visited.Contains(hedge)) {
                    sb.AppendLine(System.FormattableString.Invariant($"A_{{{i}}}=({hedge.Origin.Position.x},{hedge.Origin.Position.y})"));
                    sb.AppendLine(System.FormattableString.Invariant($"B_{{{i}}}=({hedge.Neighbour.Position.x},{hedge.Neighbour.Position.y})"));
                    sb.AppendLine(System.FormattableString.Invariant($"A_{{{i}}}+t(B_{{{i}}}-A_{{{i}}})"));
                    ++i;

                    // Add the edge to visited (must add both current half edge and twin half edge)
                    visited.Add(hedge);
                    visited.Add(hedge.Twin);
                }
            }

            return sb.ToString();
        }

        #endregion

        #region Element creation/deletion

        private VertexNode AddVertex(Vector2 position) {
            return vertices.AddLast(new Vertex(position));
        }

        private Vertex GetVertexCopy(Vertex orig, Dictionary<Vertex, Vertex> copies) {
            Vertex copy;
            if (!copies.TryGetValue(orig, out copy)) {
                copy = AddVertex(orig.Position).Value;
                copies.Add(orig, copy);
            }

            return copy;
        }

        private FaceNode AddFace() {
            return faces.AddLast(new Face());
        }

        private HalfEdgeNode AddHalfEdge() {
            return halfEdges.AddLast(new HalfEdge());
        }

        private HalfEdge GetHalfEdgeCopy(HalfEdge orig, Dictionary<HalfEdge, HalfEdge> copies) {
            HalfEdge copy;
            if (!copies.TryGetValue(orig, out copy)) {
                copy = AddHalfEdge().Value;
                copies.Add(orig, copy);
            }

            return copy;
        }

        private void DeleteHalfEdge(HalfEdge hEdge) {
            hEdge.Origin = null;
            hEdge.Twin = null;
            hEdge.Face = null;
            hEdge.NextHEdge = null;
            hEdge.PreviousHEdge = null;

            halfEdges.Remove(hEdge);
        }

        private void DeleteVertex(Vertex vertex) {
            vertex.Edge = null;

            vertices.Remove(vertex);
        }

        private void DeleteFace(Face face) {
            face.OuterComponent = null;
            face.InnerComponents.Clear();

            faces.Remove(face);
        }

        #endregion

        #region Element addition/modification/removal

        /// <summary>
        /// Places a new edge between <paramref name="a"/> and <paramref name="b"/>, creating new vertices and connecting them where necessary.
        /// </summary>
        /// <param name="edge">Edge to be placed.</param>
        /// <param name="addedEdges">List of all newly created edges.</param>
        public void PlaceEdge(LineSegment newEdge, out List<LineSegment> addedEdges, bool addChangesToHistory = true) {
            addedEdges = new List<LineSegment>();
            if (newEdge.A == newEdge.B) {
                throw new DCELOpFailedSafeException("Line segment endpoints cannot be equal to each other");
            }

            // Intersect cut with the paper graph
            if (Intersect(newEdge, out List<Intersection> intersections)) {
                // Sort the intersections bottom left to upper right
                float slope = newEdge.Slope;
                Intersection.sortByX = slope != float.PositiveInfinity && Mathf.Abs(slope) < 1.0f;
                intersections.Sort();

                // Add cut points to intersections too
                Vector2 firstCutPoint;
                Vector2 lastCutPoint;
                int sortCoordIdx = Intersection.sortByX ? 0 : 1;
                if (newEdge.A[sortCoordIdx] < newEdge.B[sortCoordIdx]) {
                    firstCutPoint = newEdge.A;
                    lastCutPoint = newEdge.B;
                }
                else {
                    firstCutPoint = newEdge.B;
                    lastCutPoint = newEdge.A;
                }

                if (intersections.Count > 0 && firstCutPoint[sortCoordIdx] > intersections[0].IntersectionPoint[sortCoordIdx]) {
                    throw new DCELOpFailedSafeException($"Bottom/left-most intersection point {firstCutPoint} is not the bottom/left-most one, which is {intersections[0].IntersectionPoint}");
                }
                if (intersections.Count > 0 && lastCutPoint[sortCoordIdx] < intersections[intersections.Count - 1].IntersectionPoint[sortCoordIdx]) {
                    throw new DCELOpFailedSafeException($"Upper/right intersection point {lastCutPoint} is not the upper/right-most one, which is {intersections[intersections.Count - 1].IntersectionPoint}");
                }

                // Merge intersections
                Intersection.Merge(intersections, ref firstCutPoint, ref lastCutPoint);

                // Connect intersections
                HalfEdge prevSplit = null;
                HalfEdge edgeA = null;
                HalfEdge edgeB = null;

                // Connect first cut point
                if (!firstCutPoint.IsPoisoned()) {
                    Intersection nextIntersec = intersections[0]; // always at least 1 intersection
                    // Intersection lies on a vertex (no need to check for 1, Intersection.Merge makes sure we can do this)
                    if (nextIntersec.intersectionAlpha == 0.0f) {
                        // Find edge with smallest CCW angle to the edge we are creating
                        edgeB = GetCCWClosestEdge(nextIntersec.A, firstCutPoint - nextIntersec.A.Position);

                        // Find edge with smallest CCW angle to the next edge we will be creating
                        if (intersections.Count > 1) {
                            prevSplit = GetCCWClosestEdge(nextIntersec.A, intersections[1].IntersectionPoint - nextIntersec.A.Position);
                        }
                        else if (!lastCutPoint.IsPoisoned()) {
                            prevSplit = GetCCWClosestEdge(nextIntersec.A, lastCutPoint - nextIntersec.A.Position);
                        }
                    }
                    // Other intersection doesn't lie on a vertex
                    else {
                        // Split edge
                        var edgeSegment = nextIntersec.edge.LineSegment;
                        edgeB = SplitEdge(nextIntersec.edge, nextIntersec.intersectionAlpha, addChangesToHistory);
                        if (edgeB == null) {
                            prevSplit = null;
                        }
                        else {
                            if (!edgeSegment.IsLeftOfLine(firstCutPoint)) {
                                edgeB = edgeB.Twin.NextHEdge;
                            }
                            prevSplit = edgeB.Twin.NextHEdge;
                        }
                    }

                    if (edgeB != null && edgeB.Face != null) {
                        var addedEdge = AddVertex(edgeB, firstCutPoint, addChangesToHistory);
                        if (addedEdge != null) {
                            addedEdges.Add(addedEdge.LineSegment);
                        }
                    }

                    // If the next intersection lies on a vertex, find edge with smallest CCW angle to the next edge we will be creating after adding new edge
                    if (nextIntersec.intersectionAlpha == 0.0f) {
                        if (intersections.Count > 1) {
                            prevSplit = GetCCWClosestEdge(nextIntersec.A, intersections[1].IntersectionPoint - nextIntersec.A.Position);
                        }
                        else if (!lastCutPoint.IsPoisoned()) {
                            prevSplit = GetCCWClosestEdge(nextIntersec.A, lastCutPoint - nextIntersec.A.Position);
                        }
                    }

                    intersections.RemoveAt(0);
                }

                // Connect intersection points
                for (int i = 0; i < intersections.Count; ++i) {
                    Intersection intersec = intersections[i];
                    edgeA = prevSplit;
                    edgeB = null;

                    // Intersection lies on a vertex
                    if (intersec.intersectionAlpha == 0.0f) {
                        // Intersections should be connected
                        if (prevSplit != null) {
                            // Find edge with smallest CCW angle to the edge we are creating
                            edgeB = GetCCWClosestEdge(intersec.A, prevSplit.Origin.Position - intersec.A.Position);
                        }
                    }
                    // Intersection doesn't lie on a vertex
                    else {
                        // Intersections should be connected
                        if (prevSplit != null) {
                            // Split edge
                            var edgeSegment = intersec.edge.LineSegment;
                            edgeB = SplitEdge(intersec.edge, intersec.intersectionAlpha, addChangesToHistory);
                            if (edgeB == null) {
                                prevSplit = null;
                            }
                            else {
                                if (!edgeSegment.IsLeftOfLine(prevSplit.Origin.Position)) {
                                    edgeB = edgeB.Twin.NextHEdge;
                                }

                                prevSplit = edgeB.Twin.NextHEdge;
                            }
                        }
                        // Intersection shouldn't be connected
                        else {
                            // Split edge
                            var edgeSegment = intersec.edge.LineSegment;
                            prevSplit = SplitEdge(intersec.edge, intersec.intersectionAlpha, addChangesToHistory);

                            // If there's a next intersection, set the correct edge
                            if (prevSplit != null && (
                                    (i + 1 < intersections.Count && !edgeSegment.IsLeftOfLine(intersections[i + 1].IntersectionPoint))
                                    || (!lastCutPoint.IsPoisoned() && !edgeSegment.IsLeftOfLine(lastCutPoint)))
                                ) {
                                prevSplit = prevSplit.Twin.NextHEdge;
                            }
                        }
                    }

                    // Add edge between prev and current intersections if the incident face is not a hole
                    if (edgeA != null && edgeB != null && edgeA.Face != null
                        /*&& Util.CCWAngle(e.LineSegment.Direction, to)*/) {
                        var addedEdge = AddEdge(edgeA, edgeB, addChangesToHistory);
                        if (addedEdge != null) {
                            addedEdges.Add(addedEdge.LineSegment);
                        }
                    }

                    // If the intersection lies on a vertex, find edge with smallest CCW angle to the next edge we will be creating after adding new edge
                    if (intersec.intersectionAlpha == 0.0f) {
                        if (i + 1 < intersections.Count) {
                            prevSplit = GetCCWClosestEdge(intersec.A, intersections[i + 1].IntersectionPoint - intersec.A.Position);
                        }
                        else if (!lastCutPoint.IsPoisoned()) {
                            prevSplit = GetCCWClosestEdge(intersec.A, lastCutPoint - intersec.A.Position);
                        }
                    }
                }

                // Connect last cut point
                if (!lastCutPoint.IsPoisoned() && prevSplit != null && prevSplit.Face != null) {
                    var addedEdge = AddVertex(prevSplit, lastCutPoint, addChangesToHistory);
                    if (addedEdge != null) {
                        addedEdges.Add(addedEdge.LineSegment);
                    }
                }
            }
            // Cut was made either on the paper or not on the paper without intersecting
            else {
                // If it lies on the paper, we place it, otherwise we ignore it
                // (we need to test only one of the vertices)
                if (Polygon.PointInPolygon(boundary, holes, newEdge.A)) {
                    addedEdges.Add(PlaceEdge(faces.First.Value, newEdge, addChangesToHistory).LineSegment);
                }
            }

        }

        /// <summary>
        /// Connects half-edge pairs by setting their next and previous pointers.
        /// </summary>
        /// <param name="previousForward">Sets this half-edge's next to <paramref name="nextForward"/></param>
        /// <param name="nextForward">Sets this half-edge's previous to <paramref name="previousForward"/></param>
        /// <param name="previousBackward">Sets this half-edge's next to <paramref name="nextBackward"/></param>
        /// <param name="nextBackward">Sets this half-edge's previous to <paramref name="previousBackward"/></param>
        private void ConnectEdges(HalfEdge previousForward, HalfEdge nextForward, HalfEdge previousBackward, HalfEdge nextBackward) {
            previousForward.NextHEdge = nextForward;
            nextForward.PreviousHEdge = previousForward;
            previousBackward.NextHEdge = nextBackward;
            nextBackward.PreviousHEdge = previousBackward;
        }

        public void SplitEdge(LineSegment edgePoints, float splitAlpha, bool addChangesToHistory = true) {
            var edge = FindEdge(edgePoints);
            if (edge == null) {
                throw new DCELOpFailedSafeException($"Could not find vertices or edge between points {edgePoints.A} and {edgePoints.B}");
            }
            SplitEdge(edge, splitAlpha, addChangesToHistory);
        }

        /// <summary>
        /// Splits <paramref name="halfEdge"/> and its twin into 4 new half-edges split at <paramref name="position"/>, while deleting the original half-edge with its twin.
        /// </summary>
        /// <remarks>
        /// If we label created half-edges as f1, f2, b1 and b2 (f=forward, b=backward), then:
        /// f1's Origin is set to <paramref name="halfEdge"/>'s Origin, f2's Neighbour is set to <paramref name="halfEdge"/>'s Neighbour;
        /// f1's Neighbour and f2's Origin are set to a newly created vertex at <paramref name="position"/>;
        /// fi and bi are set to be twins; remaining properties also set.
        /// </remarks>
        /// <param name="halfEdge">Half-edge to be split.</param>
        /// <param name="position">Position where the edge splits.</param>
        /// <returns>New half-edge with origin at newly created vertex at <paramref name="position"/> with same orientation as <paramref name="halfEdge"/>.</returns>
        private HalfEdge SplitEdge(HalfEdge halfEdge, float splitAlpha, bool addChangesToHistory/*, out Vertex removeVertex*/) {
            //removeVertex = null;
            var oldEdge = halfEdge.LineSegment;

            var halfEdgeTwin = halfEdge.Twin;

            var v1 = halfEdge.Origin;
            var v2 = halfEdgeTwin.Origin;

            // Check if one of the splits is not too small
            var intersectionPoint = new Intersection(halfEdge, splitAlpha).IntersectionPoint;
            bool v1TooSmall = new LineSegment(v1.Position, intersectionPoint).IsTooSmall(Util.cm * ScaleInverse), v2Toosmall = new LineSegment(v2.Position, intersectionPoint).IsTooSmall(Util.cm * ScaleInverse);
            if (v1TooSmall && v2Toosmall) {
                return null;
            }
            else if (v1TooSmall) {
                return null;
                //if (v1.Edges.Count == 1) {
                //    removeVertex = v1;
                //}
                //else {
                //    return null;
                //}
            }
            else if (v2Toosmall) {
                return null;
                //if (v2.Edges.Count == 1) {
                //    removeVertex = v2;
                //}
                //else {
                //    return null;
                //}
            }

            var f1 = halfEdge.Face;
            var f2 = halfEdgeTwin.Face;

            var vertex = AddVertex(intersectionPoint).Value;

            // Add first edge
            var forward1 = AddHalfEdge().Value;
            var backward1 = AddHalfEdge().Value;
            // Twin
            forward1.Twin = backward1;
            backward1.Twin = forward1;
            // Origin
            forward1.Origin = v1;
            backward1.Origin = vertex;
            // Connect around vertex v1
            if (halfEdge.PreviousHEdge == halfEdgeTwin) {
                backward1.NextHEdge = forward1;
                forward1.PreviousHEdge = backward1;
            }
            else {
                ConnectEdges(halfEdge.PreviousHEdge, forward1, backward1, halfEdgeTwin.NextHEdge);
            }

            // Add second edge
            var forward2 = AddHalfEdge().Value;
            var backward2 = AddHalfEdge().Value;
            // Twin
            forward2.Twin = backward2;
            backward2.Twin = forward2;
            // Origin
            forward2.Origin = vertex;
            backward2.Origin = v2;
            // Connect around vertex v2
            if (halfEdge.NextHEdge == halfEdgeTwin) {
                forward2.NextHEdge = backward2;
                backward2.PreviousHEdge = forward2;
            }
            else {
                ConnectEdges(forward2, halfEdge.NextHEdge, halfEdgeTwin.PreviousHEdge, backward2);
            }

            // Connect around vertex v
            ConnectEdges(forward1, forward2, backward2, backward1);

            // Faces and new vertex
            forward1.Face = f1;
            forward2.Face = f1;
            backward1.Face = f2;
            backward2.Face = f2;
            vertex.Edge = forward2;

            // Replace references to deleted edge where there are any
            if (v1.Edge == halfEdge) {
                v1.Edge = forward1;
            }
            if (v2.Edge == halfEdgeTwin) {
                v2.Edge = backward2;
            }
            if (f1 != null) {
                if (f1.OuterComponent == halfEdge) {
                    f1.OuterComponent = forward1;
                }
                else if (f1.InnerComponents.Contains(halfEdge)) {
                    f1.InnerComponents.Remove(halfEdge);
                    f1.InnerComponents.AddLast(forward1);
                }
            }
            if (f2 != null) {
                if (f2.OuterComponent == halfEdgeTwin) {
                    f2.OuterComponent = backward1;
                }
                else if (f2.InnerComponents.Contains(halfEdgeTwin)) {
                    f2.InnerComponents.Remove(halfEdgeTwin);
                    f2.InnerComponents.AddLast(backward1);
                }
            }

            // Remove original edge
            DeleteHalfEdge(halfEdgeTwin);
            DeleteHalfEdge(halfEdge);

            if (addChangesToHistory) {
                history.AddEdit(new DCELEditSplit(this, oldEdge, splitAlpha));
            }

            return forward2;
        }

        public void JoinEdges(LineSegment newEdge, float joiningVertexAlpha) {
            var vertexPosition = newEdge.A + joiningVertexAlpha * newEdge.Direction;
            var vertex = FindVertex(vertexPosition);
            if (vertex == null) {
                throw new DCELOpFailedSafeException($"Could not find vertex {vertexPosition} and therefore could not join edges around it");
            }
            JoinEdges(vertex);
        }

        private HalfEdge JoinEdges(Vertex vertex) {
            var edges = vertex.Edges;
            if (edges.Count != 2) {
                throw new DCELOpFailedSafeException($"Joining edges around vertex {vertex}, but 2 edges are not connected to it");
            }

            var backward1 = edges[0];
            var forward1 = backward1.Twin;
            var forward2 = edges[1];
            var backward2 = forward2.Twin;
            var v1 = forward1.Origin;
            var v2 = forward2.Neighbour;

            if (forward1.Face != forward2.Face || backward1.Face != backward2.Face) {
                throw new DCELInvalidException($"Tried joining around vertex {vertex}, but the faces of edges around it are inconsistent");
            }

            var f1 = forward1.Face;
            var f2 = backward1.Face;

            var forward = AddHalfEdge().Value;
            var backward = AddHalfEdge().Value;

            // Twin
            forward.Twin = backward;
            backward.Twin = forward;
            // Origin
            forward.Origin = v1;
            backward.Origin = v2;
            // Connect around vertex v1
            if (forward1.PreviousHEdge == forward1.Twin) {
                backward.NextHEdge = forward;
                forward.PreviousHEdge = backward;
            }
            else {
                ConnectEdges(forward1.PreviousHEdge, forward, backward, backward1.NextHEdge);
            }
            // Connect around vertex v2
            if (forward2.NextHEdge == forward2.Twin) {
                forward.NextHEdge = backward;
                backward.PreviousHEdge = forward;
            }
            else {
                ConnectEdges(forward, forward2.NextHEdge, backward2.PreviousHEdge, backward);
            }
            // Faces
            forward.Face = f1;
            backward.Face = f2;

            // Replace references to deleted edge where there are any
            if (v1.Edge == forward1) {
                v1.Edge = forward;
            }
            if (v2.Edge == backward2) {
                v2.Edge = backward;
            }
            if (f1 != null) {
                if (f1.OuterComponent == forward1) {
                    f1.OuterComponent = forward;
                }
                else if (f1.OuterComponent == forward2) {
                    f1.OuterComponent = forward;
                }
                else if (f1.InnerComponents.Contains(forward1)) {
                    f1.InnerComponents.Remove(forward1);
                    f1.InnerComponents.AddLast(forward);
                }
                else if (f1.InnerComponents.Contains(forward2)) {
                    f1.InnerComponents.Remove(forward2);
                    f1.InnerComponents.AddLast(forward);
                }
            }
            if (f2 != null) {
                if (f2.OuterComponent == backward1) {
                    f2.OuterComponent = backward;
                }
                else if (f2.OuterComponent == backward2) {
                    f2.OuterComponent = backward;
                }
                else if (f2.InnerComponents.Contains(backward1)) {
                    f2.InnerComponents.Remove(backward1);
                    f2.InnerComponents.AddLast(backward);
                }
                else if (f2.InnerComponents.Contains(backward2)) {
                    f2.InnerComponents.Remove(backward2);
                    f2.InnerComponents.AddLast(backward);
                }
            }

            // Remove original edges and vertex
            DeleteHalfEdge(forward1);
            DeleteHalfEdge(forward2);
            DeleteHalfEdge(backward1);
            DeleteHalfEdge(backward2);

            DeleteVertex(vertex);

            return forward;
        }

        public void AddEdge(LineSegment newEdge, bool addChangesToHistory = true) {
            var vertexA = FindVertex(newEdge.A);
            var vertexB = FindVertex(newEdge.B);
            var edgeDir = newEdge.Direction;

            if (vertexA == null || vertexB == null) {
                throw new DCELOpFailedSafeException($"Vertex for point A {newEdge.A} or B {newEdge.B} could not be found, cannot add edge");
            }

            var neighs = vertexA.Neighbours;
            foreach (var neigh in neighs) {
                if (neigh == vertexB) {
                    throw new DCELOpFailedSafeException($"Vertex A {newEdge.A} and B {newEdge.B} are already connected with an edge, cannot add new");
                }
            }

            var edgeA = GetCCWClosestEdge(vertexA, edgeDir);
            var edgeB = GetCCWClosestEdge(vertexB, -edgeDir);

            if (edgeA == null || edgeB == null) {
                throw new DCELOpFailedSafeException($"An already existing edge is too close to new edge between {newEdge.A} and {newEdge.B}");
            }

            AddEdge(edgeA, edgeB, addChangesToHistory);
        }

        /// <summary>
        /// Adds edge between origins of <paramref name="e1"/> and <paramref name="e2"/>.
        /// </summary>
        /// <remarks>
        /// Parameter half-edges should have the same incident face. If the vertices are already connected, this method does nothing and returns null.
        /// </remarks>
        /// <param name="e1">Half-edge whose origin will be connected with <paramref name="e2"/>'s origin.</param>
        /// <param name="e2">Half-edge whose origin will be connected with <paramref name="e1"/>'s origin.</param>
        /// <returns>New half-edge with origin at <paramref name="e1"/>'s origin.</returns>
        private HalfEdge AddEdge(HalfEdge e1, HalfEdge e2, bool addChangesToHistory) {
            if (e1.Face != e2.Face) {
                throw new DCELOpFailedSevereException("Tried connecting vertices using edges with different incident faces");
            }
            if (e1.Face == null) {
                throw new DCELOpFailedSevereException("Tried connecting vertices with null incident faces");
            }

            // Don't add edge if vertices are already connected
            if (e1.Origin.Neighbours.Contains(e2.Origin)) {
                return null;
            }

            // Check if edge is not too small
            if (new LineSegment(e1.Origin.Position, e2.Origin.Position).IsTooSmall(Util.cm * ScaleInverse)) {
                return null;
            }

            var face = e1.Face;

            var v1 = e1.Origin;
            var v2 = e2.Origin;

            var edgeLoopOrig = e1.EdgeLoop;

            var forward = AddHalfEdge().Value;
            var backward = AddHalfEdge().Value;
            // Twin
            forward.Twin = backward;
            backward.Twin = forward;
            // Origin
            forward.Origin = v1;
            backward.Origin = v2;
            // Connect around vertex v1
            ConnectEdges(e1.PreviousHEdge, forward, backward, e1);
            // Connect around vertex v2
            ConnectEdges(forward, e2, e2.PreviousHEdge, backward);

            // Faces

            // If the edge loop containing edge1 also contains edge2, both edges belong to the same component
            // and therefore the face must be split into 2. Otherwise the edges belong to different components,
            // meaning the new edge joins them into one
            // -----------------
            // Split face into 2
            if (edgeLoopOrig.Contains(e2)) {
                // Terminology - outer face = face surrounding the other face; inner face = face that is not outer
                // we want: f1 - inner, f2 - outer OR f1,f2 - inner

                var f1 = AddFace().Value;
                var f2 = AddFace().Value;

                var e1Loop = backward.EdgeLoop;
                var e2Loop = forward.EdgeLoop;

                // Connect edges to faces
                // Includes forward and backward
                foreach (var edge in e1Loop) {
                    edge.Face = f1;
                }
                foreach (var edge in e2Loop) {
                    edge.Face = f2;
                }

                // As both edges belong to same component, they are either part of one outer or inner component
                bool componentIsOuter = edgeLoopOrig.Contains(face.OuterComponent);

                // Even if an edge belongs to an outer component, it might belong to a part that would normally be inner, but is outer because it's connected by a bridge edge
                // We need to find the face that is inner for checking which inner components belong to which face
                var f1Poly = Polygon.FromEdgeLoop(e1Loop);
                // Polygon shouldn't be null, edge loops were created from backward and forward edges that split the face, so they should belong to different faces
                if (f1Poly == null) {
                    throw new DCELOpFailedSevereException("Adding edge, spliting face: Forward and backward belong to same face, cannot continue");
                }

                // Swap locals so that e1 belongs to inner face
                bool swapped = false;
                // Check e1 loop orientation - if ccw, then e1 belongs to inner face (forward might belong to outer or inner)
                if (!f1Poly.CCW) {
                    swapped = true;
                    Util.Swap(ref e1, ref e2);
                    Util.Swap(ref v1, ref v2);
                    Util.Swap(ref f1, ref f2);
                    Util.Swap(ref forward, ref backward);
                    Util.Swap(ref e1Loop, ref e2Loop);
                    f1Poly = Polygon.FromEdgeLoop(e1Loop);
                    if (f1Poly == null) {
                        throw new DCELOpFailedSevereException("Adding edge, spliting face: Forward and backward belong to same face (after swap), cannot continue");
                    }
                }

                // Connect faces to edges
                // Edges belong to face outer component
                if (componentIsOuter) {
                    f1.OuterComponent = e1;
                    f2.OuterComponent = e2;
                }
                // Edges belong to face inner component
                else {
                    f1.OuterComponent = e1;

                    f2.OuterComponent = face.OuterComponent;
                    f2.InnerComponents.AddLast(e2);
                    var outerEdgeLoop = face.OuterComponent.EdgeLoop;
                    foreach (var edge in outerEdgeLoop) {
                        edge.Face = f2;
                    }
                }

                // Move original face inner components to correct new face
                foreach (var inner in face.InnerComponents) {
                    // Only for inner components that e1 and e2 do not belonged to
                    if (componentIsOuter || !edgeLoopOrig.Contains(inner)) {
                        Face belongsTo;

                        // If one of the vertices from the component is inside a face, then the whole component is in the face
                        if (f1Poly.PointInPolygon(inner.Origin.Position)) {
                            belongsTo = f1;
                        }
                        else {
                            belongsTo = f2;
                        }

                        // Add as inner
                        belongsTo.InnerComponents.AddLast(inner);
                        // Connect inner edges to face
                        foreach (var innerEdge in inner.EdgeLoop) {
                            innerEdge.Face = belongsTo;
                        }
                    }
                }

                // Swap forward and backward back so that the right one is returned
                if (swapped) {
                    Util.Swap(ref forward, ref backward);
                }

                // Delete original face
                DeleteFace(face);
            }
            // Keep face
            else {
                var edgeLoopNew = e1.EdgeLoop;

                // Face would contain two edge references to the same component, so delete one edge reference
                // ------
                // No need to search in outer components, as the new edge can be only between
                // an inner and outer or two inner components - the reason for the second case is trivial,
                // for the first case connecting an inner to an outer component makes the new component
                // an outer component, so we only need to remove the inner reference
                for (var edgeRef = face.InnerComponents.First; edgeRef != null; edgeRef = edgeRef.Next) {
                    // Remove first ref
                    if (edgeLoopNew.Contains(edgeRef.Value)) {
                        face.InnerComponents.Remove(edgeRef);
                        break;
                    }
                }

                forward.Face = face;
                backward.Face = face;
            }

            if (addChangesToHistory) {
                history.AddEdit(new DCELEditEdge(this, DCELEditEdge.EditType.Add, forward.LineSegment));
            }

            return forward;
        }

        public void AddVertex(Vector2 vertexPosition, List<Vector2> connectionPositions, bool addChangesToHistory = true) {
            if (connectionPositions.Count == 0) {
                throw new DCELOpFailedSafeException($"Cannot add vertex {vertexPosition} without any connections");
            }

            // Find vertices for positions
            var connections = new List<Vertex>();
            foreach (var connectionPosition in connectionPositions) {
                var connection = FindVertex(connectionPosition);
                if (connection == null) {
                    throw new DCELOpFailedSafeException($"Connection vertex at {connectionPosition} could not be found, cannot add vertex {vertexPosition}");
                }
                connections.Add(connection);
            }

            // Create vertex and connect it to first connection
            var otherEdge = GetCCWClosestEdge(connections[0], vertexPosition - connections[0].Position);
            if (otherEdge == null) {
                throw new DCELOpFailedSafeException($"An already existing edge is too close to new edge between {vertexPosition} and {connections[0]}");
            }
            var vertex = AddVertex(otherEdge, vertexPosition, addChangesToHistory).Neighbour;

            // Connect the rest of the connections
            int connectionN = connections.Count;
            for (int i = 1; i < connectionN; i++) {
                otherEdge = GetCCWClosestEdge(connections[i], vertexPosition - connections[i].Position);
                var vertexEdge = GetCCWClosestEdge(vertex, connections[i].Position - vertexPosition);

                if (otherEdge == null || vertexEdge == null) {
                    throw new DCELOpFailedSevereException($"An already existing edge is too close to new edge between {vertexPosition} and {connections[i]}");
                }

                AddEdge(vertexEdge, otherEdge, addChangesToHistory);
            }
        }

        /// <summary>
        /// Adds vertex at <paramref name="position"/> and connect it to origin of <paramref name="e1"/>.
        /// </summary>
        /// <param name="e1">Half-edge whose origin will be connected with vertex at <paramref name="position"/>.</param>
        /// <param name="position">Position at which a new vertex will be created and connected with <paramref name="e1"/>'s origin.</param>
        /// <returns>New half-edge with origin at <paramref name="e1"/>'s origin.</returns>
        private HalfEdge AddVertex(HalfEdge e1, Vector2 position, bool addChangesToHistory) {
            // Check if edge is not too small
            if (new LineSegment(e1.Origin.Position, position).IsTooSmall(Util.cm * ScaleInverse)) {
                return null;
            }

            Face face = e1.Face;

            var v1 = e1.Origin;
            var v2 = AddVertex(position).Value;

            var forward = AddHalfEdge().Value;
            var backward = AddHalfEdge().Value;
            // Twin
            forward.Twin = backward;
            backward.Twin = forward;
            // Origin
            forward.Origin = v1;
            backward.Origin = v2;
            // Connect around v1
            ConnectEdges(e1.PreviousHEdge, forward, backward, e1);
            // Connect around v2
            forward.NextHEdge = backward;
            backward.PreviousHEdge = forward;
            // Faces
            forward.Face = face;
            backward.Face = face;
            // Vertex edge
            v2.Edge = backward;

            if (addChangesToHistory) {
                var connections = new List<Vector2>(1);
                connections.Add(v1.Position);
                history.AddEdit(new DCELEditVertex(this, position, connections));
            }

            return forward;
        }

        /// <summary>
        /// Creates vertices at <paramref name="pos1"/> and <paramref name="pos2"/>, connects them with an edge and places the edge on <paramref name="face"/>.
        /// </summary>
        /// <param name="face">Face on which to place the new edge.</param>
        /// <param name="pos1">Position of one of the new vertices of the new edge.</param>
        /// <param name="pos2">Position of one of the new vertices of the new edge.</param>
        /// <returns>New half-edge with origin at vertex at <paramref name="pos1"/>.</returns>
        private HalfEdge PlaceEdge(Face face, LineSegment newEdge, bool addChangesToHistory) {
            var v1 = AddVertex(newEdge.A).Value;
            var v2 = AddVertex(newEdge.B).Value;

            HalfEdge forward = AddHalfEdge().Value;
            HalfEdge backward = AddHalfEdge().Value;
            // Twin
            forward.Twin = backward;
            backward.Twin = forward;
            // Origin
            forward.Origin = v1;
            backward.Origin = v2;
            // back <-> forw
            backward.NextHEdge = forward;
            forward.PreviousHEdge = backward;
            // forw <-> backw
            forward.NextHEdge = backward;
            backward.PreviousHEdge = forward;
            // Vertices
            v1.Edge = forward;
            v2.Edge = backward;
            // Faces
            forward.Face = face;
            backward.Face = face;

            face.InnerComponents.AddLast(forward);

            if (addChangesToHistory) {
                history.AddEdit(new DCELEditEdge(this, DCELEditEdge.EditType.Place, newEdge));
            }

            return forward;
        }

        private HalfEdge AddEdge(Vertex v1, Vertex v2, Face inner, Face outer) {
            var forward = AddHalfEdge().Value;
            var backward = AddHalfEdge().Value;

            forward.Origin = v1;
            backward.Origin = v2;

            forward.Twin = backward;
            backward.Twin = forward;

            forward.Face = inner;
            backward.Face = outer;

            v1.Edge = forward;

            return forward;
        }

        #endregion

        #region Element removal

        public void RemoveEdge(LineSegment edge) {
            var vertexA = FindVertex(edge.A);

            if (vertexA == null) {
                throw new DCELOpFailedSafeException($"Vertex for point A {edge.A} could not be found, cannot remove edge");
            }

            var vertexEdges = vertexA.Edges;
            foreach (var vertexEdge in vertexEdges) {
                var vertexB = vertexEdge.Neighbour;
                if (vertexB.Position.EqualsSimple(edge.B)) {
                    RemoveEdge(vertexEdge);

                    if (vertexA.Edge == null) {
                        RemoveVertex(vertexA);
                    }
                    if (vertexB.Edge == null) {
                        RemoveVertex(vertexB);
                    }

                    return;
                }
            }

            throw new DCELOpFailedSafeException($"Could not find a suitable edge to remove between {edge.A} and {edge.B}");
        }

        private void RemoveEdge(HalfEdge forward) {
            var backward = forward.Twin;
            string forwardFormatted = forward.ToString();
            if (forward.Face == null || backward.Face == null) {
                throw new DCELOpFailedSafeException($"Cannot remove a border edge (removing {forwardFormatted})");
            }

            // Disonnect around v1
            var e1 = DisconnectFromVertex(forward.Origin, forward, backward);
            // Disonnect around v2
            var e2 = DisconnectFromVertex(backward.Origin, backward, forward);

            // Disconnect face
            // Forward and backward belong to the same face
            if (forward.Face == backward.Face) {
                var face = forward.Face;

                // We are disconnecting 2 seperate components
                if (e1 != null && e2 != null) {
                    var loop1 = e1.EdgeLoop;
                    var loop2 = e2.EdgeLoop;
                    var allHEdges = new List<HalfEdge>(loop1.Count + loop2.Count + 2);
                    allHEdges.AddRange(loop1);
                    allHEdges.AddRange(loop2);
                    allHEdges.Add(forward);
                    allHEdges.Add(backward);

                    // The current component was an outer component => 2 new components will be outer and inner
                    // (there can't be 2 outer components)
                    if (allHEdges.Contains(face.OuterComponent)) {
                        // Decide which component is outer by checking which has the highest vertex
                        // Find highest vertex Y coordinate in e1's component
                        float e1HighestVertexY = float.NegativeInfinity;
                        foreach (var edge in loop1) {
                            float vertexY = edge.Origin.Position.y;
                            if (vertexY > e1HighestVertexY) {
                                e1HighestVertexY = vertexY;
                            }
                        }

                        // Check if e2's component has a higher vertex
                        bool e1IsOuter = true;
                        foreach (var edge in loop2) {
                            float vertexY = edge.Origin.Position.y;
                            if (vertexY > e1HighestVertexY) {
                                e1IsOuter = false;
                                break;
                            }
                        }

                        face.OuterComponent = e1IsOuter ? e1 : e2;
                        face.InnerComponents.AddLast(e1IsOuter ? e2 : e1);
                    }
                    // The current component was an inner component => 2 new components will be both inner
                    else {
                        // Find the old reference
                        for (var innerNode = face.InnerComponents.First; innerNode != null; innerNode = innerNode.Next) {
                            // Remove old reference and add new ones for e1's and e2's components
                            if (allHEdges.Contains(innerNode.Value)) {
                                face.InnerComponents.Remove(innerNode);
                                face.InnerComponents.AddLast(e1);
                                face.InnerComponents.AddLast(e2);
                                break;
                            }
                        }
                    }
                }
                // We are removing the component
                else if (e1 == null && e2 == null) {
                    if (face.OuterComponent.EqualsAny(forward, backward)) {
                        throw new DCELOpFailedSafeException($"Cannot remove outer component completely (removing {forwardFormatted})");
                    }

                    for (var innerNode = face.InnerComponents.First; innerNode != null; innerNode = innerNode.Next) {
                        if (innerNode.Value.EqualsAny(forward, backward)) {
                            face.InnerComponents.Remove(innerNode);
                            break;
                        }
                    }
                }
                // We are disconnecting a vertex
                else {
                    var replacement = e1 == null ? e2 : e1;
                    if (face.OuterComponent.EqualsAny(forward, backward)) {
                        face.OuterComponent = replacement;
                    }
                    else {
                        for (var innerNode = face.InnerComponents.First; innerNode != null; innerNode = innerNode.Next) {
                            if (innerNode.Value.EqualsAny(forward, backward)) {
                                innerNode.Value = replacement;
                                break;
                            }
                        }
                    }
                }
            }
            // Forward and backward belong to different faces, so merge faces into one
            else {
                if (e1 == null || e2 == null) {
                    throw new DCELInvalidException($"Tried removing edge where both faces are different, but at least one vertex is connected only to this edge (removing {forwardFormatted})");
                }

                var newCompLoop = e1.EdgeLoop;

                // Sanity check
                if (!newCompLoop.Contains(e2)) {
                    throw new DCELOpFailedSevereException($"Removed edge with different faces, but newly created edge loop does not contain edges connected to origin vertices (removing {forwardFormatted})");
                }

                var f1 = e1.Face;
                var f2 = e2.Face;

                bool e1IsOuter = backward == f1.OuterComponent || newCompLoop.Contains(f1.OuterComponent);
                bool e2IsOuter = forward == f2.OuterComponent || newCompLoop.Contains(f2.OuterComponent);

                // Both were outer, so new component is outer too
                if (e1IsOuter && e2IsOuter) {
                    // Replace reference to component if needed
                    if (f2.OuterComponent == forward) {
                        f2.OuterComponent = e2;
                    }
                }
                else if (!e1IsOuter && !e2IsOuter) {
                    throw new DCELOpFailedSevereException($"Removed edge with different faces, but both components were inner (removing {forwardFormatted})");
                }
                // One component was outer, one inner, new component will be inner
                else {
                    // Swap so that f1 is outer and f2 is inner
                    if (e2IsOuter) {
                        Util.Swap(ref f1, ref f2);
                        Util.Swap(ref forward, ref backward);
                        Util.Swap(ref e1, ref e2);
                    }

                    for (var innerNode = f2.InnerComponents.First; innerNode != null; innerNode = innerNode.Next) {
                        // Replace reference to component if needed
                        if (innerNode.Value == forward) {
                            innerNode.Value = e2;
                            break;
                        }
                    }
                }

                // Replace references to f1 with f2 and add inner comps from f1 to f2
                newCompLoop.ForEach(e => e.Face = f2);
                foreach (var inner in f1.InnerComponents) {
                    inner.EdgeLoop.ForEach(e => e.Face = f2);
                    f2.InnerComponents.AddLast(inner);
                }

                DeleteFace(f1);
            }

            // Delete the hedges
            DeleteHalfEdge(forward);
            DeleteHalfEdge(backward);
        }

        private HalfEdge DisconnectFromVertex(Vertex vertex, HalfEdge forward, HalfEdge backward) {
            var prev = forward.PreviousHEdge;
            var next = backward.NextHEdge;

            // Vertex is connected only to forward and backward
            if (prev == backward) {
                vertex.Edge = null;
                return null;
            }
            // Vertex has at least one other edge connected to it
            else {
                prev.NextHEdge = next;
                if (vertex.Edge == forward) {
                    vertex.Edge = next;
                }

                next.PreviousHEdge = prev;

                return next;
            }
        }

        public void RemoveVertex(Vector2 vertexCoords) {
            var vertex = FindVertex(vertexCoords);

            if (vertex == null) {
                throw new DCELOpFailedSafeException($"Vertex with coords {vertexCoords} could not be found, cannot remove it");
            }

            RemoveVertex(vertex);
        }

        private void RemoveVertex(Vertex vertex) {
            if (vertex.Edge == null) {
                DeleteVertex(vertex);
            }
            else {
                // Remove all conected edges
                var vertexEdges = vertex.Edges;
                var vertexNeighs = vertex.Neighbours;
                foreach (var edge in vertexEdges) {
                    RemoveEdge(edge);
                }

                if (vertex.Edge != null) {
                    throw new DCELOpFailedSevereException($"Vertex edge is not null after removing all connected edges {vertex}");
                }

                foreach (var neigh in vertexNeighs) {
                    if (neigh.Edge == null) {
                        RemoveVertex(neigh);
                    }
                }

                // Delete the vertex
                DeleteVertex(vertex);
            }
        }

        #endregion

        #region Queries

        /// <summary>
        /// Returns edge connected to <paramref name="v"/> with smallest ccw angle (range <0,360>) to <paramref name="to"/>.
        /// </summary>
        /// <param name="v">Vertex with which the returned edge is connected.</param>
        /// <param name="to">Vector to which the angle of the returned edge will be smallest to.</param>
        /// <returns>Edge connected to <paramref name="v"/> with smallest ccw angle (range <0,360>) to <paramref name="to"/>.</returns>
        private HalfEdge GetCCWClosestEdge(Vertex v, Vector2 to) {
            if (to == Vector2.zero) {
                return null;
            }

            float minAngle = 370.0f;
            float maxAngle = -1.0f;
            HalfEdge minAngleEdge = null;

            foreach (var e in v.Edges) {
                // CCW angle between an edge and to vector
                var angle = Util.CCWAngle(e.LineSegment.Direction, to);
                if (angle < minAngle) {
                    minAngle = angle;
                    minAngleEdge = e;
                }
                if (angle > maxAngle) {
                    maxAngle = angle;
                }
            }

            // Ignore if an existing edge and the edge we are creating would be to closely parallel
            if (minAngle < angleDelta || 360.0f - angleDelta < maxAngle) {
                return null;
            }
            else {
                return minAngleEdge;
            }
        }

        /// <summary>
        /// Intersects <paramref name="segment"/> with the DCEL graph and saves intersections to <paramref name="intersections"/>.
        /// </summary>
        /// <param name="segment">Line segment with which to intersect with.</param>
        /// <param name="intersections">List of intersections. Empty if no intersections are found.</param>
        /// <returns><see langword="true"/> if intersected at least once, <see langword="false"/> otherwise.</returns>
        private bool Intersect(LineSegment segment, out List<Intersection> intersections) {
            intersections = new List<Intersection>();
            var visited = new HashSet<HalfEdge>();

            // Go through half edges
            foreach (var edge in halfEdges) {
                // If edge hasn't been visited
                if (!visited.Contains(edge)) {
                    // Intersect with the edge
                    if (edge.LineSegment.Intersect(segment, out float intersectionAlpha)) {
                        intersections.Add(new Intersection(edge, intersectionAlpha));
                    }

                    // Add the edge to visited (must add both current half edge and twin half edge)
                    visited.Add(edge);
                    visited.Add(edge.Twin);
                }
            }

            return intersections.Count > 0;
        }

        /// <summary>
        /// Returns <see langword="true"/> if <paramref name="pos"/> is inside one of the face of this DCEL, false otherwise.
        /// </summary>
        /// <param name="pos">Point to be tested.</param>
        /// <returns><see langword="true"/> if <paramref name="pos"/> is inside one of the face of this DCEL, false otherwise.</returns>
        public bool IsInside(Vector2 pos) {
            return Polygon.PointInPolygon(boundary, holes, pos);
        }

        /// <summary>
        /// Returns the position of the nearest vertex relative to <paramref name="pos"/>, or a poisoned vector if the nearest vertex is furhter than <paramref name="maxDist"/>.
        /// </summary>
        /// <param name="pos">To which coords should the nearest vertex be found.</param>
        /// <param name="maxDist">The maximum distance of the nearest vertex.</param>
        /// <returns>Position of the nearest vertex relative to <paramref name="pos"/>, or a poisoned vector if the nearest vertex is furhter than <paramref name="maxDist"/></returns>
        public Vector2 NearestVertexCoords(Vector2 pos, float maxDist = float.PositiveInfinity) {
            maxDist *= ScaleInverse;
            var vertex = NearestVertex(pos, maxDist);
            if (vertex == null) {
                Vector2 pp = new Vector2();
                pp.Poison();
                return pp;
            }
            else {
                return vertex.Position;
            }
        }

        private Vertex NearestVertex(Vector2 pos, float maxDist = float.PositiveInfinity) {
            float maxDistSqrd = maxDist * maxDist;
            float nearestDistSqrd = float.PositiveInfinity;
            Vertex nearest = null;

            foreach (var vertex in vertices) {
                float distSqrd = pos.DistanceSqrd(vertex.Position);
                if (distSqrd < maxDistSqrd && distSqrd < nearestDistSqrd) {
                    nearestDistSqrd = distSqrd;
                    nearest = vertex;
                }
            }

            return nearest;
        }

        private Vertex FindVertex(Vector2 pos) {
            foreach (var vertex in vertices) {
                if (vertex.Position.EqualsSimple(pos)) {
                    return vertex;
                }
            }

            return null;
        }

        private HalfEdge FindEdge(LineSegment edgePoints) {
            var vertexA = FindVertex(edgePoints.A);
            var vertexB = FindVertex(edgePoints.B);

            if (vertexA == null || vertexB == null) {
                return null;
            }

            foreach (var edge in vertexA.Edges) {
                if (edge.Neighbour == vertexB) {
                    return edge;
                }
            }

            return null;
        }

        #endregion

        #region Other

        /// <summary>
        /// Calculates interpolation value for DCEL visualization. See <see href="https://www.desmos.com/calculator/negp53e3fv"/> for more info.
        /// </summary>
        /// <param name="A1">Origin of edge (<paramref name="A1"/>, <paramref name="A2"/>)</param>
        /// <param name="v1">Bisector of angle between normals of edge (<paramref name="A1"/>, <paramref name="A2"/>) and its previous edge.</param>
        /// <param name="A2">Neighbour of edge (<paramref name="A1"/>, <paramref name="A2"/>)</param>
        /// <param name="v2">Bisector of angle between normals of edge (<paramref name="A1"/>, <paramref name="A2"/>) and its next edge.</param>
        /// <param name="pointDist"></param>
        /// <returns>Calculated interpolation value.</returns>
        private float GetIntersectionAlpha(Vector2 A1, Vector2 v1, Vector2 A2, Vector2 v2, float pointDist) {
            float denom = v1.x * v2.y - v1.y * v2.x;
            if (denom == 0.0f) {
                return 0.0f;
            }

            float alpha = (v2.x * (A1.y - A2.y) - v2.y * (A1.x - A2.x)) / denom;
            return alpha < 5 * pointDist ? alpha : 0.0f;
        }

        #endregion
    }
}
