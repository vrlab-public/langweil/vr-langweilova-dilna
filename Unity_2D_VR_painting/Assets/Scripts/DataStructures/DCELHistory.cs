using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DataStructures
{
    public class DCELHistory
    {
        private int time = 0;
        public int Time => time;

        private readonly Stack<DCELEditBase> editHistory = new Stack<DCELEditBase>();

        private readonly Dictionary<DCEL, List<DCEL>> resurrectionQueue = new Dictionary<DCEL, List<DCEL>>();
        public Dictionary<DCEL, List<DCEL>> ResurrectionQueue => resurrectionQueue;

        public DCELHistory() {}

        public void Undo(out List<DCEL> affectedInstances) {
            affectedInstances = null;
            if (time == 0) {
                return;
            }

            --time;

            while (editHistory.Count > 0) {
                if (editHistory.Peek().time == time) {
                    if (affectedInstances == null) {
                        affectedInstances = new List<DCEL>();
                    }

                    var edit = editHistory.Pop();
                    affectedInstances.Add(edit.instance);
                    edit.Revert();
                }
                else {
                    break;
                }
            }
        }

        public void AddEdit(DCELEditBase edit) {
            editHistory.Push(edit);
        }

        public void AdvanceTime() {
            if (editHistory.Count > 0 && editHistory.Peek().time == time) {
                ++time;
            }
        }
    }
}
