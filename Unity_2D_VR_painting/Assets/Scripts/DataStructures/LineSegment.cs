using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;

namespace DataStructures
{
    public readonly struct LineSegment : System.IEquatable<LineSegment>, System.IFormattable {
        public readonly Vector2 A;
        public readonly Vector2 B;

        public Vector2 Direction => B - A;

        public float LengthSqrd {
            get {
                float x = B.x - A.x;
                float y = B.y - A.y;
                return x * x + y * y;
            }
        }

        public float Length => Mathf.Sqrt(LengthSqrd);

        public float Slope => Mathf.Approximately(A.x, B.x) ? float.PositiveInfinity : (Mathf.Approximately(A.y, B.y) ? 0.0f : (B.y - A.y) / (B.x - A.x));

        public LineSegment(Vector2 a, Vector2 b) {
            if (a.EqualsSimple(b)) {
                throw new System.ArgumentException("Point B cannot be equal to point A", nameof(b));
            }
            A = a;
            B = b;
        }

        public static implicit operator LineSegment((Vector2 a, Vector2 b) segmentTuple) => new LineSegment(segmentTuple.a, segmentTuple.b);
        public static implicit operator (Vector2 a, Vector2 b)(LineSegment lineSegment) => (lineSegment.A, lineSegment.B);

        /// <summary>
        /// Returns true if the <paramref name="point"/> lies on the left of this half-edge.
        /// </summary>
        /// <param name="point"></param>
        /// <returns><see langword="true"/> if <paramref name="point"/> lies on the left of this half-edge, <see langword="false"/> otherwise.</returns>
        public bool IsLeftOfLine(Vector2 point) {
            var lineDir = Direction;
            var pointDir = point - A;
            return lineDir.x * pointDir.y - pointDir.x * lineDir.y > 0.0f;
        }

        public bool IsInbetweenEndpoints(Vector2 point) {
            float slope = Slope;
            int sortCoord = (slope != float.PositiveInfinity && Mathf.Abs(slope) < 1.0f) ? 0 : 1;
            if (A[sortCoord] < B[sortCoord]) {
                return A[sortCoord] < point[sortCoord] && point[sortCoord] < B[sortCoord];
            }
            else {
                return B[sortCoord] < point[sortCoord] && point[sortCoord] < A[sortCoord];
            }
        }

        public bool IsTooSmall(float smallestCutMag) {
            float angleDenom = Mathf.Sqrt(Direction.sqrMagnitude * (smallestCutMag * smallestCutMag));
            if (angleDenom <= Vector2.kEpsilon) {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Calculates <paramref name="intersection"/> with <paramref name="separatorSegment"/> and returns <see langword="true"/> if they intersect, otherwise returns <see langword="false"/>.
        /// </summary>
        /// <remarks>
        /// Assumes <paramref name="separatorSegment"/> is "separating" <see langword="this"/>, so <paramref name="intersection"/> always lies on <see langword="this"/>.
        /// </remarks>
        /// <exception cref="DCELException"> Thrown when segments are parallel.</exception>
        /// <param name="separatorSegment">Second line segment.</param>
        /// <param name="intersection">Coordinates of the intersection.</param>
        /// <returns>True if line segments intersect, false otherwise.</returns>
        public bool Intersect(LineSegment separatorSegment, out float intersectionAlpha) {
            intersectionAlpha = float.NaN;

            var baseDir = Direction;
            var sepDirInverse = -separatorSegment.Direction;
            var segmentsVec = A - separatorSegment.A;

            float[] numerators = { sepDirInverse.y * segmentsVec.x - sepDirInverse.x * segmentsVec.y, baseDir.x * segmentsVec.y - baseDir.y * segmentsVec.x };
            float denominator = baseDir.y * sepDirInverse.x - baseDir.x * sepDirInverse.y;

            int[] signNums = { (int)Mathf.Sign(numerators[0]), (int)Mathf.Sign(numerators[1]) };
            int signDenom = (int)Mathf.Sign(denominator);

            // Segments are not parallel
            if (signDenom != 0) {
                // Check if any pair of endpoints are the same (after checking if the segments aren't parallel)
                if (A.EqualsSimple(separatorSegment.A) || A.EqualsSimple(separatorSegment.B)) {
                    intersectionAlpha = 0.0f;
                    return true;
                }
                else if (B.EqualsSimple(separatorSegment.A) || B.EqualsSimple(separatorSegment.B)) {
                    intersectionAlpha = 1.0f;
                    return true;
                }

                for (int i = 0; i < 2; i++) {
                    // Alpha would be less than 0
                    if (signNums[i] != 0 && signNums[i] != signDenom) return false;
                }
            }

            float angleDenom = Mathf.Sqrt(baseDir.sqrMagnitude * sepDirInverse.sqrMagnitude);
            if (angleDenom <= Vector2.kEpsilon) {
                if (baseDir.sqrMagnitude < sepDirInverse.sqrMagnitude) {
                    throw new DCELInvalidException($"Edge {this} is too small ({baseDir.magnitude}) for intersecting");
                }
                else {
                    throw new DCELOpFailedSafeException($"Separator {separatorSegment} is too small ({sepDirInverse.magnitude}) for intersecting");
                }
            }

            // Test if the segments are parallel
            if (Mathf.Abs(denominator / angleDenom) <= 0.01f // something below 0.5 degree
                && (IsInbetweenEndpoints(separatorSegment.A) || IsInbetweenEndpoints(separatorSegment.B) || separatorSegment.IsInbetweenEndpoints(A)) // at least 1 endpoint lies inside the other segment
                && Mathf.Abs(Vector2.Angle(baseDir, segmentsVec) - 90.0f) > 89.5f // both segments lie on the same line
                ) {
                throw new DCELParallelException($"Segments {this} & {separatorSegment} are parallel");
            }
            // The segments are parallel but don't intersect
            else if (denominator == 0.0f) {
                return false;
            }

            // Calculate exact intersection point
            float alpha = numerators[0] / denominator;
            float beta = numerators[1] / denominator;

            // We know that they are >= 0, so test for <= 1
            if (alpha <= 1.0f && beta <= 1.0f) {
                //float vec1mag = baseDir.magnitude;
                //float actualDist = alpha * vec1mag;
                var intersectionPoint = A + alpha * Direction;

                // Treat alpha as zero
                //if (intersectionPoint.EqualsSimple(A)) {
                if (intersectionPoint == A) {
                    intersectionAlpha = 0.0f;
                }
                // Treat alpha as one
                //else if (intersectionPoint.EqualsSimple(B)) {
                else if (intersectionPoint == B) {
                    intersectionAlpha = 1.0f;
                }
                // Calculate intersection point
                else {
                    intersectionAlpha = alpha;
                }
            }
            else {
                return false;
            }

            return true;
        }

        #region System methods

        public bool Equals(LineSegment other) {
            return this == other;
        }

        public override bool Equals(object obj) {
            return obj is LineSegment && this == (LineSegment)obj;
        }

        public static bool operator ==(LineSegment lhs, LineSegment rhs) {
            return lhs.A.x == rhs.A.x && lhs.A.y == rhs.A.y && lhs.B.x == rhs.B.x && lhs.B.y == rhs.B.y;
        }

        public static bool operator !=(LineSegment x, LineSegment y) {
            return !(x == y);
        }

        public override int GetHashCode() {
            return (A, B).GetHashCode();
        }

        public override string ToString() {
            return ToString("G", System.Globalization.CultureInfo.CurrentCulture);
        }

        public string ToString(string format, System.IFormatProvider formatProvider) {
            if (string.IsNullOrEmpty(format)) format = "G";
            if (formatProvider == null) formatProvider = System.Globalization.CultureInfo.CurrentCulture;

            var sb = new System.Text.StringBuilder();
            sb.Append("[");

            sb.Append(A.ToString(format, formatProvider));
            sb.Append(", ");
            sb.Append(B.ToString(format, formatProvider));

            sb.Append("]");

            return sb.ToString();
        }

        #endregion
    }
}
