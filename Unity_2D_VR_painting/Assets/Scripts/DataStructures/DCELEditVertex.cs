using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DataStructures
{
    public class DCELEditVertex : DCELEditBase
    {
        protected readonly Vector2 vertexCoords;
        protected readonly List<Vector2> connections;

        public DCELEditVertex(DCEL instance, Vector2 vertexCoords, List<Vector2> connections) : base(instance) {
            this.vertexCoords = vertexCoords;
            this.connections = connections ?? throw new ArgumentNullException(nameof(connections), "Vertex has to have connections");
        }

        public override void Apply() {
            instance.AddVertex(vertexCoords, connections, false);
        }

        public override void Revert() {
            instance.RemoveVertex(vertexCoords);
        }
    }
}
