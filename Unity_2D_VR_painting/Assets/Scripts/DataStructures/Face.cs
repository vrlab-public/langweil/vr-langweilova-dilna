using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DataStructures
{

    /// <summary>
    /// Class for representing DCEL faces.
    /// </summary>
    public class Face
    {
        private HalfEdge outerComponent;
        /// <summary>
        /// Half-edge that's part of the component enclosing this face.
        /// </summary>
        public HalfEdge OuterComponent {
            get => outerComponent;
            set => outerComponent = value;
        }

        private readonly LinkedList<HalfEdge> innerComponents = new LinkedList<HalfEdge>();
        /// <summary>
        /// List of half-edges where each one is part of exactly one component inside this face.
        /// </summary>
        public LinkedList<HalfEdge> InnerComponents => innerComponents;

        /// <summary>
        /// Returns outer and then all inner components.
        /// </summary>
        public IEnumerable<HalfEdge> AllComponents {
            get {
                yield return outerComponent;
                foreach (var innerComponent in innerComponents) {
                    yield return innerComponent;
                }
            }
        }

        public Face() {}
    }
}
