using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DataStructures
{
    public class DCELEditEdge : DCELEditBase
    {
        public enum EditType
        {
            Add, Place
        }

        protected readonly EditType editType;
        protected readonly LineSegment edgeSegment;

        public DCELEditEdge(DCEL instance, EditType editType, LineSegment edgeSegment) : base(instance) {
            this.editType = editType;
            this.edgeSegment = edgeSegment;
        }

        public override void Apply() {
            switch (editType) {
                case EditType.Add:
                    instance.AddEdge(edgeSegment, false);
                    break;
                case EditType.Place:
                    instance.PlaceEdge(edgeSegment, out _, false);
                    break;
                default:
                    break;
            }
        }

        public override void Revert() {
            instance.RemoveEdge(edgeSegment);
        }
    }
}
