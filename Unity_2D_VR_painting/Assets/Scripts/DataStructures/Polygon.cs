using DataStructures;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DataStructures
{
    /// <summary>
    /// Oriented open list of vertices defining a closed polygon.
    /// </summary>
    public class Polygon : IFormattable
    {
        private readonly List<Vector2> vertices = new List<Vector2>();
        public IList<Vector2> VerticesRO => vertices.AsReadOnly();

        private bool ccw;
        public bool CCW => ccw;

        /// <summary>
        /// Constructs polygon from <paramref name="vertices"/> and calculates orientation.
        /// </summary>
        /// <param name="vertices">Polygon vertices.</param>
        public Polygon(List<Vector2> vertices)
            : this(vertices, IsCCW(vertices)) {
        }

        /// <summary>
        /// Constructs polygon from <paramref name="vertices"/> with given orientation <paramref name="ccw"/>.
        /// </summary>
        /// <param name="vertices">Polygon vertices.</param>
        /// <param name="ccw">Polygon orientation (does NOT check if this is correct).</param>
        public Polygon(List<Vector2> vertices, bool ccw) {
            if (vertices.Count < 3) {
                throw new ArgumentException("Polygon must have at least 3 vertices", "vertices");
            }

            this.vertices = vertices;
            this.ccw = ccw;
        }

        private static bool IsCCW(List<Vector2> vertices) {
            Vector2 leftmostVertex = Vector2.positiveInfinity;
            int leftmostIDX = 0;

            for (int i = 0; i < vertices.Count; ++i) {
                Vector2 currentVertex = vertices[i];
                // Leftmost vertex test
                if (currentVertex.x < leftmostVertex.x || (currentVertex.x == leftmostVertex.x && currentVertex.y < leftmostVertex.y)) {
                    leftmostVertex = currentVertex;
                    leftmostIDX = i;
                }
            }

            return IsCCW(vertices[(leftmostIDX - 1 + vertices.Count) % vertices.Count], vertices[leftmostIDX], vertices[(leftmostIDX + 1) % vertices.Count]);
        }

        private static bool IsCCW(Vector2 a, Vector2 b, Vector2 c) {
            // Determinant of matrix with vectors b-a and c-b
            return (b.x - a.x) * (c.y - a.y) - (c.x - a.x) * (b.y - a.y) > 0.0f;
        }

        /// <summary>
        /// Constructs polygon from an edge loop.
        /// </summary>
        /// <remarks>
        /// All edges that should be on the polygon and the starting edge must have a different face from their twin.
        /// Ignores all edges that are connected to the starting edge via a bridge.
        /// </remarks>
        /// <param name="edgeLoop">Edge loop from which the polygon should be constructed from.</param>
        /// <returns>Constructed polygon or <see langword="null"/> if the start edge is a bridge.</returns>
        public static Polygon FromEdgeLoop(List<HalfEdge> edgeLoop) {
            if (edgeLoop[0].Twin.Face == edgeLoop[0].Face) {
                return null;
            }

            // Save leftmost (and downmost if needed) vertex of the polygon
            Vector2 leftmostVertex = Vector2.positiveInfinity;
            int leftmostIDX = 0;

            var polygonVertices = new List<Vector2>();

            for (int i = 0; i < edgeLoop.Count; ++i) {
                var edge = edgeLoop[i];
                // Add to polygon only if both half-edges belong to different faces
                if (edge.Twin.Face != edge.Face) {
                    Vector2 pos = edge.Origin.Position;
                    polygonVertices.Add(pos);
                
                    // Leftmost vertex test
                    if (pos.x < leftmostVertex.x || (pos.x == leftmostVertex.x && pos.y < leftmostVertex.y)) {
                        leftmostVertex = pos;
                        leftmostIDX = polygonVertices.Count - 1;
                    }
                }
                // Skip all edges until coming across the same vertex
                else {
                    var origin = edge.Origin;
                    do {
                        ++i;
                        if (!(i < edgeLoop.Count)) {
                            break;
                        }
                        edge = edgeLoop[i];
                    } while (edge.Origin != origin);
                    --i;
                }
            }

            if (polygonVertices.Count < 3) {
                return null;
            }

            return new Polygon(polygonVertices, IsCCW(
                polygonVertices[(leftmostIDX - 1 + polygonVertices.Count) % polygonVertices.Count],
                polygonVertices[leftmostIDX],
                polygonVertices[(leftmostIDX + 1) % polygonVertices.Count]
                ));
        
            //float signedArea = 0.0f;
            //var previousPos = outerEdgeLoop[outerEdgeLoop.Count - 1].Value.Origin.Value.Position;
            //var pos = outerEdgeLoop[0].Value.Origin.Value.Position;
            //var nextPos = outerEdgeLoop[1].Value.Origin.Value.Position;
            //signedArea += pos.x * (nextPos.y - previousPos.y);
            //bool ccwSignedAreaBased = signedArea > 0.0f;
        }

        /// <summary>
        /// Reverses orientation of the polygon by reveresing the vertices.
        /// </summary>
        public void ReverseOrientation() {
            ccw = !ccw;
            vertices.Reverse();
        }

        #region System methods

        public override string ToString() {
            return ToString("G", System.Globalization.CultureInfo.CurrentCulture);
        
        }

        public string ToString(string format, IFormatProvider formatProvider) {
            if (String.IsNullOrEmpty(format)) format = "G";
            if (formatProvider == null) formatProvider = System.Globalization.CultureInfo.CurrentCulture;

            var sb = new System.Text.StringBuilder("[");

            foreach (var vertex in vertices) {
                sb.Append(vertex.ToString(format, formatProvider));
                sb.Append(", ");
            }
            sb.Remove(sb.Length - 2, 2);

            sb.Append("]");

            return sb.ToString();
        }

        #endregion

        /// <summary>
        /// Returns <see langword="true"/> if <paramref name="point"/> is inside the polygon.
        /// </summary>
        /// <remarks>
        /// Code taken from https://wrf.ecse.rpi.edu/Research/Short_Notes/pnpoly.html and converted to C#.
        /// </remarks>
        /// <param name="point">Point to be tested.</param>
        /// <returns><see langword="true"/> if <paramref name="point"/> is inside the polygon.</returns>
        public bool PointInPolygon(Vector2 point) {
            bool inside = false;
            for (int i = 0, j = vertices.Count - 1; i < vertices.Count; j = i++) {
                if (((vertices[i].y > point.y) != (vertices[j].y > point.y)) &&
                 (point.x < (vertices[j].x - vertices[i].x) * (point.y - vertices[i].y) / (vertices[j].y - vertices[i].y) + vertices[i].x))
                    inside = !inside;
            }
            return inside;
        }

        /// <summary>
        /// Returns <see langword="true"/> if <paramref name="point"/> is inside <paramref name="boundary"/> and outside <paramref name="holes"/>.
        /// </summary>
        /// <param name="boundary">Boundary polygon.</param>
        /// <param name="holes">Polygons defining the holes.</param>
        /// <param name="point">Point to be tested.</param>
        /// <returns<see langword="true"/> if <paramref name="point"/> is inside <paramref name="boundary"/> and outside <paramref name="holes"/>.></returns>
        public static bool PointInPolygon(Polygon boundary, List<Polygon> holes, Vector2 point) {
            // Test if the point is inside the closed polygon
            if (boundary.PointInPolygon(point)) {
                // Test if the point is inside any holes
                foreach (var hole in holes) {
                    if (hole.PointInPolygon(point)) {
                        // Point is inside the polygon's hole, so not inside the polygon
                        return false;
                    }
                }

                // The point is inside the polygon boundary but not inside any holes,
                // so it lies on the polygon's surface == is inside the polygon
                return true;
            }

            // The point is outside of the polygon's boundary, so not inside the polygon
            return false;
        }

        /// <summary>
        /// Converts the polygon to <see cref="LibTessDotNet"/> contour.
        /// </summary>
        /// <param name="PositionToData">Method to convert position to vertex data.</param>
        /// <returns><see cref="LibTessDotNet"/> contour corresponding the this polygon.</returns>
        public LibTessDotNet.ContourVertex[] ToContour(Func<Vector2, object> PositionToData) {
            var contour = new LibTessDotNet.ContourVertex[vertices.Count];
            for (int i = 0; i < vertices.Count; ++i) {
                contour[i].Position = new LibTessDotNet.Vec3(vertices[i].x, vertices[i].y, 0.0f);
                contour[i].Data = PositionToData(vertices[i]);
            }

            return contour;
        }
    }
}
