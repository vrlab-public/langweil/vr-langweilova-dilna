using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DataStructures
{
    /// <summary>
    /// Class for representing DCEL half-edges.
    /// </summary>
    public class HalfEdge : System.IFormattable
    {
        private Vertex origin;
        /// <summary>
        /// Vertex from which this half-edge originates.
        /// </summary>
        public Vertex Origin {
            get => origin;
            set => origin = value;
        }

        private HalfEdge twin;
        /// <summary>
        /// This half-edges twin (=other half of the edge)
        /// </summary>
        public HalfEdge Twin {
            get => twin;
            set => twin = value;
        }

        private Face face;
        /// <summary>
        /// Incident face.
        /// </summary>
        public Face Face {
            get => face;
            set => face = value;
        }

        private HalfEdge next;
        /// <summary>
        /// Next half-edge.
        /// </summary>
        public HalfEdge NextHEdge {
            get => next;
            set => next = value;
        }

        private HalfEdge previous;
        /// <summary>
        /// Previous half-edge.
        /// </summary>
        public HalfEdge PreviousHEdge {
            get => previous;
            set => previous = value;
        }

        /// <summary>
        /// Returns sequence of half-edges originating from this half-edge that form a loop. All half-edges should have the same incident face.
        /// </summary>
        public List<HalfEdge> EdgeLoop {
            get {
                var loop = new List<HalfEdge>();

                var current = next.previous;
                do {
                    loop.Add(current);
                    current = current.NextHEdge;
                } while (current != this);

                return loop;
            }
        }

        /// <summary>
        /// Neighbouring vertex of this half-edge, AKA origin of <see cref="Twin"/>.
        /// </summary>
        public Vertex Neighbour => twin.origin;

        /// <summary>
        /// Neighbouring half-edge in clockwise direction.
        /// </summary>
        public HalfEdge EdgeNeighbourCW => twin.next;

        /// <summary>
        /// Neighbouring half-edge in counter-clockwise direction.
        /// </summary>
        public HalfEdge EdgeNeighbourCCW => previous.twin;

        public LineSegment LineSegment => new LineSegment(origin.Position, Neighbour.Position);

        public HalfEdge() {
        }

        #region System methods

        public override string ToString() {
            return ToString("G", System.Globalization.CultureInfo.CurrentCulture);
        }

        public string ToString(string format, System.IFormatProvider formatProvider) {
            if (string.IsNullOrEmpty(format)) format = "G";
            if (formatProvider == null) formatProvider = System.Globalization.CultureInfo.CurrentCulture;

            var sb = new System.Text.StringBuilder("[");

            sb.Append(origin.ToString(format, formatProvider));
            sb.Append(", ");
            sb.Append(Neighbour.ToString(format, formatProvider));

            sb.Append("]");

            return sb.ToString();
        }

        #endregion
    }
}
