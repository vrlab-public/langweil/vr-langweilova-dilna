using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;

namespace Langweil
{
    public class ModelChanger : MonoBehaviour
    {
        [SerializeField]
        [WarnOnNull]
        private List<ModelInfo> modelInfos;

        [SerializeField]
        [WarnOnNull]
        private Material bookMaterial;

        private int currentModel = 0;

        private void Start() {
            this.CheckNull();

            foreach (var mi in modelInfos) {
                mi.mesh.SetActive(false);
            }

            SwitchModel(0);
        }

        private void Update() {
            if (Input.GetKeyUp(KeyCode.M)) {
                SwitchModel(+1);
            }
            else if (Input.GetKeyUp(KeyCode.N)) {
                SwitchModel(-1);
            }
        }

        private void SwitchModel(int change) {
            // Increment index
            int oldIdx = currentModel;
            currentModel = (currentModel + change + modelInfos.Count) % modelInfos.Count;

            // Set new paper and book texture
            foreach (var paper in Paper.PaperRegistry) {
                paper.Drawable.PaperTexture = modelInfos[currentModel].paperTexture;
                // Reset paper from slot to default position
                if (paper.transform.parent != null) {
                    paper.transform.parent = null;
                    paper.transform.SetPositionAndRotation(Calibration.PaperStartPos, Calibration.PaperStartRot);
                }
            }
            bookMaterial.mainTexture = modelInfos[currentModel].bookTexture;

            // Deactivate old and activate new model
            modelInfos[oldIdx].mesh.SetActive(false);
            modelInfos[currentModel].mesh.SetActive(true);
        }
    }
}
