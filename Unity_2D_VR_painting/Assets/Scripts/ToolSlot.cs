using Langweil;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;
using Valve.VR.InteractionSystem;

/// <summary>
/// Component for storing and changing active tool.
/// </summary>
public class ToolSlot : MonoBehaviour
{
    private static Registry<ToolSlot> registry = new HashSetRegistry<ToolSlot>();

    [SerializeField]
    [Tooltip("The tool this slot houses")]
    [WarnOnNull]
    private Interactable tool;

    private void Start() {
        registry.Register(this);
        ResetTool();
    }

    private void OnTriggerEnter(Collider other) {
        if (tool == Calibration.CurrentTool) return;

        // Get tool interactable
        if (other.attachedRigidbody != null && other.attachedRigidbody.TryGetComponent(out Interactable collidedTool)) {
            if (tool == collidedTool) return;

            // Switch active tool to this slot's tool
            Calibration.SwitchTool(tool, Calibration.CurrentHand);
            // Find tool slot that houses the previous active tool and reset it
            foreach (var changeTool in registry.Items) {
                if (changeTool.tool == collidedTool) {
                    changeTool.ResetTool();
                }
            }
        }
    }

    private void ResetTool() {
        tool.GetComponent<Rigidbody>().isKinematic = true;
        tool.transform.SetPositionAndRotation(transform.position, transform.rotation);
    }

    private void OnDestroy() {
        registry.Unregister(this);
    }
}
