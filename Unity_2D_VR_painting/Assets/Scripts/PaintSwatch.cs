using Langweil.Painting;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Langweil
{
    /// <summary>
    /// Component for changing tool color.
    /// </summary>
    public class PaintSwatch : MonoBehaviour
    {
        [SerializeField]
        private Color changeToColor;

        private void OnTriggerEnter(Collider other) {
            if (other.attachedRigidbody != null && other.attachedRigidbody.TryGetComponent(out Brush pt)) {
                pt.ToolColor = changeToColor;
            }
        }
    }
}
