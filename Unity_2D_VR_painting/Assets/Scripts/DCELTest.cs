using DataStructures;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using Utilities;
using SysRandom = System.Random;

public class DCELTest : MonoBehaviour
{
    [SerializeField]
    private GameObject hitPrefab;
    [SerializeField]
    private MeshFilter lineMeshFilter;
    [SerializeField]
    private MeshFilter pointMeshFilter;

    [SerializeField]
    private MeshCollider mc;
    private Vector2? lastHit;

    private List<DCEL> DCELs = new List<DCEL>();
    private List<Mesh> colliders = new List<Mesh>();
    int currentDCEL = 0;

    private float a = 0.32f;
    private float b = 0.24f;

    public float Scale => a;
    public float Ratio => b / a;

    private float colliderPadding = 0.2f;
    private const float placeMaxDist = 0.05f;

    private bool test = false;
    private SysRandom randomCoords = new SysRandom();
    private List<LineSegment> cuts = new List<LineSegment>();

    private readonly string cutsSavePath = @"D:\dcel\cuts.dat";

    private int testedDCELs = 0;
    private int testLandmark = 1;
    private int nextLandmark = 10;

    //private int loadedDCELIdx;
    //private int loadedDCELCount;
    //private List<LineSegment> loadedCuts;
    //private string loadedMsg;

    private bool skip = false;

    void Start() {
        var initDCEL = DCEL.Create(new Polygon(
            new List<Vector2> { new Vector2(0.0f, 0.0f), new Vector2(1.0f, 0.0f), new Vector2(1.0f, Ratio), new Vector2(0.0f, Ratio) }
        ));

        transform.localScale = new Vector3(Scale, Scale, Scale);
        Init(initDCEL);

        mc.sharedMesh = colliders[0];
        DCELs[0].Visualize(out (Vector3[] vertices, int[] indeces, Color32[] otherData) lineMeshProperties, out (Vector3[] vertices, int[] indeces, Color32[] otherData) pointMeshProperties);
        SetMesh(lineMeshFilter.mesh, pointMeshFilter.mesh, lineMeshProperties, pointMeshProperties);

    }

    void Update() {
        if (Input.GetKeyDown(KeyCode.T)) {
            test = !test;
        }
        else if (Input.GetKeyDown(KeyCode.L)) {
            StartCoroutine(LoadCuts());
        }
        else if (Input.GetKeyDown(KeyCode.P)) {
            (var idx, var loadedDCELCount, var loadedCuts, var msg) = LoadCuts(cutsSavePath);
            foreach (var cut in loadedCuts) {
                print(cut);
            }
        }
        else if (Input.GetKeyDown(KeyCode.E)) {
            skip = !skip;
        }
        else if (Input.GetKeyDown(KeyCode.U)) {
            DCEL.history.Undo(out var _);
            foreach (var pair in DCEL.history.ResurrectionQueue) {
                foreach (var deadDCEL in pair.Value) {
                    int removeIdx = DCELs.IndexOf(deadDCEL);
                    DCELs.RemoveAt(removeIdx);
                    colliders.RemoveAt(removeIdx);
                    currentDCEL = 0;
                }
                Init(pair.Key);
            }
            DCEL.history.ResurrectionQueue.Clear();

            mc.sharedMesh = colliders[currentDCEL];
            DCELs[currentDCEL].Visualize(out (Vector3[] vertices, int[] indeces, Color32[] otherData) lineMeshProperties, out (Vector3[] vertices, int[] indeces, Color32[] otherData) pointMeshProperties);
            SetMesh(lineMeshFilter.mesh, pointMeshFilter.mesh, lineMeshProperties, pointMeshProperties);
        }
        else if (Input.GetKeyDown(KeyCode.Q)) {
            var edges = DCELs[currentDCEL].EdgesToString();
            print(edges);
        }

        if (Input.GetKeyDown(KeyCode.Mouse0)) {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (mc.Raycast(ray, out hit, Mathf.Infinity)) {
                //var movedHit = DCELs[currentDCEL].NearestVertex(hit.textureCoord, placeMaxDist);
                //var useHit = movedHit.IsPoisoned() ? hit.textureCoord : movedHit;
                var useHit = hit.textureCoord;

                var hitvis = Instantiate(hitPrefab, transform.TransformPoint(useHit), Quaternion.identity);
                Destroy(hitvis, 0.2f);
                //print(hit.textureCoord.ToString("F10"));
                if (lastHit is Vector2 lh) {
                    var cut = new LineSegment(lh, useHit);
                    Place(currentDCEL, ref cut);
                    DCEL.history.AdvanceTime();

                    mc.sharedMesh = colliders[currentDCEL];
                    DCELs[currentDCEL].Visualize(out (Vector3[] vertices, int[] indeces, Color32[] otherData) lineMeshProperties, out (Vector3[] vertices, int[] indeces, Color32[] otherData) pointMeshProperties);
                    SetMesh(lineMeshFilter.mesh, pointMeshFilter.mesh, lineMeshProperties, pointMeshProperties);

                    lastHit = null;
                }
                else {
                    lastHit = useHit;
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.Mouse1)) {
            currentDCEL = (currentDCEL + 1) % DCELs.Count;
            mc.sharedMesh = colliders[currentDCEL];
            DCELs[currentDCEL].Visualize(out (Vector3[] vertices, int[] indeces, Color32[] otherData) lineMeshProperties, out (Vector3[] vertices, int[] indeces, Color32[] otherData) pointMeshProperties);
            SetMesh(lineMeshFilter.mesh, pointMeshFilter.mesh, lineMeshProperties, pointMeshProperties);
        }

        if (test) {
            bool endTest = false;
            TestStart:
            if (endTest || DCELs.Count > 50) {
                ++testedDCELs;
                ResetDCEL();
                cuts.Clear();

                if (testedDCELs > nextLandmark) {
                    testLandmark = nextLandmark;
                    nextLandmark *= 10;
                }

                if (testedDCELs % testLandmark == 0) {
                    print($"{testedDCELs} DCELs have been tested successfully!");
                }
            }

            Vector2 start = new Vector2(RandomX(), RandomY());
            Vector2 end = new Vector2(RandomX(), RandomY());
            cuts.Add(new LineSegment(start, end));

            int dcelN = DCELs.Count;
            for (int i = 0; i < dcelN; i++) {
                try {
                    var cut = new LineSegment(start, end);
                    if (Place(i, ref cut) > 0) {
                        --i;
                        dcelN--;
                    }
                }
                catch (DCELInvalidException invalid) {
                    print(i + ": " + invalid.Message);
                    endTest = true;
                    goto TestStart;
                }
                catch (Exception e) {
                    print(i + ": " + e.Message);
                    SaveCuts(i, e.Message);
                    test = false;
                }
            }
            DCEL.history.AdvanceTime();
        }
    }

    private IEnumerator LoadCuts() {
        ResetDCEL();

        (var loadedDCELIdx, var loadedDCELCount, var loadedCuts, var msg) = LoadCuts(cutsSavePath);

        int loadedCount = loadedCuts.Count;
        int i = 0;

        // Go over cuts
        for (int c = 0; c < loadedCount; c++) {
            int dcelN = DCELs.Count;
            if (c == loadedCount - 1) {
                skip = false;
            }
            // Go over dcels
            for (i = 0; i < dcelN; i++) {
                var loadedCut = loadedCuts[c];

                // Visualize dcel before cutting it
                currentDCEL = i;
                if (!skip) {
                    VisualizeCurrentCut(loadedCut);
                }

                // Place the cut to current dcel
                int newDcelN = Place(i, ref loadedCut);
                if (newDcelN > 0) {
                    if (!skip) {
                        do {
                            yield return null;
                        } while (!Input.GetKeyDown(KeyCode.N));
                    }

                    // Visulize newly created dcels
                    for (int newDcelIdx = DCELs.Count - newDcelN; newDcelIdx < DCELs.Count; newDcelIdx++) {
                        currentDCEL = newDcelIdx;
                        if (!skip) {
                            VisualizeCurrentCut(loadedCut);

                            do {
                                yield return null;
                            } while (!Input.GetKeyDown(KeyCode.N));
                        }
                    }

                    --i;
                    dcelN--;
                }
                else if (newDcelN < 0) {
                    // Visualize cut dcel
                    currentDCEL = i;
                    VisualizeCurrentCut(loadedCut);

                    do {
                        yield return null;
                    } while (!Input.GetKeyDown(KeyCode.N));
                }
            }
            DCEL.history.AdvanceTime();
        }
        //i = 0;
        //while (DCELs.Count < loadedDCELCount) {
        //    if (Place(i, loadedCuts[loadedCount]) > 0) {
        //        --i;
        //    }
        //    ++i;
        //}

        //currentDCEL = loadedDCELIdx;
        //VisualizeCurrentCut(loadedCuts[loadedCount]);
    }

    private void VisualizeCurrentCut(LineSegment cut) {
        mc.sharedMesh = colliders[currentDCEL];
        DCELs[currentDCEL].Visualize(out (Vector3[] vertices, int[] indeces, Color32[] otherData) lineMeshProperties);
        SetMeshCut(lineMeshFilter.mesh, pointMeshFilter.mesh, lineMeshProperties,
            (new Vector3[] { cut.A, cut.B }, new int[] { 0, 1 }, new Color32[] { Color.red, Color.red }));
    }

    private int Place(int i, ref LineSegment cut) {
        var movedA = DCELs[i].NearestVertexCoords(cut.A, placeMaxDist);
        var movedB = DCELs[i].NearestVertexCoords(cut.B, placeMaxDist);

        try {
            cut = new LineSegment(movedA.IsPoisoned() ? cut.A : movedA, movedB.IsPoisoned() ? cut.B : movedB);
        }
        catch (System.ArgumentException) {
            return 0;
        }

        if (Vector2.Distance(cut.A, cut.B) < 0.01f) {
            return 0;
        }

        int ret;
        try {
            DCELs[i].PlaceEdge(cut, out var x);
            ret = -x.Count;
        }
        catch (DCELParallelException) {
            ret = 0;
        }

        if (DCELs[i].FaceCount > 1) {
            int faces = DCELs[i].FaceCount;
            foreach (var newDCEL in DCELs[i].CreateFromFaces()) {
                Init(newDCEL);
            }
            DCELs.RemoveAt(i);
            colliders.RemoveAt(i);
            currentDCEL = 0;
            return faces;
        }
        return ret;
    }

    private void SaveCuts(int dcelIdx, string msg) {
        using (var stream = File.Open(cutsSavePath, FileMode.Create)) {
            using (var writer = new BinaryWriter(stream, System.Text.Encoding.UTF8, false)) {
                writer.Write(msg);
                writer.Write(cuts.Count);
                foreach (var cut in cuts) {
                    writer.Write(cut.A.x);
                    writer.Write(cut.A.y);
                    writer.Write(cut.B.x);
                    writer.Write(cut.B.y);
                }
                writer.Write(dcelIdx);
                writer.Write(DCELs.Count);
            }
        }
    }

    private static (int dcelIdx, int dcelN, List<LineSegment> cuts, string msg) LoadCuts(string path) {
        int dcelIdx = -1;
        int dcelN = -1;
        string msg = "";
        var cuts = new List<LineSegment>();

        if (File.Exists(path)) {
            using (var stream = File.Open(path, FileMode.Open)) {
                using (var reader = new BinaryReader(stream, System.Text.Encoding.UTF8, false)) {
                    msg = reader.ReadString();
                    int cutN = reader.ReadInt32();
                    for (int i = 0; i < cutN; i++) {
                        float startX = reader.ReadSingle();
                        float startY = reader.ReadSingle();
                        float endX = reader.ReadSingle();
                        float endY = reader.ReadSingle();
                        cuts.Add(new LineSegment(new Vector2(startX, startY), new Vector2(endX, endY)));
                    }
                    dcelIdx = reader.ReadInt32();
                    dcelN = reader.ReadInt32();
                }
            }
        }
        return (dcelIdx, dcelN, cuts, msg);
    }

    private void ResetDCEL() {
        DCELs.Clear();
        colliders.Clear();
        Init(DCEL.Create(new Polygon(
            new List<Vector2> { new Vector2(0.0f, 0.0f), new Vector2(1.0f, 0.0f), new Vector2(1.0f, Ratio), new Vector2(0.0f, Ratio) }
        )));
    }

    private float RandomX() {
        return ChangeInterval((float)randomCoords.NextDouble(), 1.0f, 0.2f);
    }

    private float RandomY() {
        return ChangeInterval((float)randomCoords.NextDouble(), Ratio, 0.2f);
    }

    private float ChangeInterval(float curr, float max, float padding) {
        return curr * (max + 2 * padding) - padding;
    }

    private void SetMesh(Mesh lineMesh, Mesh pointMesh, (Vector3[] vertices, int[] indeces, Color32[] otherData) lineMeshProperties, (Vector3[] vertices, int[] indeces, Color32[] otherData) pointMeshProperties) {
        lineMesh.Clear(false);
        lineMesh.vertices = lineMeshProperties.vertices;
        lineMesh.SetIndices(lineMeshProperties.indeces, MeshTopology.Lines, 0);
        lineMesh.colors32 = lineMeshProperties.otherData;

        pointMesh.Clear(false);
        pointMesh.vertices = pointMeshProperties.vertices;
        pointMesh.SetIndices(pointMeshProperties.indeces, MeshTopology.Points, 0);
        pointMesh.colors32 = pointMeshProperties.otherData;
    }

    private void SetMeshCut(Mesh lineMesh, Mesh cutMesh, (Vector3[] vertices, int[] indeces, Color32[] otherData) lineMeshProperties, (Vector3[] vertices, int[] indeces, Color32[] otherData) cutMeshProperties) {
        lineMesh.Clear(false);
        lineMesh.vertices = lineMeshProperties.vertices;
        lineMesh.SetIndices(lineMeshProperties.indeces, MeshTopology.Lines, 0);
        lineMesh.colors32 = lineMeshProperties.otherData;

        cutMesh.Clear(false);
        cutMesh.vertices = cutMeshProperties.vertices;
        cutMesh.SetIndices(cutMeshProperties.indeces, MeshTopology.Lines, 0);
        cutMesh.colors32 = cutMeshProperties.otherData;
    }

    public void Init(DCEL initDCEL) {
        // Init DCEL
        DCELs.Add(initDCEL);

        // Triangulate DCEL for rendering
        initDCEL.Triangulate(VertexPositionToData, VertexCombine, out (Vector3[] vertices, int[] triangleIndeces, object[] otherData) meshProperties);

        // Cast and unwrap vertex data
        (Vector2[] uvs, Vector3[] normals) meshData = Array.ConvertAll(meshProperties.otherData, x => (Tuple<Vector2, Vector3>)x).Unwrap();

        // Render mesh ------------------------------------------------------------

        // Duplicate the mesh so it is double-sided
        // vertices
        var vertices = new Vector3[meshProperties.vertices.Length * 2];
        for (int i = 0; i < meshProperties.vertices.Length; i++) {
            vertices[i] = vertices[meshProperties.vertices.Length + i] = meshProperties.vertices[i];
        }
        // uvs
        var uvs = new Vector2[meshData.uvs.Length * 2];
        for (int i = 0; i < meshData.uvs.Length; i++) {
            uvs[i] = uvs[meshData.uvs.Length + i] = meshData.uvs[i];
        }
        // normals
        var normals = new Vector3[meshData.normals.Length * 2];
        for (int i = 0; i < meshData.normals.Length; i++) {
            normals[i] = meshData.normals[i];
            normals[meshData.normals.Length + i] = -1.0f * meshData.normals[i];
        }

        // Create mesh for rendering
        Mesh renderMesh = new Mesh();
        //renderMesh.subMeshCount = 2;
        renderMesh.subMeshCount = 1;
        renderMesh.vertices = vertices;
        renderMesh.uv = uvs;
        renderMesh.normals = normals;

        // Front side submesh indeces
        renderMesh.SetTriangles(meshProperties.triangleIndeces, 0);
        // Back side submesh indeces - translate by number of vertices to point to duplicated vertices and reverse order to render correctly (not back-face cull)
        //renderMesh.SetTriangles(meshProperties.triangleIndeces.Reverse().Select(x => x + meshProperties.vertices.Length).ToArray(), 1);

        // Collider mesh ------------------------------------------------------------

        // Get min and max x and z from mesh coords
        float minX = float.PositiveInfinity;
        float minY = float.PositiveInfinity;
        float maxX = float.NegativeInfinity;
        float maxY = float.NegativeInfinity;
        foreach (var vertex in meshProperties.vertices) {
            if (vertex.x < minX) minX = vertex.x;
            if (vertex.y < minY) minY = vertex.y;
            if (vertex.x > maxX) maxX = vertex.x;
            if (vertex.y > maxY) maxY = vertex.y;
        }

        Vector2[] colliderVertices = {
            new Vector2(minX - colliderPadding, minY - colliderPadding),
            new Vector2(minX - colliderPadding, maxY + colliderPadding),
            new Vector2(maxX + colliderPadding, maxY + colliderPadding),
            new Vector2(maxX + colliderPadding, minY - colliderPadding)
        };

        vertices = new Vector3[8];
        for (int i = 0; i < 4; i++) {
            vertices[i] = vertices[4 + i] = colliderVertices[i];
        }
        // uvs
        uvs = new Vector2[8];
        for (int i = 0; i < 4; i++) {
            uvs[i] = uvs[4 + i] = colliderVertices[i];
        }
        // normals
        normals = new Vector3[8];
        for (int i = 0; i < 4; i++) {
            normals[i] = Vector3.up;
            normals[4 + i] = -1.0f * Vector3.up;
        }

        Mesh colliderMesh = new Mesh();
        colliderMesh.vertices = vertices;
        colliderMesh.uv = uvs;
        colliderMesh.normals = normals;
        colliderMesh.triangles = new int[] { 0, 1, 2, 0, 2, 3, 4, 7, 6, 4, 6, 5 };
        colliders.Add(colliderMesh);
    }

    private object VertexPositionToData(Vector2 pos) {
        return new Tuple<Vector2, Vector3>(pos, Vector3.up);
    }

    private object VertexCombine(LibTessDotNet.Vec3 position, object[] data, float[] weights) {
        return VertexPositionToData(new Vector2(position.X, position.Y));
    }

}
