using Langweil;
using Langweil.Cutting;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;
using Valve.VR;
using Valve.VR.InteractionSystem;

namespace Langweil
{
    /// <summary>
    /// Component for tool that moves the paper.
    /// </summary>
    [RequireComponent(typeof(Interactable))]
    public class MoveTool : MonoBehaviour, IInteractableTool
    {
        [WarnOnNull]
        [SerializeField]
        private SteamVR_Action_Boolean grabAction;

        [WarnOnNull]
        [SerializeField]
        [Tooltip("Tip of the tool that grabs the paper")]
        private Transform tip;
        public Transform Tip => tip;

        [SerializeField]
        [Tooltip("Distance between paper and tip when the tool grabs")]
        private float pickUpDistance;
        public float InteractionDistance => pickUpDistance;

        public bool InteractionActive {
            get;
            set;
        } = false;

        [WarnOnNull]
        [SerializeField]
        [Tooltip("Material for displaying the paper transparently")]
        private Material transparentMaterial;
        private Material[] transparentMaterials;

        [WarnOnNull]
        [SerializeField]
        [Tooltip("Prefab that displays when about to destroy paper")]
        private GameObject redCrossPrefab;

        private Material[] collidingMaterials;

        private Cuttable collidingPaper = null;
        private Cuttable CollidingPaper {
            set {
                // Setting a new value
                if (value != null) {
                    // Get drawable
                    var drawable = value.GetComponent<Painting.Drawable>();
                    var projectionInfo = value.ProjectOnPaper(tip.position);

                    if (drawable != null && projectionInfo.isOnPaper) {
                        // Cache the drawable original material
                        collidingMaterials = drawable.MeshRenderer.sharedMaterials;

                        // Switch current paper material to transparent one
                        for (int i = 0; i < collidingMaterials.Length; i++) {
                            transparentMaterials[i].mainTexture = collidingMaterials[i].mainTexture;
                            var col = collidingMaterials[i].color;
                            col.a = 0.5f;
                            transparentMaterials[i].color = col;
                        }

                        drawable.MeshRenderer.sharedMaterials = transparentMaterials;
                    }
                    else {
                        value = null;
                    }
                }
                // Setting value to null and we have a cached mat
                else if (collidingMaterials != null && collidingPaper != null) {
                    // Reset the original material
                    var drawable = collidingPaper.GetComponent<Painting.Drawable>();
                    if (drawable != null) {
                        drawable.MeshRenderer.sharedMaterials = collidingMaterials;
                    }
                    collidingMaterials = null;
                }

                collidingPaper = value;
            }
        }

        private Material[] pickedUpMaterials;

        private Cuttable pickedUpPaper = null;
        private Cuttable PickedUpPaper {
            set {
                // Trying to pick up paper
                if (value != null) {
                    // Get drawable
                    var drawable = value.GetComponent<Painting.Drawable>();
                    var projectionInfo = value.ProjectOnPaper(tip.position);

                    if (drawable != null && projectionInfo.isOnPaper) {
                        // Cache the drawable original material
                        pickedUpMaterials = drawable.MeshRenderer.sharedMaterials;

                        // Set ghost mesh
                        ghostMeshFilter.sharedMesh = value.RenderMeshFilter.sharedMesh;
                        ghostMeshFilter.transform.localScale = value.transform.localScale;

                        // Set ghost material
                        for (int i = 0; i < pickedUpMaterials.Length; i++) {
                            transparentMaterials[i].mainTexture = pickedUpMaterials[i].mainTexture;
                            var col = pickedUpMaterials[i].color;
                            col.a = 0.5f;
                            transparentMaterials[i].color = col;

                        }

                        // Display ghost
                        ghostMeshFilter.gameObject.SetActive(true);

                        // Set picked up paper material
                        drawable.MeshRenderer.sharedMaterials = transparentMaterials;

                        // Calculate paper offset
                        paperOffset = value.transform.TransformVector(projectionInfo.localSpace);
                    }
                    // Don't pickup paper
                    else {
                        value = null;
                    }
                }

                // Dropping paper
                if (value == null) {
                    if (pickedUpPaper != null && pickedUpMaterials != null) {
                        // Reset the original material
                        var drawable = pickedUpPaper.GetComponent<Painting.Drawable>();
                        if (drawable != null) {
                            drawable.MeshRenderer.sharedMaterials = pickedUpMaterials;
                        }

                        pickedUpMaterials = null;
                    }

                    ghostMeshFilter.gameObject.SetActive(false);
                }

                pickedUpPaper = value;
            }
        }

        private Vector3 paperOffset;

        private MeshFilter ghostMeshFilter;

        private void Start() {
            WarnOnNullAttribute.Check(typeof(MoveTool), this, name);

            transparentMaterials = new Material[] { Instantiate(transparentMaterial), Instantiate(transparentMaterial) };

            // Create ghost paper GO
            ghostMeshFilter = new GameObject("Paper ghost").AddComponent<MeshFilter>();
            ghostMeshFilter.gameObject.layer = LayerMask.NameToLayer("ToolInteraction");
            var meshRenderer = ghostMeshFilter.gameObject.AddComponent<MeshRenderer>();
            meshRenderer.sharedMaterials = transparentMaterials;
            ghostMeshFilter.gameObject.SetActive(false);

            redCrossPrefab = Instantiate(redCrossPrefab);
            redCrossPrefab.SetActive(false);
        }

        private void Update() {
            if (pickedUpPaper != null) {
                // Move paper
                pickedUpPaper.transform.position = tip.transform.position - paperOffset;

                // Move ghost
                var pos = GetPaperPlacement();
                if (pos.HasValue) {
                    ghostMeshFilter.transform.SetPositionAndRotation(pos.Value.pos, pos.Value.rot);
                    redCrossPrefab.SetActive(false);
                }
                else {
                    // Show that it will be destroyed
                    redCrossPrefab.transform.position = tip.transform.position + 0.1f * Vector3.up;
                    redCrossPrefab.SetActive(true);
                }
            }
        }

        private void FixedUpdate() {
            if (!InteractionActive) return;

            // Pick up on grip
            if (grabAction.GetState(Calibration.CurrentHand) && pickedUpPaper == null && collidingPaper != null) {
                var temp = collidingPaper;
                CollidingPaper = null;
                PickedUpPaper = temp;
            }
            // Drop on grip loss
            else if (!grabAction.GetState(Calibration.CurrentHand) && pickedUpPaper != null) {
                var pos = GetPaperPlacement();
                if (pos.HasValue) {
                    pickedUpPaper.transform.parent = pos.Value.parent;
                    pickedUpPaper.transform.SetPositionAndRotation(pos.Value.pos, pos.Value.rot);
                }
                else {
                    // Destroy
                    Utilities.Util.DestroyGameObject(pickedUpPaper);
                }

                redCrossPrefab.SetActive(false);
                PickedUpPaper = null;
            }
        }

        private void OnTriggerStay(Collider other) {
            if (!InteractionActive) return;

            // Collide only not already colliding and if not holding paper
            if (collidingPaper == null && pickedUpPaper == null) {
                Transform otherTransform = other.transform;
                Cuttable collidedPaper;

                // Find cuttable
                while (!otherTransform.TryGetComponent(out collidedPaper) && otherTransform.parent != null) {
                    otherTransform = otherTransform.parent;
                }

                if (collidedPaper != null) {
                    CollidingPaper = collidedPaper;
                }
            }
        }

        private void OnTriggerExit(Collider other) {
            if (!InteractionActive) return;

            // Exit collision only if already colliding and the current colliding is same
            if (collidingPaper != null && pickedUpPaper == null) {
                Transform otherTransform = other.transform;
                Cuttable collidedPaper;

                while (!otherTransform.TryGetComponent(out collidedPaper) && otherTransform.parent != null) {
                    otherTransform = otherTransform.parent;
                }

                if (collidedPaper == collidingPaper) {
                    CollidingPaper = null;
                }
            }
        }

        private (Vector3 pos, Quaternion rot, Transform parent)? GetPaperPlacement() {
            Vector3 pos = Calibration.PaperStartPos;
            Quaternion rot = Calibration.PaperStartRot;

            // Raycast down and get the position where to place the paper
            if (Physics.Raycast(new Ray(tip.position, Vector3.down), out RaycastHit hitInfo, float.PositiveInfinity, 1)) {
                // Place into paper slot
                if (hitInfo.transform.TryGetComponent(out PaperSlot ps)) {
                    var ret = ps.GetPosition(tip.position, Quaternion.Inverse(pickedUpPaper.transform.rotation) * paperOffset);
                    if (ret.HasValue) return ret;
                }

                switch (hitInfo.transform.tag) {
                    // Place on table
                    case "Table":
                        pos = hitInfo.point - paperOffset;
                        break;
                    // Destroy
                    case "Trash":
                        return null;
                    // Place on reset position
                    default:
                        break;
                }
            }

            // Move paper to original Y and offset it
            pos.y = Calibration.PaperStartPos.y;

            return (pos, rot, null);
        }

        private void OnDestroy() {
            foreach (var mat in transparentMaterials) {
                Destroy(mat);
            }
            Destroy(redCrossPrefab);
        }
    }
}
