using Langweil.Cutting;
using Langweil.Painting;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;

namespace Langweil
{
    public class Paper : MonoBehaviour
    {
        private static readonly Registry<Paper> paperRegistry = new HashSetRegistry<Paper>();
        public static Registry<Paper> PaperRegistry => paperRegistry;

        [WarnOnNull]
        private Drawable drawable;
        public Drawable Drawable => drawable;

        [WarnOnNull]
        private Cuttable cuttable;
        public Cuttable Cuttable => cuttable;

        void Awake() {
            drawable = GetComponent<Drawable>();
            cuttable = GetComponent<Cuttable>();
            this.CheckNull();

            paperRegistry.Register(this);
        }

        private void Update() {
            if (Input.GetKeyUp(KeyCode.S)) {
                // Save
                string path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments, System.Environment.SpecialFolderOption.Create);
                drawable.SaveToDiskPNG(path, Util.GetUniqueFileName(path, "paper_" + System.DateTime.Now.ToString("yyyy-MM-dd-hhmm"), "png"));
            }
        }

        private void OnDestroy() {
            paperRegistry.Unregister(this);
        }

    }
}
