using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;

namespace Langweil
{
    [Serializable]
    public struct ModelInfo
    {
        [WarnOnNull]
        public GameObject mesh;
        [WarnOnNull]
        public Texture2D paperTexture;
        [WarnOnNull]
        public Texture2D bookTexture;
    }
}
