using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;

namespace Langweil.Cutting
{
    /// <summary>
    /// Class for storing information about the cut and visualizing it.
    /// </summary>
    public class Cut
    {
        private Vector2? first = null;
        /// <summary>
        /// First point of the cut.
        /// </summary>
        public Vector2? First {
            get => first;
            set {
                first = value;
                SetLRPositions();
            }
        }

        private Vector2? last = null;
        /// <summary>
        /// Last (or second) point of the cut.
        /// </summary>
        public Vector2? Last {
            get => last;
            set {
                last = value;
                SetLRPositions();
            }
        }

        private readonly Transform cuttableTransform;
        private readonly Material lrMat;
        private readonly Vector3 visNormal;

        private LineRenderer cutVisual = new GameObject("Cut visual").AddComponent<LineRenderer>();
        private GameObject segmentedCutsParent = null;

        /// <summary>
        /// Creates a cut owned by <paramref name="cuttableTransform"/> visualized with a LineRenderer using <paramref name="lrMat"/> offset by <paramref name="visNormal"/>.
        /// </summary>
        /// <param name="cuttableTransform">Transform of the cuttable this cut belongs to.</param>
        /// <param name="lrMat">Material for the LRs.</param>
        /// <param name="visNormal">Normal of the cuttable to offset the LRs.</param>
        public Cut(Transform cuttableTransform, Material lrMat, Vector3 visNormal) {
            this.cuttableTransform = cuttableTransform;
            this.lrMat = lrMat;
            this.visNormal = visNormal;

            cutVisual.alignment = LineAlignment.TransformZ;
            cutVisual.loop = false;
            cutVisual.material = lrMat;
            cutVisual.useWorldSpace = true;
            cutVisual.startColor = cutVisual.endColor = Color.blue;
            cutVisual.positionCount = 2;
            cutVisual.numCapVertices = 20;
            cutVisual.startWidth = cutVisual.endWidth = CutVisual.thickness;
        }

        /// <summary>
        /// Display the <paramref name="segments"/> the cut was segmented into.
        /// </summary>
        /// <param name="segments">Segments the cut was segmented into.</param>
        public void SegmentCut(List<DataStructures.LineSegment> segments) {
            // Destroy the unsegmented cut visual
            Util.DestroyGameObject(cutVisual);
            Object.Destroy(segmentedCutsParent);
            segmentedCutsParent = new GameObject("Visual parent");

            // Create segment visuals
            foreach (var segment in segments) {
                var visual = new GameObject("Cut segment").AddComponent<CutVisual>();
                visual.transform.parent = segmentedCutsParent.transform;
                visual.InitCut(cuttableTransform, segment.A, segment.B, lrMat, visNormal);
            }
        }

        /// <summary>
        /// Deletes the cut, that is destroys all visuals (does NOT alter DCEL or mesh).
        /// </summary>
        public void Delete() {
            Util.DestroyGameObject(cutVisual);
            Object.Destroy(segmentedCutsParent);
        }

        private void SetLRPositions() {
            if (first.HasValue || last.HasValue) {
                // If one of the first and last values is not set, use the other
                var f = (Vector3)(first.HasValue ? first.Value : last.Value) + CutVisual.visualOffset * visNormal;
                var l = (Vector3)(last.HasValue ? last.Value : first.Value) + CutVisual.visualOffset * visNormal;

                // The LR looks in -Z, so get rotation to transformed normal of the cuttable
                cutVisual.transform.rotation = Quaternion.FromToRotation(Vector3.back, cuttableTransform.TransformDirection(visNormal));
                // Transform positions from cuttable local space to world space
                cutVisual.SetPosition(0, cuttableTransform.TransformPoint(f));
                cutVisual.SetPosition(1, cuttableTransform.TransformPoint(l));
            }
        }
    }
}
