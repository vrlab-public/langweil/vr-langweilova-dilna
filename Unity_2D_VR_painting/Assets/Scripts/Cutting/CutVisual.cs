using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;

namespace Langweil.Cutting
{
    /// <summary>
    /// Component for visualizing a cut segment.
    /// </summary>
    public class CutVisual : MonoBehaviour
    {
        /// <summary>
        /// If <see langword="true"/>, all cuts are rendered, otherwise they are disabled.
        /// </summary>
        public static bool display;

        private Transform cuttableTransform;
        private Vector3 a;
        private Vector3 b;
        private Material lrMat;
        private Vector3 visNormal;

        private LineRenderer aLR;
        private LineRenderer bLR;
        private LineRenderer segmentLR;

        public const float thickness = 0.0025f;
        public const float visualOffset = 0.0001f;

        void Start() {
            // Create LRs
            aLR = CreateLR();
            bLR = CreateLR();
            aLR.numCapVertices = bLR.numCapVertices = 20;
            aLR.startWidth = aLR.endWidth = bLR.startWidth = bLR.endWidth = 2 * thickness;
            segmentLR = CreateLR();
            segmentLR.startWidth = segmentLR.endWidth = thickness;
        }

        void Update() {
            if (display) {
                aLR.gameObject.SetActive(true);
                bLR.gameObject.SetActive(true);
                segmentLR.gameObject.SetActive(true);

                // Offset and transform cut points
                Vector3 aWorld = cuttableTransform.TransformPoint(a + visualOffset * visNormal);
                Vector3 bWorld = cuttableTransform.TransformPoint(b + visualOffset * visNormal);

                // The LR looks in -Z, so get rotation to transformed normal of the cuttable
                var rot = Quaternion.FromToRotation(Vector3.back, cuttableTransform.TransformDirection(visNormal));

                // Set positions of points A and B
                aLR.SetPosition(0, aWorld);
                aLR.SetPosition(1, aWorld);
                aLR.transform.rotation = rot;

                bLR.SetPosition(0, bWorld);
                bLR.SetPosition(1, bWorld);
                bLR.transform.rotation = rot;

                // Set positions of the segment
                segmentLR.SetPosition(0, aWorld);
                segmentLR.SetPosition(1, bWorld);
                segmentLR.transform.rotation = rot;
            }
            else {
                aLR.gameObject.SetActive(false);
                bLR.gameObject.SetActive(false);
                segmentLR.gameObject.SetActive(false);
            }
        }

        /// <summary>
        /// Init the visual.
        /// </summary>
        /// <param name="cuttableTransform">Transform of the cuttable this cut belongs to.</param>
        /// <param name="a">Point A of the segment.</param>
        /// <param name="b">Point B of the segment.</param>
        /// <param name="lrMat">Material for the LRs.</param>
        /// <param name="visNormal">Normal of the cuttable to offset the LRs.</param>
        public void InitCut(Transform cuttableTransform, Vector3 a, Vector3 b, Material lrMat, Vector3 visNormal) {
            this.cuttableTransform = cuttableTransform;
            this.a = a;
            this.b = b;
            this.lrMat = lrMat;
            this.visNormal = visNormal;
        }

        private LineRenderer CreateLR() {
            var lr = new GameObject("Segment LR").AddComponent<LineRenderer>();
            lr.alignment = LineAlignment.TransformZ;
            lr.loop = false;
            lr.material = lrMat;
            lr.useWorldSpace = true;
            lr.startColor = lr.endColor = Color.red;
            lr.positionCount = 2;

            return lr;
        }


        private void OnDestroy() {
            Util.DestroyGameObject(aLR);
            Util.DestroyGameObject(bLR);
            Util.DestroyGameObject(segmentLR);
        }
    }
}
