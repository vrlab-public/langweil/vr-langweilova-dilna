using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;

namespace Langweil.Cutting
{
    /// <summary>
    /// Component for storing information about the cutting tool.
    /// </summary>
    public class Knife : MonoBehaviour, IInteractableTool
    {
        private static readonly Registry<Knife> knifeRegistry = new HashSetRegistry<Knife>();
        /// <summary>
        /// Active knives.
        /// </summary>
        public static IEnumerable<Knife> Knives => knifeRegistry.Items;

        [SerializeField]
        [Tooltip("Tool's tip that should interact")]
        private Transform tip;
        public Transform Tip => tip;

        [SerializeField]
        [Tooltip("Distance between paper and tip when the knife will cut")]
        private float cuttingDistance = 0.01f;
        public float InteractionDistance => cuttingDistance;

        public bool InteractionActive {
            get;
            set;
        } = false;

        private void Start() {
            knifeRegistry.Register(this);
        }

        private void OnEnable() {
            knifeRegistry.Register(this);
        }

        private void OnDisable() {
            knifeRegistry.Unregister(this);
        }

        private void OnDestroy() {
            knifeRegistry.Unregister(this);
        }
    }
}
