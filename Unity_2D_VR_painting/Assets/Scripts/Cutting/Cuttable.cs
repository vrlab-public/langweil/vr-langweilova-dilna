using DataStructures;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Utilities;

namespace Langweil.Cutting
{
    /// <summary>
    /// Component for making a 2D mesh cuttable.
    /// </summary>
    public class Cuttable : MonoBehaviour
    {
        [SerializeField]
        [Tooltip("Height in meters")]
        private float height = 1.0f;
        public float Height => height;

        [SerializeField]
        [Tooltip("Width in meters")]
        private float width = 1.0f;
        public float Width => width;

        /// <summary>
        /// Primary and secondary axes of the mesh.
        /// </summary>
        /// <remarks>
        /// Max coordinate (in local coord system) along primary axis is 1, along secondary axis is between 0 and 1.
        /// </remarks>
        public (float primaryAxis, float secondaryAxis) Axes => height > width ? (height, width) : (width, height);
        /// <summary>
        /// Scale of the mesh.
        /// </summary>
        public float Scale => Axes.primaryAxis;
        /// <summary>
        /// Ration between width and height.
        /// </summary>
        public float Ratio {
            get {
                var axes = Axes;
                return axes.secondaryAxis / axes.primaryAxis;
            }
        }

        [SerializeField]
        [WarnOnNull]
        [Tooltip("Mesh filter that holds the mesh for rendering")]
        private MeshFilter renderMeshFilter;
        public MeshFilter RenderMeshFilter => renderMeshFilter;

        [SerializeField]
        [WarnOnNull]
        [Tooltip("Mesh collider that holds the mesh for raycast collisions")]
        private MeshCollider cuttingCollider;

        private Vector3 cuttingColliderCenter;
        /// <summary>
        /// Center of the cutting collider (average of min and max X and Y coordinates).
        /// </summary>
        public Vector3 CuttingColliderCenter => cuttingColliderCenter;

        [SerializeField]
        [Tooltip("Cuts with length less than this epsilon will not be placed")]
        private float _cutEpsilon = 0.01f;
        private float CutEpsilon => (1.0f / Scale) * _cutEpsilon;
        [SerializeField]
        [Tooltip("Cut points with a vertex within epsilon radius will snap to that vertex")]
        private float _snapEpsilon = 0.01f;
        private float SnapEpsilon => (1.0f / Scale) * _snapEpsilon;
        [SerializeField]
        [Tooltip("Padding around the mesh for the cutting collider")]
        private float colliderPadding = 0.2f;
        [SerializeField]
        [Tooltip("Normal of the paper in local space, raycasts will be cast in reverse of this direction. Gets normalized in Start")]
        private Vector3 interactionNormal = new Vector3(0.0f, 0.0f, 1.0f);

        [SerializeField]
        [WarnOnNull]
        [Tooltip("Material used by the LineRenderers to display cuts")]
        private Material cutVisualMaterial;

        private DCEL meshDCEL = null;
        private Cut currentCut = null;
        private Cut cutVisual = null;
        private Cut CutVisual {
            set {
                if (cutVisual != null) {
                    cutVisual.Delete();
                }

                if (value != null) {
                    value.SegmentCut(meshDCEL.Bridges);
                }

                cutVisual = value;
            }
        }

        private void Awake() {
            transform.localScale = new Vector3(Scale, Scale, Scale);
        }

        void Start() {
            WarnOnNullAttribute.Check(typeof(Cuttable), this, name);
            var s = interactionNormal.NormalizeExceptZero(Vector3.forward);
            if (s != null) {
                Debug.Log($"{name}, {nameof(interactionNormal)}: {s}");
            }
        }

        private void FixedUpdate() {
            foreach (var knife in Knife.Knives) {
                if (knife.InteractionActive) {
                    var normal = transform.TransformDirection(interactionNormal);

                    // First and last hit raycast define Cut points
                    if (cuttingCollider.Raycast(new Ray(knife.Tip.position, -normal), out RaycastHit hitInfo, knife.InteractionDistance)) {
                        if (currentCut == null) {
                            currentCut = new Cut(transform, cutVisualMaterial, interactionNormal);
                            var movedFirst = meshDCEL.NearestVertexCoords(hitInfo.textureCoord, _snapEpsilon);
                            currentCut.First = movedFirst.IsPoisoned() ? hitInfo.textureCoord : movedFirst;
                        }
                        else {
                            var movedLast = meshDCEL.NearestVertexCoords(hitInfo.textureCoord, _snapEpsilon);
                            currentCut.Last = movedLast.IsPoisoned() ? hitInfo.textureCoord : movedLast;
                        }
                    }
                    // Place the cut on the paper if there is an active cut and reset it
                    else {
                        if (currentCut != null) {
                            Place(currentCut);
                            currentCut.Delete();
                            currentCut = null;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Initializes the component using <paramref name="initDCEL"/>.
        /// </summary>
        /// <remarks>
        /// Triangulates <paramref name="initDCEL"/> and creates meshes for rendering and colliding.
        /// </remarks>
        /// <param name="initDCEL">DCEL with which to initialize this cuttable.</param>
        public void Init(DCEL initDCEL) {
            // Init DCEL
            meshDCEL = initDCEL;
            meshDCEL.Scale = Scale;

            // Add cut visulizations
            CutVisual = new Cut(transform, cutVisualMaterial, interactionNormal);
            
            // Triangulate DCEL for rendering
            meshDCEL.Triangulate(VertexPositionToData, VertexCombine, out (Vector3[] vertices, int[] triangleIndeces, object[] otherData) meshProperties);

            // Cast and unwrap vertex data
            (Vector2[] uvs, Vector3[] normals) meshData = Array.ConvertAll(meshProperties.otherData, x => (Tuple<Vector2, Vector3>)x).Unwrap();

            // Render mesh ------------------------------------------------------------

            // Duplicate the mesh so it is double-sided
            // vertices
            var vertices = new Vector3[meshProperties.vertices.Length * 2];
            for (int i = 0; i < meshProperties.vertices.Length; i++) {
                vertices[i] = vertices[meshProperties.vertices.Length + i] = meshProperties.vertices[i];
            }
            // uvs
            var uvs = new Vector2[meshData.uvs.Length * 2];
            for (int i = 0; i < meshData.uvs.Length; i++) {
                uvs[i] = uvs[meshData.uvs.Length + i] = meshData.uvs[i];
            }
            // normals
            var normals = new Vector3[meshData.normals.Length * 2];
            for (int i = 0; i < meshData.normals.Length; i++) {
                normals[i] = meshData.normals[i];
                normals[meshData.normals.Length + i] = -1.0f * meshData.normals[i];
            }

            // Create mesh for rendering
            Mesh renderMesh = new Mesh();
            renderMesh.subMeshCount = 2;
            //renderMesh.subMeshCount = 1;
            renderMesh.vertices = vertices;
            renderMesh.uv = uvs;
            renderMesh.normals = normals;
            renderMeshFilter.sharedMesh = renderMesh;

            // Front side submesh indeces
            renderMesh.SetTriangles(meshProperties.triangleIndeces, 0);
            // Back side submesh indeces - translate by number of vertices to point to duplicated vertices and reverse order to render correctly (not back-face cull)
            renderMesh.SetTriangles(meshProperties.triangleIndeces.Reverse().Select(x => x + meshProperties.vertices.Length).ToArray(), 1);

            renderMeshFilter.mesh = renderMesh;

            // Collider mesh ------------------------------------------------------------

            // Get min and max x and z from mesh coords
            float minX = float.PositiveInfinity;
            float minY = float.PositiveInfinity;
            float maxX = float.NegativeInfinity;
            float maxY = float.NegativeInfinity;
            foreach (var vertex in meshProperties.vertices) {
                if (vertex.x < minX) minX = vertex.x;
                if (vertex.y < minY) minY = vertex.y;
                if (vertex.x > maxX) maxX = vertex.x;
                if (vertex.y > maxY) maxY = vertex.y;
            }

            cuttingColliderCenter = new Vector3((minX + maxX) / 2.0f, (minY + maxY) / 2.0f, 0.0f);

            Vector2[] colliderVertices = {
                new Vector2(minX - colliderPadding, minY - colliderPadding),
                new Vector2(minX - colliderPadding, maxY + colliderPadding),
                new Vector2(maxX + colliderPadding, maxY + colliderPadding),
                new Vector2(maxX + colliderPadding, minY - colliderPadding)
            };

            vertices = new Vector3[8];
            for (int i = 0; i < 4; i++) {
                vertices[i] = vertices[4 + i] = colliderVertices[i];
            }
            // uvs
            uvs = new Vector2[8];
            for (int i = 0; i < 4; i++) {
                uvs[i] = uvs[4 + i] = colliderVertices[i];
            }
            // normals
            normals = new Vector3[8];
            for (int i = 0; i < 4; i++) {
                normals[i] = Vector3.up;
                normals[4 + i] = -1.0f * Vector3.up;
            }

            Mesh colliderMesh = new Mesh();
            colliderMesh.vertices = vertices;
            colliderMesh.uv = uvs;
            colliderMesh.normals = normals;
            colliderMesh.triangles = new int[] { 0, 1, 2, 0, 2, 3, 4, 7, 6, 4, 6, 5 };
            cuttingCollider.sharedMesh = colliderMesh;
        }

        private void Place(Cut cut) {
            if (cut.First is Vector2 cutA && cut.Last is Vector2 cutB) {
                // Ignore the cut if the points are too close
                if (Vector2.Distance(cutA, cutB) < CutEpsilon) {
                    cut.Delete();
                    return;
                }

                meshDCEL.PlaceEdge((cutA, cutB), out var addedEdges);

                // Split the mesh if number of faces increases past 1, meaning the cut has split the mesh in 2 or more pieces
                if (meshDCEL.FaceCount > 1) {
                    var drawable = GetComponent<Painting.Drawable>();

                    // Create a new mesh from each extracted face
                    foreach (var newDCEL in meshDCEL.CreateFromFaces()) {
                        Cuttable copy = Instantiate(this);
                        copy.Init(newDCEL);
                        copy.transform.localScale = transform.localScale;

                        var drawableCopy = copy.GetComponent<Painting.Drawable>();
                        drawableCopy.CopyTextures(drawable);
                    }

                    Destroy(gameObject);
                }
                else {
                    // Display the placed segmented cut
                    cutVisual.SegmentCut(meshDCEL.Bridges);
                }
            }
            else {
                Debug.LogWarning("Cut has less than 2 points");
            }
        }

        public void UpdateDCELVisual() {
            cutVisual.SegmentCut(meshDCEL.Bridges);
        }

        //private Vector2 UVtoXY(Vector2 uv) {
        //    return new Vector2(uv.x * Ratio, uv.y);
        //}

        //private Vector2 XYtoUV(Vector2 v) {
        //    return new Vector2(v.x / Ratio, v.y);
        //}

        private object VertexPositionToData(Vector2 pos) {
            return new Tuple<Vector2, Vector3>(pos, Vector3.back);
        }

        private object VertexCombine(LibTessDotNet.Vec3 position, object[] data, float[] weights) {
            return VertexPositionToData(new Vector2(position.X, position.Y));
        }

        /// <summary>
        /// Projects <paramref name="pos"/> onto the plane defined by the mesh.
        /// </summary>
        /// <param name="pos">Point to project</param>
        /// <returns>Projected point in world space, in local space, and a <see cref="bool"/> that's <see langword="true"/> if the projected point lies on the paper.</returns>
        public (Vector3 projected, Vector2 localSpace, bool isOnPaper) ProjectOnPaper(Vector3 pos) {
            var normal = transform.TransformDirection(interactionNormal);

            // Raycast hit the paper collider
            if (cuttingCollider.Raycast(new Ray(pos, -normal), out RaycastHit hitInfo, float.PositiveInfinity)) {
                return (hitInfo.point, hitInfo.textureCoord, meshDCEL.IsInside(hitInfo.textureCoord));
            }
            // Raycast didn't hit the paper collider
            else {
                // Project the paper by transforming it to local space, zero'ing the Z coordinate, and transforming back to world space
                var localProjected = transform.InverseTransformPoint(pos);
                localProjected.z = 0.0f;

                return (transform.TransformPoint(localProjected), localProjected, false);
            }
        }

        public bool HasDCEL(DCEL instance) {
            return meshDCEL == instance;
        }

        private void OnDestroy() {
            var renderMesh = renderMeshFilter.sharedMesh;
            var colliderMesh = cuttingCollider.sharedMesh;
            renderMeshFilter.sharedMesh = null;
            cuttingCollider.sharedMesh = null;
            Destroy(renderMesh);
            Destroy(colliderMesh);

            if (currentCut != null) {
                currentCut.Delete();
                currentCut = null;
            }
            CutVisual = null;
        }
    }
}
