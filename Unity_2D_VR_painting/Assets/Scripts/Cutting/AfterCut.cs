using DataStructures;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Langweil.Cutting
{
    public class AfterCut : MonoBehaviour
    {
        private static AfterCut executingInstance = null;

        private static CooldownTimer timer = new CooldownTimer(0.75f, Undo);

        private void Start() {
            if (executingInstance == null) {
                executingInstance = this;
            }
        }

        private void FixedUpdate() {
            if (executingInstance == null) {
                executingInstance = this;
            }

            if (this == executingInstance) {
                DCEL.history.AdvanceTime();
                timer.Tick(Time.fixedDeltaTime);
            }
        }

        private void OnTriggerEnter(Collider other) {
            if (other.tag == "Undo") {
                timer.TryExecute();
            }
        }

        private static void Undo() {
            // Undo the actions
            DCEL.history.Undo(out var undoneDCELs);

            // Revive dead parents
            foreach (var pair in DCEL.history.ResurrectionQueue) {
                var parent = pair.Key;
                var children = pair.Value;
                var childPapers = new List<Paper>();
                foreach (var child in children) {
                    foreach (var paper in Paper.PaperRegistry.Items) {
                        if (paper.Cuttable.HasDCEL(child)) {
                            childPapers.Add(paper);
                            break;
                        }
                    }
                }

                Paper copy = Instantiate(childPapers[0]);
                copy.Cuttable.Init(parent);
                copy.transform.localScale = childPapers[0].transform.localScale;

                copy.Drawable.CopyTextures(childPapers[0].Drawable);

                // Kill the children
                foreach (var childPaper in childPapers) {
                    Utilities.Util.DestroyGameObject(childPaper);
                }
            }

            DCEL.history.ResurrectionQueue.Clear();

            // Update cut visuals
            if (undoneDCELs != null) {
                foreach (var undone in undoneDCELs) {
                    foreach (var paper in Paper.PaperRegistry.Items) {
                        if (paper.Cuttable.HasDCEL(undone)) {
                            paper.Cuttable.UpdateDCELVisual();
                            break;
                        }
                    }
                }
            }
        }

        private void OnDestroy() {
            executingInstance = null;
        }
    }
}
