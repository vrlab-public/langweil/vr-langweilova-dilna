using Langweil.Cutting;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;

namespace Langweil
{
    /// <summary>
    /// Component for storing transform of a paper slot.
    /// </summary>
    public class PaperSlot : MonoBehaviour
    {
        [SerializeField]
        [Tooltip("Hitbox where the paper should be placed")]
        [WarnOnNull]
        private Collider slot;

        [SerializeField]
        [Tooltip("Direction of the raycast against slot")]
        private Vector3 interactionNormal;

        private void Start() {
            WarnOnNullAttribute.Check(typeof(PaperSlot), this, name);
        }

        /// <summary>
        /// Returns position where the paper should get placed, or <see langword="null"/> when paper cannot be placed.
        /// </summary>
        /// <param name="currentPos">Current position of the move tool.</param>
        /// <param name="offset">Offset of the move tool.</param>
        /// <returns>Position where the paper should get placed, or <see langword="null"/> when paper cannot be placed.</returns>
        public (Vector3 pos, Quaternion rot, Transform parent)? GetPosition(Vector3 currentPos, Vector3 offset) {
            if (slot.Raycast(new Ray(currentPos, slot.transform.TransformDirection(interactionNormal)), out RaycastHit hitInfo, float.PositiveInfinity)) {
                return (hitInfo.point - (slot.transform.rotation * offset), slot.transform.rotation, slot.transform);
            }

            return null;
        }
    }
}
