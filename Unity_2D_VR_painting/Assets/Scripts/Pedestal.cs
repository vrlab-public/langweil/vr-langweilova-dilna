using Langweil;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Component for rotating a pedestal. Copied and modified from <see cref="Valve.VR.InteractionSystem.CircularDrive"/>.
/// </summary>
public class Pedestal : MonoBehaviour
{
	public enum Axis_t
	{
		XAxis,
		YAxis,
		ZAxis
	};

	[Tooltip("The axis around which the circular drive will rotate in local space")]
	public Axis_t axisOfRotation = Axis_t.XAxis;

	private Quaternion start;

	private Vector3 worldPlaneNormal = new Vector3(1.0f, 0.0f, 0.0f);
	private Vector3 localPlaneNormal = new Vector3(1.0f, 0.0f, 0.0f);

	private Vector3 lastHandProjected;

	private float outAngle;

	//-------------------------------------------------
	private void Start() {
		worldPlaneNormal = new Vector3(0.0f, 0.0f, 0.0f);
		worldPlaneNormal[(int)axisOfRotation] = 1.0f;

		localPlaneNormal = worldPlaneNormal;

		if (transform.parent) {
			worldPlaneNormal = transform.parent.localToWorldMatrix.MultiplyVector(worldPlaneNormal).normalized;
		}

		start = Quaternion.AngleAxis(transform.localEulerAngles[(int)axisOfRotation], localPlaneNormal);
		outAngle = 0.0f;
	}

	private Vector3 ProjectOnDrivePlane(Vector3 pos) {
		Vector3 toTransform = (pos - transform.position).normalized;
		Vector3 toTransformProjected = new Vector3(0.0f, 0.0f, 0.0f);

		// Need a non-zero distance from the hand to the center of the CircularDrive
		if (toTransform.sqrMagnitude > 0.0f) {
			toTransformProjected = Vector3.ProjectOnPlane(toTransform, worldPlaneNormal).normalized;
		}

		return toTransformProjected;
	}

	//-------------------------------------------------
	// Computes the angle to rotate the game object based on the change in the transform
	//-------------------------------------------------
	private void ComputeAngle(Vector3 pos) {
		Vector3 toHandProjected = ProjectOnDrivePlane(pos);

		if (!toHandProjected.Equals(lastHandProjected)) {
			float absAngleDelta = Vector3.Angle(lastHandProjected, toHandProjected);

			if (absAngleDelta > 0.0f) {
				
				Vector3 cross = Vector3.Cross(lastHandProjected, toHandProjected).normalized;
				float dot = Vector3.Dot(worldPlaneNormal, cross);

				float signedAngleDelta = absAngleDelta;

				if (dot < 0.0f) {
					signedAngleDelta = -signedAngleDelta;
				}

				outAngle += signedAngleDelta;
				lastHandProjected = toHandProjected;
			}
		}
	}

    private void OnTriggerEnter(Collider other) {
		if (other.attachedRigidbody != null && other.attachedRigidbody.TryGetComponent(out IInteractableTool collidedTool)) {
			lastHandProjected = ProjectOnDrivePlane(collidedTool.Tip.position);
			ComputeAngle(collidedTool.Tip.position);
            transform.localRotation = start * Quaternion.AngleAxis(outAngle, localPlaneNormal);
		}
	}

    private void OnTriggerStay(Collider other) {
		if (other.attachedRigidbody != null && other.attachedRigidbody.TryGetComponent(out IInteractableTool collidedTool)) {
			ComputeAngle(collidedTool.Tip.position);
			transform.localRotation = start * Quaternion.AngleAxis(outAngle, localPlaneNormal);
		}
	}
}
