using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorMixTest : MonoBehaviour
{
    [SerializeField]
    private Color mixColor;

    private Color currentColor;

    private Texture2D texture;
    private Color[] textureColor;
    private const int textureSize = 1024;

    // Start is called before the first frame update
    void Start() {
        texture = new Texture2D(textureSize, textureSize);
        textureColor = texture.GetPixels();
        SetColor(mixColor);
        
        GetComponent<MeshRenderer>().sharedMaterial.mainTexture = texture;
    }

    // Update is called once per frame
    void Update() {
        if (Input.GetKeyDown(KeyCode.A)) {
            SetColor(mixColor);
        }
        else if (Input.GetKeyDown(KeyCode.M)) {
            MixColor(mixColor, currentColor);
        }
    }

    private void SetColor(Color clr) {
        for (int i = 0; i < textureSize * textureSize; i++) {
            textureColor[i] = clr;
        }
        currentColor = clr;
        texture.SetPixels(textureColor);
        texture.Apply();
    }

    private void MixColor(Color srcClr, Color dstColor) {
        Color newClr = (1 - srcClr.a) * srcClr + (srcClr.a) * dstColor;
        SetColor(newClr);
    }
}
