using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Langweil
{
    /// <summary>
    /// Interface for tools that can interact with objects.
    /// </summary>
    public interface IInteractableTool
    {
        /// <summary>
        /// Tip of the tool that interacts.
        /// </summary>
        public Transform Tip {
            get;
        }

        /// <summary>
        /// Distance between tip and object when the tool interacts.
        /// </summary>
        public float InteractionDistance {
            get;
        }

        /// <summary>
        /// <see langword="true"/> if the tool should interact, <see langword="false"/> if not.
        /// </summary>
        public bool InteractionActive {
            get;
            set;
        }
    }
}
