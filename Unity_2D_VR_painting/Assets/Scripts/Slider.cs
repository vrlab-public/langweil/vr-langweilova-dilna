using Langweil.Painting;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Langweil
{
    /// <summary>
    /// Component for representing a slider that scales the paper.
    /// </summary>
    public class Slider : MonoBehaviour
    {
        [SerializeField]
        [Tooltip("Transform where the minimal step goes")]
        private Transform minPoint;
        [SerializeField]
        [Tooltip("Transform where the maximal step goes")]
        private Transform maxPoint;

        [SerializeField]
        [Tooltip("Prefab of the object that gets spawned for each step")]
        private GameObject stepPrefab;
        [SerializeField]
        [Tooltip("Prefab of the object that moves along with the current step")]
        private GameObject currentPrefab;

        private readonly List<GameObject> steps = new List<GameObject>();

        private GameObject currentStepGO;

        private Vector3 originalScale = Vector3.zero;

        private const float minScale = 0.5f;
        private const float maxScale = 3.0f;
        private const float step = 0.25f;
        private const int stepN = (int)((maxScale - minScale) / step);

        private Vector3 minMaxVector;
        private float minMaxMagnitude;

        private float currentScale;
        private float CurrentScale {
            set {
                currentScale = value;

                // Resize drawables
                foreach (var drawable in Drawable.drawableRegistry.Items) {
                    // Save original scale
                    if (originalScale.x == 0.0f) {
                        originalScale = drawable.transform.localScale;
                    }

                    // If not in a slot
                    if (drawable.transform.parent == null) {
                        // Save center
                        var cuttable = drawable.GetComponent<Cutting.Cuttable>();
                        Vector3 origCenter = cuttable != null ? cuttable.transform.TransformPoint(cuttable.CuttingColliderCenter) : Vector3.zero;

                        // Scale
                        drawable.transform.localScale = value * originalScale;

                        if (cuttable != null) {
                            // Get new center
                            Vector3 newCenter = cuttable.transform.TransformPoint(cuttable.CuttingColliderCenter);
                            // Offset so that scale happens around center
                            drawable.transform.position += origCenter - newCenter;

                        }
                    }
                }

                // Set step GO position
                var t = (value - minScale) / (maxScale - minScale);
                currentStepGO.transform.position = minPoint.position + t * minMaxVector;
            }
        }

        void Start() {
            // Spawn step prefabs
            minMaxVector = maxPoint.position - minPoint.position;
            minMaxMagnitude = minMaxVector.magnitude;

            for (int i = 0; i < stepN; ++i) {
                var t = (i * step) / (maxScale - minScale);
                steps.Add(Instantiate(stepPrefab, minPoint.position + t * minMaxVector, Quaternion.identity, transform));
            }

            // Spawn current prefab
            currentStepGO = Instantiate(currentPrefab, transform);

            CurrentScale = 1.0f;
        }

        private void OnTriggerStay(Collider other) {
            if (other.attachedRigidbody != null && other.attachedRigidbody.TryGetComponent(out IInteractableTool it)) {
                // Project the interacting tool on the line between min and max points using dot product
                var toolVector = it.Tip.position - minPoint.position;
                float t = Vector3.Dot(minMaxVector, toolVector) / minMaxMagnitude;

                // t lies on minMaxVector
                if (0.0f <= t && t <= minMaxMagnitude) {
                    // Convert t to percentage
                    t /= minMaxMagnitude;

                    // Get current step percentage
                    var tCurr = (currentScale - minScale) / (maxScale - minScale);

                    // If t is under current step
                    if (t < tCurr) {
                        // Get lower bound (half-way between current and prev), if under move current
                        var tPrev = (currentScale - step - minScale) / (maxScale - minScale);
                        if (t < (tPrev + tCurr) / 2.0f) {
                            CurrentScale = currentScale - step;
                        }
                    }
                    // If t is over current step
                    else {
                        // Get upper bound (half-way between current and next), if under move current
                        var tNext = (currentScale + step - minScale) / (maxScale - minScale);
                        if (t > (tCurr + tNext) / 2.0f) {
                            CurrentScale = currentScale + step;
                        }
                    }
                }
            }
        }

    }
}
