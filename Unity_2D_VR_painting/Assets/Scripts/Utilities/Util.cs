using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace Utilities
{
    /// <summary>
    /// Class for utility methods.
    /// </summary>
    public static class Util
    {
        public const float cm = .01f;
        public const float mm = .001f;

        public static Vector2 PoisonedVector2 => new Vector2(float.NaN, float.NaN);
        public static Vector3 PoisonedVector3 => new Vector3(float.NaN, float.NaN, float.NaN);

        /// <summary>
        /// Swaps <paramref name="a"/> and <paramref name="b"/>.
        /// </summary>
        /// <typeparam name="T">Type of the swapped items.</typeparam>
        /// <param name="a">Item to be swapped with <paramref name="b"/>.</param>
        /// <param name="b">Item to be swapped with <paramref name="a"/>.</param>
        public static void Swap<T>(ref T a, ref T b) {
            T temp = a;
            a = b;
            b = temp;
        }

        /// <summary>
        /// Converts Unity's <see cref="Vector2.SignedAngle(Vector2, Vector2)"/> from range <-180, 180> to <0, 360>.
        /// </summary>
        /// <param name="from">Angle is measured from this vector.</param>
        /// <param name="to">Angle is measured to this vector.</param>
        /// <returns>CCW angle in degrees between <paramref name="from"/> and <paramref name="to"/>.</returns>
        public static float CCWAngle(Vector2 from, Vector2 to) {
            return (360.0f + Vector2.SignedAngle(from, to)) % 360.0f;
        }

        /// <summary>
        /// Generates a semi-random sequence of colors with each color having distinctly different hue from the previous one.
        /// </summary>
        /// <returns>Generator for a semi-random sequence of colors.</returns>
        public static IEnumerable<Color32> GetColorSeq() {
            return GetColorSeq(new System.Random());
        }

        /// <summary>
        /// Generates a semi-random sequence of colors with each color having distinctly different hue from the previous one.
        /// </summary>
        /// <param name="seed">Seed for the random number generator.</param>
        /// <returns>Generator for a semi-random sequence of colors.</returns>
        public static IEnumerable<Color32> GetColorSeq(int seed) {
            return GetColorSeq(new System.Random(seed));
        }

        // Actual implementation of the public methods
        private static IEnumerable<Color32> GetColorSeq(System.Random gen) {
            float step = 0.18f;
            int time = 0;

            while (time < 1000) {
                yield return Color.HSVToRGB((time * step) % 1.0f, 0.6f * (float)gen.NextDouble() + 0.4f, 0.2f * (float)gen.NextDouble() + 0.8f);
                ++time;
            }
        }

        /// <summary>
        /// Destroys the GO the component belongs to safely.
        /// </summary>
        /// <param name="component">Reference to the component whose GO to destroy.</param>
        public static void DestroyGameObject(Component component) {
            if (component != null) UnityEngine.Object.Destroy(component.gameObject);
        }

        public static string ToHexString(float f) {
            var bytes = BitConverter.GetBytes(f);
            var i = BitConverter.ToInt32(bytes, 0);
            return "0x" + i.ToString("X8");
        }

        public static string GetUniqueFileName(string folderPath, string name, string ext) {
            string validatedName = name;
            int tries = 1;
            while (File.Exists(Path.Combine(folderPath, validatedName + "." + ext))) {
                validatedName = $"{name} ({tries++})";
            }
            return validatedName;
        }
    }
}
