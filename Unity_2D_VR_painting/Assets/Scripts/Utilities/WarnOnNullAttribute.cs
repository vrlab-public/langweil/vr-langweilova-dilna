using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace Utilities
{
    [AttributeUsage(AttributeTargets.Field, Inherited = false, AllowMultiple = false)]
    sealed class WarnOnNullAttribute : Attribute
    {
        private static Action<string> logger = Debug.LogWarning;

        public WarnOnNullAttribute() {
        }

        public static void Check(Type t, object instance, string instanceName) {
#if UNITY_EDITOR
            // Check if t and type of instance is same
            if (!t.IsAssignableFrom(instance.GetType())) return;

            // Get all fields
            var fields = t.GetFields(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.DeclaredOnly);

            foreach (var field in fields) {
                // Get fields that have this attribute
                var att = GetCustomAttribute(field, typeof(WarnOnNullAttribute)) as WarnOnNullAttribute;
                if (att != null) {
                    // Get field value
                    var fieldValue = field.GetValue(instance);

                    if (fieldValue == null || (fieldValue != null && fieldValue.Equals(null))) {
                        logger($"Field {field.FieldType} {field.Name} on {t.FullName} \"{instanceName}\" is not set (null)!");
                    }
                    // Field is not null
                    else {
                        // Check for subitems
                        var collection = fieldValue as ICollection;
                        if (collection != null) {
                            int i = 0;
                            foreach (var item in collection) {
                                Check(item.GetType(), item, $"{instanceName}::{field.Name}[{i}]");
                                ++i;
                            }
                        }
                    }
                }
            }
#endif
        }
    }
}
