using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utilities
{
    /// <summary>
    /// Class for keeping a registry of items, allowing for registering, unregistering and iterating over registered items.
    /// </summary>
    /// <typeparam name="T">Type of the item in the registry.</typeparam>
    public abstract class Registry<T>
    {
        /// <summary>
        /// Returns registered items.
        /// </summary>
        public abstract IEnumerable<T> Items { get; }

        /// <summary>
        /// Registers an item.
        /// </summary>
        /// <param name="item">Item to be registered.</param>
        /// <returns>Depends on implementation.</returns>
        public abstract bool Register(T item);

        /// <summary>
        /// Unregisters an item, that is removes it from the registry.
        /// </summary>
        /// <param name="item">Item to be unregistered.</param>
        /// <returns>Depends on implementation.</returns>
        public abstract bool Unregister(T item);

        /// <summary>
        /// Method to allow use in foreach statemnet.
        /// </summary>
        /// <returns>Enumerator for registered items</returns>
        public IEnumerator<T> GetEnumerator() {
            return Items.GetEnumerator();
        }
    }
}
