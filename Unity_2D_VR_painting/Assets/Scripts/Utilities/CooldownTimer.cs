using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CooldownTimer
{
    private Action onExecute;

    private float timer = 0.0f;
    public float RemainingTime => timer;

    private float scaledCooldown;

    private float baseCooldown;
    /// <summary>
    /// Base cooldown in seconds. Negative values get clamped to 0.
    /// </summary>
    public float BaseCooldown {
        get => baseCooldown;
        set {
            if (value < 0.0f) {
                baseCooldown = 0.0f;
            }
            else {
                baseCooldown = value;
            }

            scaledCooldown = baseCooldown;
        }
    }

    public float CooldownPercent => scaledCooldown == 0.0f ? 100.0f : (timer / scaledCooldown) * 100.0f;

    private float lastReduction = 0.0f;
    private static float reduction = 0.0f;
    /// <summary>
    /// Cooldown reduction in percents/100. Value gets clamped into <0,<see cref="MaxReduction"/>>.
    /// </summary>
    /// <remarks>Current cooldown will be multiplied by 1-<see cref="Reduction"/> when checking whether the timer is off cooldown.</remarks>
    public static float Reduction {
        get => reduction;
        set {
            if (value < 0.0f) {
                reduction = 0.0f;
            }
            else if (maxReduction < value) {
                reduction = maxReduction;
            }
            else {
                reduction = value;
            }
        }
    }

    private static float maxReduction = 1.0f;
    /// <summary>
    /// Cooldown reduction cannot be set higher than this value. Value gets clamped into <0,1>.
    /// </summary>
    public static float MaxReduction {
        get => maxReduction;
        set {
            if (value < 0.0f) {
                maxReduction = 0.0f;
            }
            else if (1.0f < value) {
                maxReduction = 1.0f;
            }
            else {
                maxReduction = value;
            }

            if (maxReduction < reduction) {
                reduction = maxReduction;
            }
        }
    }

    private static float rate = 1.0f;
    /// <summary>
    /// Cooldown rate in percents/100. Value gets clamped into <0,<see cref="MaxRate"/>>.
    /// </summary>
    /// <remarks>Tick step will get multiplied by this value.</remarks>
    public static float Rate {
        get => rate;
        set {
            if (value < 0.0f) {
                rate = 0.0f;
            }
            else if (maxRate < value) {
                rate = maxRate;
            }
            else {
                rate = value;
            }
        }
    }

    private static float maxRate = 2.0f;
    /// <summary>
    /// Cooldown rate cannot be set higher than this value. Negative values get clamped to 0.
    /// </summary>
    public static float MaxRate {
        get => maxRate;
        set {
            if (value < 0.0f) {
                maxRate = 0.0f;
            }
            else {
                maxRate = value;
            }

            if (maxRate < rate) {
                rate = maxRate;
            }
        }
    }

    public bool OnCooldown => timer > 0.0f;

    public bool OffCooldown => !OnCooldown;

    public CooldownTimer(float cooldown, Action onExecute) {
        BaseCooldown = cooldown;
        this.onExecute = onExecute;
        ResetCooldown();
    }

    public void Tick(float timeDelta) {
        if (OnCooldown) {
            if (reduction != lastReduction) {
                //TODO
                scaledCooldown = (1.0f - reduction) * baseCooldown;
                lastReduction = reduction;
            }
            timer -= rate * timeDelta;
        }
    }

    public void ResetCooldown() {
        timer = -1.0f;
    }

    public bool TryExecute() {
        if (OffCooldown) {
            onExecute();
            timer = scaledCooldown;
            return true;
        }
        return false;
    }
}
