using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utilities
{
    public static class ExtensionMethods
    {
        /// <summary>
        /// Unwraps an array of tuples to a tuple of arrays.
        /// </summary>
        /// <param name="tupleArray">Reference to the array of tuples to unwrap.</param>
        /// <returns>Unwrapped tuple of arrays.</returns>
        public static (T1[], T2[]) Unwrap<T1, T2>(this Tuple<T1, T2>[] tupleArray) {
            var array1 = new T1[tupleArray.Length];
            var array2 = new T2[tupleArray.Length];

            for (int i = 0; i < tupleArray.Length; ++i) {
                array1[i] = tupleArray[i].Item1;
                array2[i] = tupleArray[i].Item2;
            }

            return (array1, array2);
        }

        public static string NormalizeExceptZero(ref this Vector3 vec, Vector3 replacement) {
            vec.Normalize();
            if (vec.x == 0.0f && vec.y == 0.0f && vec.z == 0.0f) {
                vec = replacement;
                return $"Normalized vector not allowed to be zero, setting to {replacement}";
            }

            return null;
        }

        public static bool EqualsSimple(this Vector2 value, Vector2 other) {
            return value.x == other.x && value.y == other.y;
        }

        public static bool EqualsSimple(this Vector3 value, Vector3 other) {
            return value.x == other.x && value.y == other.y && value.z == other.z;
        }

        public static float DistanceSqrd(this Vector2 a, Vector2 b) {
            float diff_x = a.x - b.x;
            float diff_y = a.y - b.y;
            return diff_x * diff_x + diff_y * diff_y;
        }

        public static float DistanceSqrd(this Vector3 a, Vector3 b) {
            float diff_x = a.x - b.x;
            float diff_y = a.y - b.y;
            float diff_z = a.z - b.z;
            return diff_x * diff_x + diff_y * diff_y + diff_z * diff_z;
        }

        public static void Poison(ref this Vector2 vec) {
            vec.x = float.NaN;
            vec.y = float.NaN;
        }

        public static bool IsPoisoned(this Vector2 vec) {
            return float.IsNaN(vec.x) && float.IsNaN(vec.y);
        }

        public static void Poison(ref this Vector3 vec) {
            vec.x = float.NaN;
            vec.y = float.NaN;
            vec.z = float.NaN;
        }

        public static bool IsPoisoned(this Vector3 vec) {
            return float.IsNaN(vec.x) && float.IsNaN(vec.y) && float.IsNaN(vec.z);
        }

        public static bool EqualsAny<T>(this T obj, params T[] others) {
            foreach (var other in others) {
                if (obj.Equals(other)) {
                    return true;
                }
            }
            return false;
        }

        public static bool EqualsAll<T>(this T obj, params T[] others) {
            foreach (var other in others) {
                if (!obj.Equals(other)) {
                    return false;
                }
            }
            return true;
        }

        public static void CheckNull(this MonoBehaviour scipt) {
            WarnOnNullAttribute.Check(scipt.GetType(), scipt, scipt.name);
        }
    }
}
