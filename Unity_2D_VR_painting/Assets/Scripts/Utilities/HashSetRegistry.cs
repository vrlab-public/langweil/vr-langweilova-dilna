using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utilities
{
    /// <summary>
    /// Implementation of <see cref="Registry{T}"/> using a HashSet as underlying storage.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class HashSetRegistry<T> : Registry<T>
    {
        private readonly HashSet<T> items = new HashSet<T>();
        public override IEnumerable<T> Items => items;

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="item"><inheritdoc/></param>
        /// <returns>True if the item has already been registered, false if the item has been succesfully registered.</returns>
        public override bool Register(T item) {
            if (items.Contains(item)) {
                return true;
            }
            else {
                items.Add(item);
                return false;
            }
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="item"><inheritdoc/></param>
        /// <returns>True if the item has been succesfully unregistered, false if the item is not in the registry.</returns>
        public override bool Unregister(T item) {
            if (items.Contains(item)) {
                items.Remove(item);
                return true;
            }
            else {
                return false;
            }
        }
    }
}
