using DataStructures;
using Langweil.Cutting;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

namespace Langweil
{
    public class Calibration : MonoBehaviour
    {
        public enum EditModes
        {
            MoveUpDown, MoveLeftRight, Rotate, NoEdit
        }

        public static Interactable CurrentTool { get; private set; } = null;
        public static SteamVR_Input_Sources CurrentHand { get; private set; } = SteamVR_Input_Sources.Any;

        [SerializeField]
        private SteamVR_Action_Boolean confirmAction;
        [SerializeField]
        private SteamVR_Action_Boolean changeAction;
        [SerializeField]
        private SteamVR_Action_Vector2 moveAction;
        [SerializeField]
        private SteamVR_Action_Boolean resetAction;

        [SerializeField]
        private Transform tableCenter;
        [SerializeField]
        private Transform tableEdgeCenter;
        [SerializeField]
        private Transform tableEdgeDirectionPoint;

        public static Vector3 PaperStartPos { get; private set; }
        public static Quaternion PaperStartRot { get; private set; }

        [SerializeField]
        private Interactable startTool;

        [SerializeField]
        [Range(0.00001f, 1.0f)]
        private float movementSensitivity = 1.0f;
        [SerializeField]
        [Range(1.0f, 180.0f)]
        private float rotationSensitivity = 1.0f;

        [SerializeField]
        private Cuttable paper;

        private static readonly Quaternion resetRot = Quaternion.Euler(0.0f, 180.0f, 180.0f);

        private EditModes editMode = EditModes.MoveUpDown;
        private const Hand.AttachmentFlags flags = Hand.AttachmentFlags.SnapOnAttach | Hand.AttachmentFlags.DetachFromOtherHand | Hand.AttachmentFlags.DetachOthers
            | Hand.AttachmentFlags.VelocityMovement | Hand.AttachmentFlags.TurnOffGravity;

        private IEnumerator Start() {
            // Init controls
            confirmAction.AddOnStateUpListener(Calibrate, SteamVR_Input_Sources.RightHand);
            changeAction.AddOnStateUpListener(ChangeEditMode, SteamVR_Input_Sources.RightHand);
            moveAction.AddOnAxisListener(Move, SteamVR_Input_Sources.RightHand);
            resetAction.AddOnStateUpListener(Reset, SteamVR_Input_Sources.RightHand);

            confirmAction.AddOnStateUpListener(Calibrate, SteamVR_Input_Sources.LeftHand);
            changeAction.AddOnStateUpListener(ChangeEditMode, SteamVR_Input_Sources.LeftHand);
            moveAction.AddOnAxisListener(Move, SteamVR_Input_Sources.LeftHand);
            resetAction.AddOnStateUpListener(Reset, SteamVR_Input_Sources.LeftHand);

            // Init paper
            var tableDir = tableEdgeDirectionPoint.position - tableEdgeCenter.position;
            var tableToCenterDir = tableCenter.position - tableEdgeCenter.position;

            PaperStartPos =
                tableEdgeCenter.position
                + (paper.Width / 2.0f) * (tableDir).normalized
                + 0.1f * (tableToCenterDir).normalized
                + 0.0001f * Vector3.Cross(tableDir, tableToCenterDir).normalized;
            PaperStartRot = tableEdgeCenter.rotation;

            paper.transform.SetPositionAndRotation(PaperStartPos, PaperStartRot);
            Vector2 max = paper.Axes.primaryAxis == paper.Width ? new Vector2(1.0f, paper.Ratio) : new Vector2(paper.Ratio, 1.0f);
            var initDCEL = DCEL.Create(new Polygon(
                new List<Vector2> { new Vector2(0.0f, 0.0f), new Vector2(max.x, 0.0f), new Vector2(max.x, max.y), new Vector2(0.0f, max.y) }
            ));
            paper.Init(initDCEL);
            paper = null;

            // Wait until player is initialized
            while (Player.instance == null || Player.instance.rightHand == null || Player.instance.leftHand == null) {
                yield return null;
            }

            
        }

        private void Update() {
            if (Input.GetKeyUp(KeyCode.R)) {
                ResetPaper();
            }
        }

        private void Calibrate(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource) {
            Player player = Player.instance;
            if (player == null || player.rightHand == null || player.leftHand == null) return;

            // Show controllers
            player.rightHand.ShowController(true);
            player.leftHand.ShowController(true);
            player.rightHand.SetSkeletonRangeOfMotion(EVRSkeletalMotionRange.WithController);
            player.leftHand.SetSkeletonRangeOfMotion(EVRSkeletalMotionRange.WithController);

            var toolTemp = CurrentTool == null ? startTool : CurrentTool;

            // Detach tool
            SwitchTool(null, fromSource);

            player.rightHand.objectAttachmentPoint = player.rightHand.mainRenderModel.transform.Find("Calibration Tip");
            player.leftHand.objectAttachmentPoint = player.leftHand.mainRenderModel.transform.Find("Calibration Tip");

            var rightHandCenter = player.rightHand.mainRenderModel.transform.Find("Calibration Center");
            var leftHandCenter = player.leftHand.mainRenderModel.transform.Find("Calibration Center");

            // Get controller positions - right is table center, left is used to determine the table's front edge
            var currentCenter = rightHandCenter.position;
            var currentDir = leftHandCenter.position - currentCenter;

            var tableDir = tableEdgeDirectionPoint.position - tableEdgeCenter.position;

            // Calculate the translation needed to move the right controller to the virtual table center and apply it to player position
            var translate = tableEdgeCenter.position - currentCenter;
            player.trackingOriginTransform.position += translate;

            // Calculate angle difference between the controller line (edge on real table) and table line (edge of virtual table)
            var delta = Vector3.SignedAngle(currentDir, tableDir, Vector3.up);

            var tableOffset = tableEdgeCenter.position - player.trackingOriginTransform.position;

            // Move the tracking origin to table center, rotate both the player and the table offset, and move the origin back
            player.trackingOriginTransform.position += tableOffset;
            player.transform.Rotate(Vector3.up, delta);
            tableOffset = Quaternion.Euler(0.0f, delta, 0.0f) * tableOffset;
            player.trackingOriginTransform.position -= tableOffset;

            // Reattach tool
            SwitchTool(toolTemp, fromSource);
        }

        private void ChangeEditMode(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource) {
            if (CurrentTool != null) {
                editMode = (EditModes)(((int)editMode + 1) % Enum.GetValues(typeof(EditModes)).Length);
            }
        }

        private void Reset(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource) {
            if (CurrentTool != null && CurrentTool.AttachmentOffset != null && CurrentTool.TryGetComponent(out IInteractableTool itool)) {
                CurrentTool.AttachmentOffset.transform.position = itool.Tip.position;
                CurrentTool.AttachmentOffset.transform.localRotation = resetRot;
            }
        }

        private void Move(SteamVR_Action_Vector2 fromAction, SteamVR_Input_Sources fromSource, Vector2 axis, Vector2 delta) {
            Player player = Player.instance;
            if (CurrentTool == null || player == null || player.rightHand == null || player.leftHand == null) return;

            Hand hand;
            if (fromSource == SteamVR_Input_Sources.RightHand) {
                hand = Player.instance.rightHand;
            }
            else if (fromSource == SteamVR_Input_Sources.LeftHand) {
                hand = Player.instance.leftHand;
            }
            else {
                return;
            }

            if (editMode == EditModes.MoveUpDown) {
                CurrentTool.AttachmentOffset.localPosition += Time.deltaTime * movementSensitivity * new Vector3(0.0f, 0.0f, axis.y);
            }
            else if (editMode == EditModes.MoveLeftRight) {
                CurrentTool.AttachmentOffset.localPosition += Time.deltaTime * movementSensitivity * new Vector3(-axis.x, 0.0f, 0.0f);
            }
            else if (editMode == EditModes.Rotate) {
                CurrentTool.AttachmentOffset.localRotation *= Quaternion.AngleAxis(Time.deltaTime * rotationSensitivity * axis.x, Vector3.forward);
            }

            // Reattach object to update attachment offset
            hand.AttachObject(CurrentTool.gameObject, GrabTypes.Grip, flags, CurrentTool.AttachmentOffset);
        }

        public static void SwitchTool(Interactable tool, SteamVR_Input_Sources handType) {
            Player player = Player.instance;
            if (player == null || player.rightHand == null || player.leftHand == null) return;

            Hand hand;
            if (handType == SteamVR_Input_Sources.RightHand) {
                hand = Player.instance.rightHand;
            }
            else if (handType == SteamVR_Input_Sources.LeftHand) {
                hand = Player.instance.leftHand;
            }
            else {
                return;
            }

            if (tool == null) {
                if (CurrentTool != null) {
                    hand.DetachObject(CurrentTool.gameObject);
                    CurrentTool = null;
                }
                return;
            }

            if (CurrentTool != null) {
                if (CurrentTool.TryGetComponent(out IInteractableTool itoolOld)) {
                    itoolOld.InteractionActive = false;
                }
                else {
                    return;
                }
            }

            IInteractableTool itool;
            if (!tool.TryGetComponent(out itool)) {
                return;
            }

            tool.GetComponent<Rigidbody>().isKinematic = false;
            itool.InteractionActive = true;

            CurrentTool = tool;
            CurrentTool.useHandObjectAttachmentPoint = true;

            if (CurrentTool.AttachmentOffset == null) {
                var offset = new GameObject("Attachment Offset");
                offset.transform.parent = CurrentTool.transform;
                offset.transform.localRotation = resetRot;
                offset.transform.position = itool.Tip.position;
                offset.transform.localScale = Vector3.one;

                CurrentTool.AttachmentOffset = offset.transform;
            }

            CutVisual.display = CurrentTool.TryGetComponent(out Knife _);

            CurrentHand = handType;
            hand.AttachObject(CurrentTool.gameObject, GrabTypes.Grip, flags, CurrentTool.AttachmentOffset);
        }

        public void ResetPaper() {
            // Cache papers
            var papers = new List<Paper>();
            foreach (var paper in Paper.PaperRegistry) {
                papers.Add(paper);
            }

            if (papers.Count > 0) {
                var copy = Instantiate(papers[0], PaperStartPos, PaperStartRot);
                Vector2 max = copy.Cuttable.Axes.primaryAxis == copy.Cuttable.Width ? new Vector2(1.0f, copy.Cuttable.Ratio) : new Vector2(copy.Cuttable.Ratio, 1.0f);
                var initDCEL = DCEL.Create(new Polygon(
                    new List<Vector2> { new Vector2(0.0f, 0.0f), new Vector2(max.x, 0.0f), new Vector2(max.x, max.y), new Vector2(0.0f, max.y) }
                ));
                copy.Cuttable.Init(initDCEL);
                copy.Drawable.CopyTextures(papers[0].Drawable);

                foreach (var paper in papers) {
                    Utilities.Util.DestroyGameObject(paper);
                }
            }
        }
    }
}
