Git repository for the bachelor's thesis project: 2D painting in VR.

Unity_2D_VR_painting contains an implementation in Unity accompanying the thesis.

Documents contain the submitted project and thesis texts, thesis latex source code, readmes for building the project and questionnaire used for testing the thesis.

Images contain screenshots and other images showcasing the implementation.

Videos contain a video showcasing the implementation.

Author: Robert Papay
